/** global definitions and methods
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2020
 */
/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdlib.h> // general utilities
#include <string.h> // memory utilities

/* STM32 (including CM3) libraries */
#include <libopencmsis/core_cm3.h> // Cortex M3 utilities
#include <libopencm3/cm3/nvic.h> // interrupt handler
#include <libopencm3/cm3/systick.h> // SysTick library
#include <libopencm3/stm32/rcc.h> // real-time control clock library
#include <libopencm3/stm32/gpio.h> // general purpose input output library
#include <libopencm3/stm32/exti.h> // external interrupt defines

#include "global.h" // common methods

volatile bool button_flag = false;
volatile bool user_input_available = false;

static volatile uint8_t user_input_buffer[64] = {0}; /**< ring buffer for received data */
static volatile uint8_t user_input_i = 0; /**< current position of read received data */
static volatile uint8_t user_input_used = 0; /**< how much data has been received and not red */

static volatile uint32_t sleep_duration = 0; /**< sleep duration count down (in SysTick interrupts) */

uint8_t addu8_safe(uint8_t a, uint8_t b)
{
	if (a > UINT8_MAX - b) {
		return UINT8_MAX;
	} else {
		return a + b;
	}
}

uint16_t addu16_safe(uint16_t a, uint16_t b)
{
	if (a > UINT16_MAX - b) {
		return UINT16_MAX;
	} else {
		return a + b;
	}
}

uint32_t addu32_safe(uint32_t a, uint32_t b)
{
	if (a > UINT32_MAX - b) {
		return UINT32_MAX;
	} else {
		return a + b;
	}
}

int8_t adds8_safe(int8_t a, int8_t b)
{
	if (b > 0) {
		if (a > INT8_MAX - b) {
			return INT8_MAX;
		} else {
			return a + b;
		}
	} else {
		if (a <  INT8_MIN + b) {
			return INT8_MIN;
		} else {
			return a + b;
		}
	}
}

int16_t adds16_safe(int16_t a, int16_t b)
{
	if (b > 0) {
		if (a > INT16_MAX - b) {
			return INT16_MAX;
		} else {
			return a + b;
		}
	} else {
		if (a <  INT16_MIN + b) {
			return INT16_MIN;
		} else {
			return a + b;
		}
	}
}

int32_t adds32_safe(int32_t a, int32_t b)
{
	if (b > 0) {
		if (a > INT32_MAX - b) {
			return INT32_MAX;
		} else {
			return a + b;
		}
	} else {
		if (a <  INT32_MIN + b) {
			return INT32_MIN;
		} else {
			return a + b;
		}
	}
}

char* b2s(uint64_t binary, uint8_t rjust)
{
	static char string[64 + 1] = {0}; // the string representation to return
	uint8_t bit = LENGTH(string) - 1; // the index of the bit to print
	string[bit--] = '\0'; // terminate string

	while (binary) {
		if (binary & 1) {
			string[bit--] = '1';
		} else {
			string[bit--] = '0';
		}
		binary >>= 1;
	}

	while (64 - bit - 1 < rjust && bit > 0) {
		string[bit--] = '0';
	}

	return string;
}

/** switch on board LED */
inline void led_on(void)
{
#if defined(LED_PIN)
#if defined(BUSVOODOO)
	gpio_set_mode(GPIO_PORT(LED_PIN), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO_PIN(LED_PIN)); // set LED pin push-pull
#endif // BUSVOODOO
#if defined(LED_ON) && LED_ON
	gpio_set(GPIO_PORT(LED_PIN), GPIO_PIN(LED_PIN));
#else
	gpio_clear(GPIO_PORT(LED_PIN), GPIO_PIN(LED_PIN));
#endif // LED_ON
#endif // LED_PIN
}

/** switch off board LED */
inline void led_off(void)
{
#if defined(LED_PIN)
#if defined(BUSVOODOO)
	gpio_set_mode(GPIO_PORT(LED_PIN), GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO_PIN(LED_PIN)); // set LED pin to floating to disable LEDs
#else
#if defined(LED_ON) && LED_ON
	gpio_clear(GPIO_PORT(LED_PIN), GPIO_PIN(LED_PIN));
#else
	gpio_set(GPIO_PORT(LED_PIN), GPIO_PIN(LED_PIN));
#endif // BUSVOODOO
#endif // LED_ON
#endif // LED_PIN
}

/** toggle board LED */
inline void led_toggle(void)
{
#if defined(LED_PIN)
#if defined(BUSVOODOO)
	gpio_set_mode(GPIO_PORT(LED_PIN), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO_PIN(LED_PIN)); // set LED pin to push-pull
#endif // BUSVOODOO
	gpio_toggle(GPIO_PORT(LED_PIN), GPIO_PIN(LED_PIN));
#endif // LED_PIN
}

void sleep_us(uint32_t duration)
{
	if (duration <= 5) { // less than the setup time
		for (volatile uint32_t nop = 0; nop < 5 * duration; nop++); // busy loop, approximate, hand tuned for 72 MHz system clock
		return;
	}

	duration -= 5; // subtract setup time
	systick_counter_disable(); // disable SysTick to reconfigure it
	if (!systick_set_frequency(1000000, rcc_ahb_frequency)) { // set SysTick frequency to microseconds
		 while (true); // unhandled error
	}
	systick_clear(); // reset SysTick
	systick_interrupt_enable(); // enable interrupt to count duration
	sleep_duration = duration; // save sleep duration for count down
	systick_counter_enable(); // start counting
	while (sleep_duration > 0) { // wait for count down to complete
		__WFI(); // go to sleep
	}
}

void sleep_ms(uint32_t duration)
{
	if (0 == duration) {
		return;
	}

	systick_counter_disable(); // disable SysTick to reconfigure it
	if (!systick_set_frequency(1000, rcc_ahb_frequency)) { // set SysTick frequency to milliseconds
		while (true); // unhandled error
	}
	systick_clear(); // reset SysTick
	systick_interrupt_enable(); // enable interrupt to count duration
	sleep_duration = duration; // save sleep duration for count down
	systick_counter_enable(); // start counting
	while (sleep_duration > 0) { // wait for count down to complete
		__WFI(); // go to sleep
	}
}

/** SysTick interrupt handler */
void sys_tick_handler(void)
{
	if (sleep_duration > 0) {
		sleep_duration--; // decrement duration
	}
	if (0 == sleep_duration) { // sleep complete
		systick_counter_disable(); // stop systick
		systick_interrupt_disable(); // stop interrupting
		sleep_duration = 0; // ensure it still is at 0
	}
}

char user_input_get(void)
{
	while (0 == user_input_used) { // wait for user input to be available (don't trust user_input_available since it user modifiable)
		__WFI(); // go to sleep
	}
	volatile char to_return = user_input_buffer[user_input_i]; // get the next available character
	user_input_i = (user_input_i + 1) % LENGTH(user_input_buffer); // update used buffer
	user_input_used--; // update used buffer
	user_input_available = (user_input_used != 0); // update available data
	return to_return;
}

void user_input_store(char c)
{
	// only save data if there is space in the buffer
	if (user_input_used >= LENGTH(user_input_buffer)) { // if buffer is full
		user_input_i = (user_input_i + 1) % LENGTH(user_input_buffer); // drop oldest data
		user_input_used--; // update used buffer information
	}
	user_input_buffer[(user_input_i + user_input_used) % LENGTH(user_input_buffer)] = c; // put character in buffer
	user_input_used++; // update used buffer
	user_input_available = true; // update available data
}

void board_setup(void)
{
#if defined(LED_PIN)
	// setup LED
	rcc_periph_clock_enable(GPIO_RCC(LED_PIN)); // enable clock for LED
#if defined(BUSVOODOO)
	gpio_set_mode(GPIO_PORT(LED_PIN), GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO_PIN(LED_PIN)); // set LED pin to floating to disable LEDs
#else
	gpio_set_mode(GPIO_PORT(LED_PIN), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO_PIN(LED_PIN)); // set LED pin to output push-pull do drive LED
#endif
	led_off(); // switch off LED per default
#endif // LED_PIN

	// setup button
#if defined(BUTTON_PIN)
	rcc_periph_clock_enable(GPIO_RCC(BUTTON_PIN)); // enable clock for button
	gpio_set_mode(GPIO_PORT(BUTTON_PIN), GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_PIN(BUTTON_PIN)); // set button pin to input
	rcc_periph_clock_enable(RCC_AFIO); // enable alternate function clock for external interrupt
	exti_select_source(GPIO_EXTI(BUTTON_PIN), GPIO_PORT(BUTTON_PIN)); // mask external interrupt of this pin only for this port
#if defined(BUTTON_PRESSED) && BUTTON_PRESSED
	gpio_clear(GPIO_PORT(BUTTON_PIN), GPIO_PIN(BUTTON_PIN)); // pull down to be able to detect button push (go high)
	exti_set_trigger(GPIO_EXTI(BUTTON_PIN), EXTI_TRIGGER_RISING); // trigger when button is pressed
#else
	gpio_set(GPIO_PORT(BUTTON_PIN), GPIO_PIN(BUTTON_PIN)); // pull up to be able to detect button push (go low)
	exti_set_trigger(GPIO_EXTI(BUTTON_PIN), EXTI_TRIGGER_FALLING); // trigger when button is pressed
#endif
	exti_enable_request(GPIO_EXTI(BUTTON_PIN)); // enable external interrupt
	nvic_enable_irq(GPIO_NVIC_EXTI_IRQ(BUTTON_PIN)); // enable interrupt
#endif

	// reset user input buffer
	user_input_available = false;
	user_input_i = 0;
	user_input_used = 0;
}

#if defined(BUTTON_PIN)
/** interrupt service routine called when button is pressed */
void GPIO_EXTI_ISR(BUTTON_PIN)(void)
{
	exti_reset_request(GPIO_EXTI(BUTTON_PIN)); // reset interrupt
	button_flag = true; // perform button action
}
#endif
