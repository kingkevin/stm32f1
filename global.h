/** global definitions and methods
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2020
 */
#pragma once

/** enable debugging functionalities */
#define DEBUG true

/** get the length of an array */
#define LENGTH(x) (sizeof(x) / sizeof((x)[0]))
/** concatenate 2 arguments */
#define CAT2(x,y) x##y
/** concatenate 3 arguments */
#define CAT3(x,y,z) x##y##z
/** concatenate 4 arguments */
#define CAT4(w,x,y,z) w##x##y##z
/** integer underflow/overflow safe uint8_t addition (result to min/max on underflow/overflow) */
#define ADDU8_SAFE(a,b) {__typeof__ (a) _a = (a); __typeof__ (b) _b = (b); a = (_b > 0 ? ((_a > UINT8_MAX - _b) ? UINT8_MAX : (_a + _b)) : ((_a < _b) ? 0 : (_a - _b)));}
/** integer underflow/overflow safe uint16_t addition (result to min/max on underflow/overflow) */
#define ADDU16_SAFE(a,b) {__typeof__ (a) _a = (a); __typeof__ (b) _b = (b); a = (_b > 0 ? ((_a > UINT16_MAX - _b) ? UINT16_MAX : (_a + _b)) : ((_a < _b) ? 0 : (_a - _b)));}
/** integer underflow/overflow safe uint32_t addition (result to min/max on underflow/overflow) */
#define ADDU32_SAFE(a,b) {__typeof__ (a) _a = (a); __typeof__ (b) _b = (b); a = (_b > 0 ? ((_a > UINT32_MAX - _b) ? UINT32_MAX : (_a + _b)) : ((_a < _b) ? 0 : (_a - _b)));}
/** integer underflow/overflow safe int8_t addition (result to min/max on underflow/overflow) */
#define ADDS8_SAFE(a,b) {__typeof__ (a) _a = (a); __typeof__ (b) _b = (b); a = (_b > 0 ? ((_a > INT8_MAX - _b) ? INT8_MAX : (_a + _b)) : ((_a < INT8_MAX + _b) ? INT8_MAX : (_a + _b)));}
/** integer underflow/overflow safe int16_t addition (result to min/max on underflow/overflow) */
#define ADDS16_SAFE(a,b) {__typeof__ (a) _a = (a); __typeof__ (b) _b = (b); a = (_b > 0 ? ((_a > INT16_MAX - _b) ? INT16_MAX : (_a + _b)) : ((_a < INT16_MIN + _b) ? INT16_MIN : (_a + _b)));}
/** integer underflow/overflow safe int32_t addition (result to min/max on underflow/overflow) */
#define ADDS32_SAFE(a,b) {__typeof__ (a) _a = (a); __typeof__ (b) _b = (b); a = (_b > 0 ? ((_a > INT32_MAX - _b) ? INT32_MAX : (_a + _b)) : ((_a < INT32_MIN + _b) ? INT32_MIN : (_a + _b)));}

/** unsigned 8-bit integer overflow safe addition
 *  @param[in] a first part of addition
 *  @param[in] b second part of addition
 *  return result of addition, or type max on overflow
 */
uint8_t addu8_safe(uint8_t a, uint8_t b);
/** unsigned 16-bit integer overflow safe addition
 *  @param[in] a first part of addition
 *  @param[in] b second part of addition
 *  return result of addition, or type max on overflow
 */
uint16_t addu16_safe(uint16_t a, uint16_t b);
/** unsigned 8-bit integer overflow safe addition
 *  @param[in] a first part of addition
 *  @param[in] b second part of addition
 *  return result of addition, or type max on overflow
 */
uint32_t addu32_safe(uint32_t a, uint32_t b);
/** signed 8-bit integer underflow/overflow safe addition
 *  @param[in] a first part of addition
 *  @param[in] b second part of addition
 *  return result of addition, or type max on overflow
 */
int8_t adds8_safe(int8_t a, int8_t b);
/** signed 16-bit integer underflow/overflow safe addition
 *  @param[in] a first part of addition
 *  @param[in] b second part of addition
 *  return result of addition, or type max on overflow
 */
int16_t adds16_safe(int16_t a, int16_t b);
/** signed 32-bit integer underflow/overflow safe addition
 *  @param[in] a first part of addition
 *  @param[in] b second part of addition
 *  return result of addition, or type max on overflow
 */
int32_t adds32_safe(int32_t a, int32_t b);

/** build year as number */
#define COMPUTE_BUILD_YEAR \
    ( \
        (__DATE__[ 7] - '0') * 1000 + \
        (__DATE__[ 8] - '0') *  100 + \
        (__DATE__[ 9] - '0') *   10 + \
        (__DATE__[10] - '0') \
    )
/** build day as number */
#define COMPUTE_BUILD_DAY \
    ( \
        ((__DATE__[4] >= '0') ? (__DATE__[4] - '0') * 10 : 0) + \
        (__DATE__[5] - '0') \
    )
/** check if build month is January */
#define BUILD_MONTH_IS_JAN (__DATE__[0] == 'J' && __DATE__[1] == 'a' && __DATE__[2] == 'n')
/** check if build month is February */
#define BUILD_MONTH_IS_FEB (__DATE__[0] == 'F')
/** check if build month is March */
#define BUILD_MONTH_IS_MAR (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'r')
/** check if build month is April */
#define BUILD_MONTH_IS_APR (__DATE__[0] == 'A' && __DATE__[1] == 'p')
/** check if build month is May */
#define BUILD_MONTH_IS_MAY (__DATE__[0] == 'M' && __DATE__[1] == 'a' && __DATE__[2] == 'y')
/** check if build month is June */
#define BUILD_MONTH_IS_JUN (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'n')
/** check if build month is July */
#define BUILD_MONTH_IS_JUL (__DATE__[0] == 'J' && __DATE__[1] == 'u' && __DATE__[2] == 'l')
/** check if build month is August */
#define BUILD_MONTH_IS_AUG (__DATE__[0] == 'A' && __DATE__[1] == 'u')
/** check if build month is September */
#define BUILD_MONTH_IS_SEP (__DATE__[0] == 'S')
/** check if build month is October */
#define BUILD_MONTH_IS_OCT (__DATE__[0] == 'O')
/** check if build month is November */
#define BUILD_MONTH_IS_NOV (__DATE__[0] == 'N')
/** check if build month is December */
#define BUILD_MONTH_IS_DEC (__DATE__[0] == 'D')
/** build month as number */
#define COMPUTE_BUILD_MONTH \
    ( \
        (BUILD_MONTH_IS_JAN) ?  1 : \
        (BUILD_MONTH_IS_FEB) ?  2 : \
        (BUILD_MONTH_IS_MAR) ?  3 : \
        (BUILD_MONTH_IS_APR) ?  4 : \
        (BUILD_MONTH_IS_MAY) ?  5 : \
        (BUILD_MONTH_IS_JUN) ?  6 : \
        (BUILD_MONTH_IS_JUL) ?  7 : \
        (BUILD_MONTH_IS_AUG) ?  8 : \
        (BUILD_MONTH_IS_SEP) ?  9 : \
        (BUILD_MONTH_IS_OCT) ? 10 : \
        (BUILD_MONTH_IS_NOV) ? 11 : \
        (BUILD_MONTH_IS_DEC) ? 12 : \
        /* error default */  99 \
    )
/** check if build date is unknown */
#define BUILD_DATE_IS_BAD (__DATE__[0] == '?')
/** build year as number if known, or 0 if unknown */
#define BUILD_YEAR  ((BUILD_DATE_IS_BAD) ? 0 : COMPUTE_BUILD_YEAR)
/** build month as number if known, or 0 if unknown */
#define BUILD_MONTH ((BUILD_DATE_IS_BAD) ? 99 : COMPUTE_BUILD_MONTH)
/** build day as number if known, or 0 if unknown */
#define BUILD_DAY   ((BUILD_DATE_IS_BAD) ? 99 : COMPUTE_BUILD_DAY)

/** @defgroup reg_macro macros to  define values based on other defines values
 *  @note used when the value is calculated or isn't a value
 *  @{
 */
/** get GPIO based on GPIO port or pin identifier */
#define GPIO(x) CAT2(GPIO,x)
/** get GPIO port identifier based on GPIO port and pin identifier */
#define GPIO_PORT_ID(x) CAT2(x,_PORT)
/** get GPIO port based on GPIO port and pin identifier */
#define GPIO_PORT(x) GPIO(GPIO_PORT_ID(x))
#define PA0_PORT A /**< GPIO port pin A0 is on port A */
#define PA1_PORT A /**< GPIO port pin A1 is on port A */
#define PA2_PORT A /**< GPIO port pin A2 is on port A */
#define PA3_PORT A /**< GPIO port pin A3 is on port A */
#define PA4_PORT A /**< GPIO port pin A4 is on port A */
#define PA5_PORT A /**< GPIO port pin A5 is on port A */
#define PA6_PORT A /**< GPIO port pin A6 is on port A */
#define PA7_PORT A /**< GPIO port pin A7 is on port A */
#define PA8_PORT A /**< GPIO port pin A8 is on port A */
#define PA9_PORT A /**< GPIO port pin A9 is on port A */
#define PA10_PORT A /**< GPIO port pin A10 is on port A */
#define PA11_PORT A /**< GPIO port pin A11 is on port A */
#define PA12_PORT A /**< GPIO port pin A12 is on port A */
#define PA13_PORT A /**< GPIO port pin A13 is on port A */
#define PA14_PORT A /**< GPIO port pin A14 is on port A */
#define PA15_PORT A /**< GPIO port pin A15 is on port A */
#define PB0_PORT B /**< GPIO port pin B0 is on port B */
#define PB1_PORT B /**< GPIO port pin B1 is on port B */
#define PB2_PORT B /**< GPIO port pin B2 is on port B */
#define PB3_PORT B /**< GPIO port pin B3 is on port B */
#define PB4_PORT B /**< GPIO port pin B4 is on port B */
#define PB5_PORT B /**< GPIO port pin B5 is on port B */
#define PB6_PORT B /**< GPIO port pin B6 is on port B */
#define PB7_PORT B /**< GPIO port pin B7 is on port B */
#define PB8_PORT B /**< GPIO port pin B8 is on port B */
#define PB9_PORT B /**< GPIO port pin B9 is on port B */
#define PB10_PORT B /**< GPIO port pin B10 is on port B */
#define PB11_PORT B /**< GPIO port pin B11 is on port B */
#define PB12_PORT B /**< GPIO port pin B12 is on port B */
#define PB13_PORT B /**< GPIO port pin B13 is on port B */
#define PB14_PORT B /**< GPIO port pin B14 is on port B */
#define PB15_PORT B /**< GPIO port pin B15 is on port B */
#define PC0_PORT C /**< GPIO port pin C0 is on port C */
#define PC1_PORT C /**< GPIO port pin C1 is on port C */
#define PC2_PORT C /**< GPIO port pin C2 is on port C */
#define PC3_PORT C /**< GPIO port pin C3 is on port C */
#define PC4_PORT C /**< GPIO port pin C4 is on port C */
#define PC5_PORT C /**< GPIO port pin C5 is on port C */
#define PC6_PORT C /**< GPIO port pin C6 is on port C */
#define PC7_PORT C /**< GPIO port pin C7 is on port C */
#define PC8_PORT C /**< GPIO port pin C8 is on port C */
#define PC9_PORT C /**< GPIO port pin C9 is on port C */
#define PC10_PORT C /**< GPIO port pin C10 is on port C */
#define PC11_PORT C /**< GPIO port pin C11 is on port C */
#define PC12_PORT C /**< GPIO port pin C12 is on port C */
#define PC13_PORT C /**< GPIO port pin C13 is on port C */
#define PC14_PORT C /**< GPIO port pin C14 is on port C */
#define PC15_PORT C /**< GPIO port pin C15 is on port C */
#define PD0_PORT D /**< GPIO port pin D0 is on port D */
#define PD1_PORT D /**< GPIO port pin D1 is on port D */
#define PD2_PORT D /**< GPIO port pin D2 is on port D */
#define PD3_PORT D /**< GPIO port pin D3 is on port D */
#define PD4_PORT D /**< GPIO port pin D4 is on port D */
#define PD5_PORT D /**< GPIO port pin D5 is on port D */
#define PD6_PORT D /**< GPIO port pin D6 is on port D */
#define PD7_PORT D /**< GPIO port pin D7 is on port D */
#define PD8_PORT D /**< GPIO port pin D8 is on port D */
#define PD9_PORT D /**< GPIO port pin D9 is on port D */
#define PD10_PORT D /**< GPIO port pin D10 is on port D */
#define PD11_PORT D /**< GPIO port pin D11 is on port D */
#define PD12_PORT D /**< GPIO port pin D12 is on port D */
#define PD13_PORT D /**< GPIO port pin D13 is on port D */
#define PD14_PORT D /**< GPIO port pin D14 is on port D */
#define PD15_PORT D /**< GPIO port pin D15 is on port D */
#define PE0_PORT E /**< GPIO port pin E0 is on port E */
#define PE1_PORT E /**< GPIO port pin E1 is on port E */
#define PE2_PORT E /**< GPIO port pin E2 is on port E */
#define PE3_PORT E /**< GPIO port pin E3 is on port E */
#define PE4_PORT E /**< GPIO port pin E4 is on port E */
#define PE5_PORT E /**< GPIO port pin E5 is on port E */
#define PE6_PORT E /**< GPIO port pin E6 is on port E */
#define PE7_PORT E /**< GPIO port pin E7 is on port E */
#define PE8_PORT E /**< GPIO port pin E8 is on port E */
#define PE9_PORT E /**< GPIO port pin E9 is on port E */
#define PE10_PORT E /**< GPIO port pin E10 is on port E */
#define PE11_PORT E /**< GPIO port pin E11 is on port E */
#define PE12_PORT E /**< GPIO port pin E12 is on port E */
#define PE13_PORT E /**< GPIO port pin E13 is on port E */
#define PE14_PORT E /**< GPIO port pin E14 is on port E */
#define PE15_PORT E /**< GPIO port pin E15 is on port E */
#define PF0_PORT F /**< GPIO port pin F0 is on port F */
#define PF1_PORT F /**< GPIO port pin F1 is on port F */
#define PF2_PORT F /**< GPIO port pin F2 is on port F */
#define PF3_PORT F /**< GPIO port pin F3 is on port F */
#define PF4_PORT F /**< GPIO port pin F4 is on port F */
#define PF5_PORT F /**< GPIO port pin F5 is on port F */
#define PF6_PORT F /**< GPIO port pin F6 is on port F */
#define PF7_PORT F /**< GPIO port pin F7 is on port F */
#define PF8_PORT F /**< GPIO port pin F8 is on port F */
#define PF9_PORT F /**< GPIO port pin F9 is on port F */
#define PF10_PORT F /**< GPIO port pin F10 is on port F */
#define PF11_PORT F /**< GPIO port pin F11 is on port F */
#define PF12_PORT F /**< GPIO port pin F12 is on port F */
#define PF13_PORT F /**< GPIO port pin F13 is on port F */
#define PF14_PORT F /**< GPIO port pin F14 is on port F */
#define PF15_PORT F /**< GPIO port pin F15 is on port F */
#define PG0_PORT G /**< GPIO port pin G0 is on port G */
#define PG1_PORT G /**< GPIO port pin G1 is on port G */
#define PG2_PORT G /**< GPIO port pin G2 is on port G */
#define PG3_PORT G /**< GPIO port pin G3 is on port G */
#define PG4_PORT G /**< GPIO port pin G4 is on port G */
#define PG5_PORT G /**< GPIO port pin G5 is on port G */
#define PG6_PORT G /**< GPIO port pin G6 is on port G */
#define PG7_PORT G /**< GPIO port pin G7 is on port G */
#define PG8_PORT G /**< GPIO port pin G8 is on port G */
#define PG9_PORT G /**< GPIO port pin G9 is on port G */
#define PG10_PORT G /**< GPIO port pin G10 is on port G */
#define PG11_PORT G /**< GPIO port pin G11 is on port G */
#define PG12_PORT G /**< GPIO port pin G12 is on port G */
#define PG13_PORT G /**< GPIO port pin G13 is on port G */
#define PG14_PORT G /**< GPIO port pin G14 is on port G */
#define PG15_PORT G /**< GPIO port pin G15 is on port G */
/** get GPIO port identifier based on GPIO port and pin identifier */
#define GPIO_PIN_ID(x) CAT2(x,_PIN)
/** get GPIO pin based on GPIO port and pin identifier */
#define GPIO_PIN(x) GPIO(GPIO_PIN_ID(x))
#define PA0_PIN 0 /**< GPIO port pin A0 is on pin 0 */
#define PA1_PIN 1 /**< GPIO port pin A1 is on pin 1 */
#define PA2_PIN 2 /**< GPIO port pin A2 is on pin 2 */
#define PA3_PIN 3 /**< GPIO port pin A3 is on pin 3 */
#define PA4_PIN 4 /**< GPIO port pin A4 is on pin 4 */
#define PA5_PIN 5 /**< GPIO port pin A5 is on pin 5 */
#define PA6_PIN 6 /**< GPIO port pin A6 is on pin 6 */
#define PA7_PIN 7 /**< GPIO port pin A7 is on pin 7 */
#define PA8_PIN 8 /**< GPIO port pin A8 is on pin 8 */
#define PA9_PIN 9 /**< GPIO port pin A9 is on pin 9 */
#define PA10_PIN 10 /**< GPIO port pin A10 is on pin 10 */
#define PA11_PIN 11 /**< GPIO port pin A11 is on pin 11 */
#define PA12_PIN 12 /**< GPIO port pin A12 is on pin 12 */
#define PA13_PIN 13 /**< GPIO port pin A13 is on pin 13 */
#define PA14_PIN 14 /**< GPIO port pin A14 is on pin 14 */
#define PA15_PIN 15 /**< GPIO port pin A15 is on pin 15 */
#define PB0_PIN 0 /**< GPIO port pin B0 is on pin 0 */
#define PB1_PIN 1 /**< GPIO port pin B1 is on pin 1 */
#define PB2_PIN 2 /**< GPIO port pin B2 is on pin 2 */
#define PB3_PIN 3 /**< GPIO port pin B3 is on pin 3 */
#define PB4_PIN 4 /**< GPIO port pin B4 is on pin 4 */
#define PB5_PIN 5 /**< GPIO port pin B5 is on pin 5 */
#define PB6_PIN 6 /**< GPIO port pin B6 is on pin 6 */
#define PB7_PIN 7 /**< GPIO port pin B7 is on pin 7 */
#define PB8_PIN 8 /**< GPIO port pin B8 is on pin 8 */
#define PB9_PIN 9 /**< GPIO port pin B9 is on pin 9 */
#define PB10_PIN 10 /**< GPIO port pin B10 is on pin 10 */
#define PB11_PIN 11 /**< GPIO port pin B11 is on pin 11 */
#define PB12_PIN 12 /**< GPIO port pin B12 is on pin 12 */
#define PB13_PIN 13 /**< GPIO port pin B13 is on pin 13 */
#define PB14_PIN 14 /**< GPIO port pin B14 is on pin 14 */
#define PB15_PIN 15 /**< GPIO port pin B15 is on pin 15 */
#define PC0_PIN 0 /**< GPIO port pin C0 is on pin 0 */
#define PC1_PIN 1 /**< GPIO port pin C1 is on pin 1 */
#define PC2_PIN 2 /**< GPIO port pin C2 is on pin 2 */
#define PC3_PIN 3 /**< GPIO port pin C3 is on pin 3 */
#define PC4_PIN 4 /**< GPIO port pin C4 is on pin 4 */
#define PC5_PIN 5 /**< GPIO port pin C5 is on pin 5 */
#define PC6_PIN 6 /**< GPIO port pin C6 is on pin 6 */
#define PC7_PIN 7 /**< GPIO port pin C7 is on pin 7 */
#define PC8_PIN 8 /**< GPIO port pin C8 is on pin 8 */
#define PC9_PIN 9 /**< GPIO port pin C9 is on pin 9 */
#define PC10_PIN 10 /**< GPIO port pin C10 is on pin 10 */
#define PC11_PIN 11 /**< GPIO port pin C11 is on pin 11 */
#define PC12_PIN 12 /**< GPIO port pin C12 is on pin 12 */
#define PC13_PIN 13 /**< GPIO port pin C13 is on pin 13 */
#define PC14_PIN 14 /**< GPIO port pin C14 is on pin 14 */
#define PC15_PIN 15 /**< GPIO port pin C15 is on pin 15 */
#define PD0_PIN 0 /**< GPIO port pin D0 is on pin 0 */
#define PD1_PIN 1 /**< GPIO port pin D1 is on pin 1 */
#define PD2_PIN 2 /**< GPIO port pin D2 is on pin 2 */
#define PD3_PIN 3 /**< GPIO port pin D3 is on pin 3 */
#define PD4_PIN 4 /**< GPIO port pin D4 is on pin 4 */
#define PD5_PIN 5 /**< GPIO port pin D5 is on pin 5 */
#define PD6_PIN 6 /**< GPIO port pin D6 is on pin 6 */
#define PD7_PIN 7 /**< GPIO port pin D7 is on pin 7 */
#define PD8_PIN 8 /**< GPIO port pin D8 is on pin 8 */
#define PD9_PIN 9 /**< GPIO port pin D9 is on pin 9 */
#define PD10_PIN 10 /**< GPIO port pin D10 is on pin 10 */
#define PD11_PIN 11 /**< GPIO port pin D11 is on pin 11 */
#define PD12_PIN 12 /**< GPIO port pin D12 is on pin 12 */
#define PD13_PIN 13 /**< GPIO port pin D13 is on pin 13 */
#define PD14_PIN 14 /**< GPIO port pin D14 is on pin 14 */
#define PD15_PIN 15 /**< GPIO port pin D15 is on pin 15 */
#define PE0_PIN 0 /**< GPIO port pin E0 is on pin 0 */
#define PE1_PIN 1 /**< GPIO port pin E1 is on pin 1 */
#define PE2_PIN 2 /**< GPIO port pin E2 is on pin 2 */
#define PE3_PIN 3 /**< GPIO port pin E3 is on pin 3 */
#define PE4_PIN 4 /**< GPIO port pin E4 is on pin 4 */
#define PE5_PIN 5 /**< GPIO port pin E5 is on pin 5 */
#define PE6_PIN 6 /**< GPIO port pin E6 is on pin 6 */
#define PE7_PIN 7 /**< GPIO port pin E7 is on pin 7 */
#define PE8_PIN 8 /**< GPIO port pin E8 is on pin 8 */
#define PE9_PIN 9 /**< GPIO port pin E9 is on pin 9 */
#define PE10_PIN 10 /**< GPIO port pin E10 is on pin 10 */
#define PE11_PIN 11 /**< GPIO port pin E11 is on pin 11 */
#define PE12_PIN 12 /**< GPIO port pin E12 is on pin 12 */
#define PE13_PIN 13 /**< GPIO port pin E13 is on pin 13 */
#define PE14_PIN 14 /**< GPIO port pin E14 is on pin 14 */
#define PE15_PIN 15 /**< GPIO port pin E15 is on pin 15 */
#define PF0_PIN 0 /**< GPIO port pin F0 is on pin 0 */
#define PF1_PIN 1 /**< GPIO port pin F1 is on pin 1 */
#define PF2_PIN 2 /**< GPIO port pin F2 is on pin 2 */
#define PF3_PIN 3 /**< GPIO port pin F3 is on pin 3 */
#define PF4_PIN 4 /**< GPIO port pin F4 is on pin 4 */
#define PF5_PIN 5 /**< GPIO port pin F5 is on pin 5 */
#define PF6_PIN 6 /**< GPIO port pin F6 is on pin 6 */
#define PF7_PIN 7 /**< GPIO port pin F7 is on pin 7 */
#define PF8_PIN 8 /**< GPIO port pin F8 is on pin 8 */
#define PF9_PIN 9 /**< GPIO port pin F9 is on pin 9 */
#define PF10_PIN 10 /**< GPIO port pin F10 is on pin 10 */
#define PF11_PIN 11 /**< GPIO port pin F11 is on pin 11 */
#define PF12_PIN 12 /**< GPIO port pin F12 is on pin 12 */
#define PF13_PIN 13 /**< GPIO port pin F13 is on pin 13 */
#define PF14_PIN 14 /**< GPIO port pin F14 is on pin 14 */
#define PF15_PIN 15 /**< GPIO port pin F15 is on pin 15 */
#define PG0_PIN 0 /**< GPIO port pin G0 is on pin 0 */
#define PG1_PIN 1 /**< GPIO port pin G1 is on pin 1 */
#define PG2_PIN 2 /**< GPIO port pin G2 is on pin 2 */
#define PG3_PIN 3 /**< GPIO port pin G3 is on pin 3 */
#define PG4_PIN 4 /**< GPIO port pin G4 is on pin 4 */
#define PG5_PIN 5 /**< GPIO port pin G5 is on pin 5 */
#define PG6_PIN 6 /**< GPIO port pin G6 is on pin 6 */
#define PG7_PIN 7 /**< GPIO port pin G7 is on pin 7 */
#define PG8_PIN 8 /**< GPIO port pin G8 is on pin 8 */
#define PG9_PIN 9 /**< GPIO port pin G9 is on pin 9 */
#define PG10_PIN 10 /**< GPIO port pin G10 is on pin 10 */
#define PG11_PIN 11 /**< GPIO port pin G11 is on pin 11 */
#define PG12_PIN 12 /**< GPIO port pin G12 is on pin 12 */
#define PG13_PIN 13 /**< GPIO port pin G13 is on pin 13 */
#define PG14_PIN 14 /**< GPIO port pin G14 is on pin 14 */
#define PG15_PIN 15 /**< GPIO port pin G15 is on pin 15 */
/** get RCC for GPIO based on GPIO identifier */
#define RCC_GPIO(x) CAT2(RCC_GPIO,x)
/** get RCC for GPIO based on GPIO identifier */
#define GPIO_RCC(x) RCC_GPIO(GPIO_PORT_ID(x))
/** get RST for GPIO based on GPIO identifier */
#define RST_GPIO(x) CAT2(RST_GPIO,x)
/** get RST for GPIO based on GPIO identifier */
#define GPIO_RST(x) RST_GPIO(GPIO_PORT_ID(x))
/** get TIM based on TIM identifier */
#define TIM(x) CAT2(TIM,x)
/** get RCC for timer based on TIM identifier */
#define RCC_TIM(x) CAT2(RCC_TIM,x)
/** get RST for timer based on TIM identifier */
#define RST_TIM(x) CAT2(RST_TIM,x)
/** get NVIC IRQ for timer base on TIM identifier */
#define NVIC_TIM_IRQ(x) CAT3(NVIC_TIM,x,_IRQ)
/** get interrupt service routine for timer base on TIM identifier */
#define TIM_ISR(x) CAT3(tim,x,_isr)
/** get port based on TIMx_CHy identifier */
#define TIM_CH_PORT(x,y) CAT4(GPIO_BANK_TIM,x,_CH,y)
/** get pin based on TIMx_CHy identifier */
#define TIM_CH_PIN(x,y) CAT4(GPIO_TIM,x,_CH,y)
/** get RCC for port based on TIMx_CHy identifier */
#define RCC_TIM_CH(x,y) CAT4(RCC_TIM,x,_CH,y)
#define RCC_TIM1_CH1 RCC_GPIOA /**< RCC for port for on TIM1_CH1 */
#define RCC_TIM1_CH2 RCC_GPIOA /**< RCC for port for on TIM1_CH2 */
#define RCC_TIM1_CH3 RCC_GPIOA /**< RCC for port for on TIM1_CH3 */
#define RCC_TIM1_CH4 RCC_GPIOA /**< RCC for port for on TIM1_CH4 */
#define RCC_TIM1_CH1N RCC_GPIOB /**< RCC for port for on TIM1_CH1N */
#define RCC_TIM1_CH2N RCC_GPIOB /**< RCC for port for on TIM1_CH2N */
#define RCC_TIM1_CH3N RCC_GPIOB /**< RCC for port for on TIM1_CH3N */
#define RCC_TIM2_CH1_ETR RCC_GPIOA /**< RCC for port for on TIM2_CH1_ETR */
#define RCC_TIM2_CH2 RCC_GPIOA /**< RCC for port for on TIM2_CH2 */
#define RCC_TIM2_CH3 RCC_GPIOA /**< RCC for port for on TIM2_CH3 */
#define RCC_TIM2_CH4 RCC_GPIOA /**< RCC for port for on TIM2_CH4 */
#define RCC_TIM3_CH1 RCC_GPIOA /**< RCC for port for on TIM3_CH1 */
#define RCC_TIM3_CH2 RCC_GPIOA /**< RCC for port for on TIM3_CH2 */
#define RCC_TIM3_CH3 RCC_GPIOB /**< RCC for port for on TIM3_CH3 */
#define RCC_TIM3_CH4 RCC_GPIOB /**< RCC for port for on TIM3_CH4 */
#define RCC_TIM4_CH1 RCC_GPIOB /**< RCC for port for on TIM4_CH1 */
#define RCC_TIM4_CH2 RCC_GPIOB /**< RCC for port for on TIM4_CH2 */
#define RCC_TIM4_CH3 RCC_GPIOB /**< RCC for port for on TIM4_CH3 */
#define RCC_TIM4_CH4 RCC_GPIOB /**< RCC for port for on TIM4_CH4 */
#define RCC_TIM5_CH1 RCC_GPIOA /**< RCC for port for on TIM5_CH1 */
#define RCC_TIM5_CH2 RCC_GPIOA /**< RCC for port for on TIM5_CH2 */
#define RCC_TIM5_CH3 RCC_GPIOA /**< RCC for port for on TIM5_CH3 */
#define RCC_TIM5_CH4 RCC_GPIOA /**< RCC for port for on TIM5_CH4 */
/** get TIM_IC based on CHx identifier */
#define TIM_IC(x) CAT2(TIM_IC,x)
/** get TIM_IC_IN_TI based on CHx identifier */
#define TIM_IC_IN_TI(x) CAT2(TIM_IC_IN_TI,x)
/** get TIM_SR_CCxIF based on CHx identifier */
#define TIM_SR_CCIF(x) CAT3(TIM_SR_CC,x,IF)
/** get TIM_SR_CCxOF based on CHx identifier */
#define TIM_SR_CCOF(x) CAT3(TIM_SR_CC,x,OF)
/** get TIM_DIER_CCxIE based on CHx identifier */
#define TIM_DIER_CCIE(x) CAT3(TIM_DIER_CC,x,IE)
/** get TIM_CCRy register based on TIMx_CHy identifier */
#define TIM_CCR(x,y) CAT2(TIM_CCR,y)(TIM(x))
/** get external interrupt based on pin identifier */
#define EXTI(x) CAT2(EXTI,x)
/** get external interrupt based on GPIO port and pin identifier */
#define GPIO_EXTI(x) EXTI(GPIO_PIN_ID(x))
/** get NVIC IRQ for external interrupt based on external interrupt/pin */
#define NVIC_EXTI_IRQ(x) CAT3(NVIC_EXTI,x,_IRQ)
/** get NVIC IRQ for external interrupt based on GPIO port and pin identifier */
#define GPIO_NVIC_EXTI_IRQ(x) NVIC_EXTI_IRQ(GPIO_PIN_ID(x))
#define NVIC_EXTI5_IRQ NVIC_EXTI9_5_IRQ /**< IRQ for line 9 to 5 for pin 5 */
#define NVIC_EXTI6_IRQ NVIC_EXTI9_5_IRQ /**< IRQ for line 9 to 5 for pin 6 */
#define NVIC_EXTI7_IRQ NVIC_EXTI9_5_IRQ /**< IRQ for line 9 to 5 for pin 7 */
#define NVIC_EXTI8_IRQ NVIC_EXTI9_5_IRQ /**< IRQ for line 9 to 5 for pin 8 */
#define NVIC_EXTI9_IRQ NVIC_EXTI9_5_IRQ /**< IRQ for line 9 to 5 for pin 9 */
#define NVIC_EXTI10_IRQ NVIC_EXTI15_10_IRQ /**< IRQ for line 15 to 10 for pin 10 */
#define NVIC_EXTI11_IRQ NVIC_EXTI15_10_IRQ /**< IRQ for line 15 to 10 for pin 11 */
#define NVIC_EXTI12_IRQ NVIC_EXTI15_10_IRQ /**< IRQ for line 15 to 10 for pin 12 */
#define NVIC_EXTI13_IRQ NVIC_EXTI15_10_IRQ /**< IRQ for line 15 to 10 for pin 13 */
#define NVIC_EXTI14_IRQ NVIC_EXTI15_10_IRQ /**< IRQ for line 15 to 10 for pin 14 */
#define NVIC_EXTI15_IRQ NVIC_EXTI15_10_IRQ /**< IRQ for line 15 to 10 for pin 15 */
/** get interrupt service routine for timer base on external interrupt/pin */
#define EXTI_ISR(x) CAT3(exti,x,_isr)
/** get external interrupt based on GPIO port and pin identifier */
#define GPIO_EXTI_ISR(x) EXTI_ISR(GPIO_PIN_ID(x))
#define exti5_isr exti9_5_isr /**< ISR for line 9 to 5 for pin 5 */
#define exti6_isr exti9_5_isr /**< ISR for line 9 to 5 for pin 6 */
#define exti7_isr exti9_5_isr /**< ISR for line 9 to 5 for pin 7 */
#define exti8_isr exti9_5_isr /**< ISR for line 9 to 5 for pin 8 */
#define exti9_isr exti9_5_isr /**< ISR for line 9 to 5 for pin 9 */
#define exti10_isr exti15_10_isr /**< ISR for line 15 to 10 for pin 10 */
#define exti11_isr exti15_10_isr /**< ISR for line 15 to 10 for pin 11 */
#define exti12_isr exti15_10_isr /**< ISR for line 15 to 10 for pin 12 */
#define exti13_isr exti15_10_isr /**< ISR for line 15 to 10 for pin 13 */
#define exti14_isr exti15_10_isr /**< ISR for line 15 to 10 for pin 14 */
#define exti15_isr exti15_10_isr /**< ISR for line 15 to 10 for pin 15 */
/** get USART based on USART identifier */
#define USART(x) CAT2(USART,x)
/** get RCC for USART based on USART identifier */
#define RCC_USART(x) CAT2(RCC_USART,x)
/** get RST for USART based on USART identifier */
#define RST_USART(x) CAT2(RST_USART,x)
/** get NVIC IRQ for USART based on USART identifier */
#define USART_IRQ(x) CAT3(NVIC_USART,x,_IRQ)
/** get interrupt service routine for USART based on USART identifier */
#define USART_ISR(x) CAT3(usart,x,_isr)
/** get port for USART transmit pin based on USART identifier */
#define USART_TX_PORT(x) CAT3(GPIO_BANK_USART,x,_TX)
/** get port for USART receive pin based on USART identifier */
#define USART_RX_PORT(x) CAT3(GPIO_BANK_USART,x,_RX)
/** get port for USART RTS pin based on USART identifier */
#define USART_RTS_PORT(x) CAT3(GPIO_BANK_USART,x,_RTS)
/** get port for USART CTS pin based on USART identifier */
#define USART_CTS_PORT(x) CAT3(GPIO_BANK_USART,x,_CTS)
/** get pin for USART transmit pin based on USART identifier */
#define USART_TX_PIN(x) CAT3(GPIO_USART,x,_TX)
/** get pin for USART receive pin based on USART identifier */
#define USART_RX_PIN(x) CAT3(GPIO_USART,x,_RX)
/** get pin for USART RTS pin based on USART identifier */
#define USART_RTS_PIN(x) CAT3(GPIO_USART,x,_RTS)
/** get pin for USART CTS pin based on USART identifier */
#define USART_CTS_PIN(x) CAT3(GPIO_USART,x,_CTS)
/** get RCC for USART port based on USART identifier */
#define RCC_USART_PORT(x) CAT2(RCC_USART_PORT,x)
#define RCC_USART_PORT1 RCC_GPIOA /**< USART 1 is on port A */
#define RCC_USART_PORT2 RCC_GPIOA /**< USART 2 is on port A */
#define RCC_USART_PORT3 RCC_GPIOB /**< USART 3 is on port B */
/** get port based on ADC12_IN identifier */
#define ADC12_IN_PORT(x) CAT3(ADC12_IN,x,_PORT)
#define ADC12_IN0_PORT GPIOA /**< ADC12_IN0 is on PA0 */
#define ADC12_IN1_PORT GPIOA /**< ADC12_IN1 is on PA1 */
#define ADC12_IN2_PORT GPIOA /**< ADC12_IN2 is on PA2 */
#define ADC12_IN3_PORT GPIOA /**< ADC12_IN3 is on PA3 */
#define ADC12_IN4_PORT GPIOA /**< ADC12_IN4 is on PA4 */
#define ADC12_IN5_PORT GPIOA /**< ADC12_IN5 is on PA5 */
#define ADC12_IN6_PORT GPIOA /**< ADC12_IN6 is on PA6 */
#define ADC12_IN7_PORT GPIOA /**< ADC12_IN7 is on PA7 */
#define ADC12_IN8_PORT GPIOB /**< ADC12_IN8 is on PB0 */
#define ADC12_IN9_PORT GPIOB /**< ADC12_IN9 is on PB1 */
#define ADC12_IN10_PORT GPIOC /**< ADC12_IN10 is on PC0 */
#define ADC12_IN11_PORT GPIOC /**< ADC12_IN11 is on PC1 */
#define ADC12_IN12_PORT GPIOC /**< ADC12_IN12 is on PC2 */
#define ADC12_IN13_PORT GPIOC /**< ADC12_IN13 is on PC3 */
#define ADC12_IN14_PORT GPIOC /**< ADC12_IN14 is on PC4 */
#define ADC12_IN15_PORT GPIOC /**< ADC12_IN15 is on PC5 */
/** get pin based on ADC12_IN identifier */
#define ADC12_IN_PIN(x) CAT3(ADC12_IN,x,_PIN)
#define ADC12_IN0_PIN GPIO0 /**< ADC12_IN0 is on PA0 */
#define ADC12_IN1_PIN GPIO1 /**< ADC12_IN1 is on PA1 */
#define ADC12_IN2_PIN GPIO2 /**< ADC12_IN2 is on PA2 */
#define ADC12_IN3_PIN GPIO3 /**< ADC12_IN3 is on PA3 */
#define ADC12_IN4_PIN GPIO4 /**< ADC12_IN4 is on PA4 */
#define ADC12_IN5_PIN GPIO5 /**< ADC12_IN5 is on PA5 */
#define ADC12_IN6_PIN GPIO6 /**< ADC12_IN6 is on PA6 */
#define ADC12_IN7_PIN GPIO7 /**< ADC12_IN7 is on PA7 */
#define ADC12_IN8_PIN GPIO0 /**< ADC12_IN8 is on PB0 */
#define ADC12_IN9_PIN GPIO1 /**< ADC12_IN9 is on PB1 */
#define ADC12_IN10_PIN GPIO0 /**< ADC12_IN10 is on PC0 */
#define ADC12_IN11_PIN GPIO1 /**< ADC12_IN11 is on PC1 */
#define ADC12_IN12_PIN GPIO2 /**< ADC12_IN12 is on PC2 */
#define ADC12_IN13_PIN GPIO3 /**< ADC12_IN13 is on PC3 */
#define ADC12_IN14_PIN GPIO4 /**< ADC12_IN14 is on PC4 */
#define ADC12_IN15_PIN GPIO5 /**< ADC12_IN15 is on PC5 */
/** get RCC based on ADC12_IN identifier */
#define RCC_ADC12_IN(x) CAT2(RCC_ADC12_IN,x)
#define RCC_ADC12_IN0 RCC_GPIOA /**< ADC12_IN0 is on PA0 */
#define RCC_ADC12_IN1 RCC_GPIOA /**< ADC12_IN1 is on PA1 */
#define RCC_ADC12_IN2 RCC_GPIOA /**< ADC12_IN2 is on PA2 */
#define RCC_ADC12_IN3 RCC_GPIOA /**< ADC12_IN3 is on PA3 */
#define RCC_ADC12_IN4 RCC_GPIOA /**< ADC12_IN4 is on PA4 */
#define RCC_ADC12_IN5 RCC_GPIOA /**< ADC12_IN5 is on PA5 */
#define RCC_ADC12_IN6 RCC_GPIOA /**< ADC12_IN6 is on PA6 */
#define RCC_ADC12_IN7 RCC_GPIOA /**< ADC12_IN7 is on PA7 */
#define RCC_ADC12_IN8 RCC_GPIOB /**< ADC12_IN8 is on PB0 */
#define RCC_ADC12_IN9 RCC_GPIOB /**< ADC12_IN9 is on PB1 */
#define RCC_ADC12_IN10 RCC_GPIOC /**< ADC12_IN10 is on PC0 */
#define RCC_ADC12_IN11 RCC_GPIOC /**< ADC12_IN11 is on PC1 */
#define RCC_ADC12_IN12 RCC_GPIOC /**< ADC12_IN12 is on PC2 */
#define RCC_ADC12_IN13 RCC_GPIOC /**< ADC12_IN13 is on PC3 */
#define RCC_ADC12_IN14 RCC_GPIOC /**< ADC12_IN14 is on PC4 */
#define RCC_ADC12_IN15 RCC_GPIOC /**< ADC12_IN15 is on PC5 */
/** get channel based on ADC12_IN identifier */
#define ADC_CHANNEL(x) CAT2(ADC_CHANNEL,x)
/** get SPI based on SPI identifier */
#define SPI(x) CAT2(SPI,x)
/** get RCC for SPI based on SPI identifier */
#define RCC_SPI(x) CAT2(RCC_SPI,x)
/** get RCC for GPIO port for SPI NSS signals */
#define RCC_SPI_NSS_PORT(x) CAT3(RCC_SPI,x,_NSS_PORT)
#define RCC_SPI1_NSS_PORT RCC_GPIOA /**< RCC for GPIO port for NSS for SPI1 */
#define RCC_SPI1_RE_NSS_PORT RCC_GPIOA /**< RCC for GPIO port for NSS for SPI1_RE */
#define RCC_SPI2_NSS_PORT RCC_GPIOB /**< RCC for GPIO port for NSS for SPI2 */
/** get RCC for GPIO port for SPI SCK signals */
#define RCC_SPI_SCK_PORT(x) CAT3(RCC_SPI,x,_SCK_PORT)
#define RCC_SPI1_SCK_PORT RCC_GPIOA /**< RCC for GPIO port for NSS for SPI1 */
#define RCC_SPI1_RE_SCK_PORT RCC_GPIOB /**< RCC for GPIO port for NSS for SPI1_RE */
#define RCC_SPI2_SCK_PORT RCC_GPIOB /**< RCC for GPIO port for NSS for SPI2 */
/** get RCC for GPIO port for SPI MISO signals */
#define RCC_SPI_MISO_PORT(x) CAT3(RCC_SPI,x,_MISO_PORT)
#define RCC_SPI1_MISO_PORT RCC_GPIOA /**< RCC for GPIO port for NSS for SPI1 */
#define RCC_SPI1_RE_MISO_PORT RCC_GPIOB /**< RCC for GPIO port for NSS for SPI1_RE */
#define RCC_SPI2_MISO_PORT RCC_GPIOB /**< RCC for GPIO port for NSS for SPI2 */
/** get RCC for GPIO port for SPI MOSI signals */
#define RCC_SPI_MOSI_PORT(x) CAT3(RCC_SPI,x,_MOSI_PORT)
#define RCC_SPI1_MOSI_PORT RCC_GPIOA /**< RCC for GPIO port for NSS for SPI1 */
#define RCC_SPI1_RE_MOSI_PORT RCC_GPIOB /**< RCC for GPIO port for NSS for SPI1_RE */
#define RCC_SPI2_MOSI_PORT RCC_GPIOB /**< RCC for GPIO port for NSS for SPI2 */
/** get SPI port for NSS signal based on SPI identifier */
#define SPI_NSS_PORT(x) CAT3(GPIO_BANK_SPI,x,_NSS)
/** get SPI port for SCK signal based on SPI identifier */
#define SPI_SCK_PORT(x) CAT3(GPIO_BANK_SPI,x,_SCK)
/** get SPI port for MISO signal based on SPI identifier */
#define SPI_MISO_PORT(x) CAT3(GPIO_BANK_SPI,x,_MISO)
/** get SPI port for MOSI signal based on SPI identifier */
#define SPI_MOSI_PORT(x) CAT3(GPIO_BANK_SPI,x,_MOSI)
/** get SPI pin for NSS signal based on SPI identifier */
#define SPI_NSS_PIN(x) CAT3(GPIO_SPI,x,_NSS)
/** get SPI pin for SCK signal based on SPI identifier */
#define SPI_SCK_PIN(x) CAT3(GPIO_SPI,x,_SCK)
/** get SPI pin for MISO signal based on SPI identifier */
#define SPI_MISO_PIN(x) CAT3(GPIO_SPI,x,_MISO)
/** get SPI pin for MOSI signal based on SPI identifier */
#define SPI_MOSI_PIN(x) CAT3(GPIO_SPI,x,_MOSI)
/** get SPI CRC polynomial register based on SPI identifier */
#define SPI_CRC_PR(x) CAT3(SPI,x,_CRCPR)
/** get SPI CRC transmit register based on SPI identifier */
#define SPI_CRC_TXR(x) CAT3(SPI,x,_TXCRCR)
/** get SPI CRC receive register based on SPI identifier */
#define SPI_CRC_RXR(x) CAT3(SPI,x,_RXCRCR)
/** get SPI IRQ based on SPI identifier */
#define SPI_IRQ(x) CAT3(NVIC_SPI,x,_IRQ)
/** get SPI ISR based on SPI identifier */
#define SPI_ISR(x) CAT3(spi,x,_isr)
/** get DMA based on SPI identifier */
#define DMA_SPI(x) CAT2(DMA_SPI,x)
#define DMA_SPI1 DMA1 /**< SPI1 is on DMA1 */
#define DMA_SPI2 DMA1 /**< SPI2 is on DMA1 */
#define DMA_SPI3 DMA2 /**< SPI3 is on DMA2 */
/** get RCC for DMA based on SPI identifier */
#define RCC_DMA_SPI(x) CAT2(RCC_DMA_SPI,x)
#define RCC_DMA_SPI1 RCC_DMA1 /**< SPI1 is on DMA1 */
#define RCC_DMA_SPI2 RCC_DMA1 /**< SPI2 is on DMA1 */
#define RCC_DMA_SPI3 RCC_DMA2 /**< SPI3 is on DMA2 */
/** get DMA channel for SPI TX based on SPI identifier */
#define DMA_CHANNEL_SPI_TX(x) CAT3(DMA_CHANNEL_SPI,x,_TX)
#define DMA_CHANNEL_SPI1_TX DMA_CHANNEL3 /**< SPI1 TX is on DMA channel 3 */
#define DMA_CHANNEL_SPI2_TX DMA_CHANNEL5 /**< SPI2 TX is on DMA channel 5 */
#define DMA_CHANNEL_SPI3_TX DMA_CHANNEL2 /**< SPI3 TX is on DMA channel 2 */
/** get DMA channel for SPI RX based on SPI identifier */
#define DMA_CHANNEL_SPI_RX(x) CAT3(DMA_CHANNEL_SPI,x,_RX)
#define DMA_CHANNEL_SPI1_RX DMA_CHANNEL4 /**< SPI1 RX is on DMA channel 4 */
#define DMA_CHANNEL_SPI2_RX DMA_CHANNEL2 /**< SPI2 RX is on DMA channel 2 */
#define DMA_CHANNEL_SPI3_RX DMA_CHANNEL1 /**< SPI3 RX is on DMA channel 1 */
/** get DMA NVIC IRQ for SPI TX based on SPI identifier */
#define DMA_IRQ_SPI_TX(x) CAT3(NVIC_DMA_CHANNEL_IRQ_SPI,x,_TX)
#define NVIC_DMA_CHANNEL_IRQ_SPI1_TX NVIC_DMA1_CHANNEL3_IRQ /**< SPI1 TX is on DMA 1 channel 3 */
#define NVIC_DMA_CHANNEL_IRQ_SPI2_TX NVIC_DMA1_CHANNEL5_IRQ /**< SPI2 TX is on DMA 1 channel 5 */
#define NVIC_DMA_CHANNEL_IRQ_SPI3_TX NVIC_DMA2_CHANNEL2_IRQ /**< SPI3 TX is on DMA 2 channel 2 */
/** get DMA NVIC IRQ for SPI RX based on SPI identifier */
#define DMA_IRQ_SPI_RX(x) CAT3(NVIC_DMA_CHANNEL_IRQ_SPI,x,_RX)
#define NVIC_DMA_CHANNEL_IRQ_SPI1_RX NVIC_DMA1_CHANNEL4_IRQ /**< SPI1 RX is on DMA 1 channel 4 */
#define NVIC_DMA_CHANNEL_IRQ_SPI2_RX NVIC_DMA1_CHANNEL2_IRQ /**< SPI2 RX is on DMA 1 channel 2 */
#define NVIC_DMA_CHANNEL_IRQ_SPI3_RX NVIC_DMA2_CHANNEL1_IRQ /**< SPI3 RX is on DMA 2 channel 1 */
/** get DMA ISR for SPI TX based on SPI identifier */
#define DMA_ISR_SPI_TX(x) CAT3(DMA_CHANNEL_ISR_SPI,x,_TX)
#define DMA_CHANNEL_ISR_SPI1_TX dma1_channel3_isr /**< SPI1 TX is on DMA 1 channel 3 */
#define DMA_CHANNEL_ISR_SPI2_TX dma1_channel5_isr /**< SPI2 TX is on DMA 1 channel 5 */
#define DMA_CHANNEL_ISR_SPI3_TX dma2_channel2_isr /**< SPI3 TX is on DMA 2 channel 2 */
/** get DMA ISR for SPI RX based on SPI identifier */
#define DMA_ISR_SPI_RX(x) CAT3(DMA_CHANNEL_ISR_SPI,x,_RX)
#define DMA_CHANNEL_ISR_SPI1_RX dma1_channel4_isr /**< SPI1 RX is on DMA 1 channel 4 */
#define DMA_CHANNEL_ISR_SPI2_RX dma1_channel2_isr /**< SPI2 RX is on DMA 1 channel 2 */
#define DMA_CHANNEL_ISR_SPI3_RX dma2_channel1_isr /**< SPI3 RX is on DMA 2 channel 1 */
/** @} */

/** @defgroup board_led board LED GPIO
 *  @{
 */
#if defined(SYSTEM_BOARD) || defined(CORE_BOARD)
	#define LED_PIN PA1 /**< GPIO pin (pin 11) */
	#define LED_ON 0 /**< LED is on when pin is low */
#elif defined(BLUE_PILL)
	#define LED_PIN PC13 /**< GPIO pin */
	#define LED_ON 0 /**< LED is on when pin is low */
#elif defined(BLACK_PILL)
	#define LED_PIN PB12 /**< GPIO pin */
	#define LED_ON 0 /**< LED is on when pin is low */
#elif defined(MAPLE_MINI)
	#define LED_PIN PB1 /**< GPIO pin (pin 19) */
	#define LED_ON 1 /**< LED is on when pin is high */
#elif defined(STLINKV2) // it's sometimes a STM32F101, but it seems to have all STM32F103 features
	/* on ST-Link V2 clone dongle in aluminum case LED is on pin PA9 (remap USART1_TX if used) */
	#define LED_PIN PA9 /**< GPIO pin */
	#define LED_ON 1 /**< the color and on level depends on the clone */
#elif defined(BLASTER)
	#define LED_PIN PA5 /**< GPIO pin */
	#define LED_ON 0 /**< red LED on when low (green LED is on when device is powered) */
#elif defined(BUSVOODOO)
	#define LED_PIN PA8 /**< GPIO pin */
	#define LED_ON 1 /**< blue LED is on when pin is high, red LED is on when pin is low, LED is off when LED is floating */
#endif
/** @} */

/** @defgroup board_button board user button GPIO
 *  @{
 */
#if defined(MAPLE_MINI)
	/* on maple mini user button is on 32/PB8 */
	#define BUTTON_PIN PB8 /**< GPIO pin (pin PB8 on maple mini) */
	#define BUTTON_PRESSED 1 /**< pin is high when button is pressed */
#elif defined(CORE_BOARD)
	/* on core board user button is on PA8 */
	#define BUTTON_PIN PA8 /**< GPIO pin (pin PA8) */
	#define BUTTON_PRESSED 0 /**< pin is low when button is pressed */
#endif
/** @} */

/** @defgroup input to force DFU mode on low, even if application is valid
 *  @{
 */
#if defined(BUTTON_PIN) && defined(BUTTON_PRESSED)
	#define DFU_FORCE_PIN BUTTON_PIN /**< button pin */
	#define DFU_FORCE_VALUE BUTTON_PRESSED /**< start DFU when button is pressed on boot */
#elif defined(BLASTER)
	#define DFU_FORCE_PIN PA8 /**< GPIO pin (pin PA8, not SWD and UART pin on debug port) */
	#define DFU_FORCE_VALUE 0 /**< short to nearby ground connection to force DFU */
#elif defined(BUSVOODOO)
#if BUSVOODOO_HARDWARE_VERSION==0
	/* on BusVoodoo v0 DFU input is on PC7 */
	#define DFU_FORCE_PIN PC7 /**< GPIO pin (pin PC7) */
	#define DFU_FORCE_VALUE 1 /**< pin is pulled low, and goes high when shorted with VUSB */
#else
	/* on BusVoodoo vA DFU input is on PC4 */
	#define DFU_FORCE_PIN PC4 /**< GPIO pin (pin PC4) */
	#define DFU_FORCE_VALUE 1 /**< pin floating, set high when shorted with nearby VCC */
#endif
#else
	#define DFU_FORCE_PIN PB2 /**< BOOT1 pin */
	#define DFU_FORCE_VALUE 1 /**< often connected to 0 by default to start system memory when BOOT0 = 1 */
#endif
/** @} */

/** symbol for beginning of the application
 *  @note this symbol will be provided by the linker script
 */
extern uint32_t __application_beginning;
/** symbol for end of the application
 *  @note this symbol will be provided by the linker script
 */
extern char __application_end;
/** symbol for end of flash
 *  @note this symbol will be provided by the linker script
 */
extern char __flash_end;
/** flag set when board user button has been pressed/released */
extern volatile bool button_flag;
/** symbol for the DFU magic word
 *  @note this symbol will be provided by the linker script
 */
extern char __dfu_magic[4];

/** flag set when user input is available */
extern volatile bool user_input_available;

/** get binary representation of a number
 *  @param[in] binary number to represent in binary
 *  @param[in] rjust justify representation with leading zeros
 *  @return string with binary representation of the number
 */
char* b2s(uint64_t binary, uint8_t rjust);
/** switch on board LED */
void led_on(void);
/** switch off board LED */
void led_off(void);
/** toggle board LED */
void led_toggle(void);
/** go to sleep for some microseconds
 *  @param[in] duration sleep duration in us
 */
void sleep_us(uint32_t duration);
/** go to sleep for some milliseconds
 *  @param[in] duration sleep duration in ms
 */
void sleep_ms(uint32_t duration);
/** get user input
 *  @note verify availability using user_input_available
 *  @warning blocks and sleeps until user input is available
 *  @return user input character
 */
char user_input_get(void);
/** store user input
 *  @param[in] c user input character to store
 *  @note old data will be overwritten when buffer is full
 */
void user_input_store(char c);
/** setup board peripherals */
void board_setup(void);

