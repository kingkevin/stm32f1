/** library to read/write internal flash
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2020
 *  @note peripherals used: none
 */
#pragma once

/** read data from internal flash
 *  @param[in] address start address of the data to read
 *  @param[out] buffer where to store the read data
 *  @param[in] size how much data to read, in bytes
 *  @return if read succeeded
 */
bool flash_internal_read(uint32_t address, uint8_t *buffer, size_t size);
/** write data to internal flash
 *  @param[in] address start address where to write data to
 *  @param[in] buffer data to be written
 *  @param[in] size how much data to write, in bytes
 *  @param[in] preserve keep the rest of the page if data needs to be erased
 *  @return number of bytes written (including preserved data), or negative in case of error
 *  @note the page will be erased if needed to write the data to the flash
 */
int32_t flash_internal_write(uint32_t address, const uint8_t *buffer, size_t size, bool preserve);
/** get flash page size
 *  @return flash page size (in bytes)
 */
uint16_t flash_internal_page_size(void);

/** setup the emulated EEPROM area
 *  @param[in] pages number of flash pages to allocate for the emulated EEPROM
 *  @warn area must be at least 4 bytes larger than the structure to write
 */
void flash_internal_eeprom_setup(uint16_t pages);
/** read emulated EEPROM area
 *  @param[out] eeprom where to store the EEPROM data
 *  @param[in] size size of the EEPROM area (in bytes)
 */
bool flash_internal_eeprom_read(uint8_t *eeprom, uint16_t size);
/** write emulated EEPROM area
 *  @param[in] eeprom EEPROM data to be stored
 *  @param[in] size size of the EEPROM area (in bytes)
 *  @return number of bytes written (including preserved data), or negative in case of error
 */
int32_t flash_internal_eeprom_write(const uint8_t *eeprom, uint16_t size);
/** probe the readable size of the internal flash
 *  @return tested size (in bytes)
 */
uint32_t flash_internal_probe_read_size(void);
/** probe the additional writable size of the internal flash, after the advertised size (and linker provided)
 *  @return tested size (in bytes)
 */
uint32_t flash_internal_probe_write_size(void);
