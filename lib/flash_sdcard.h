/** library to communicate with an SD card flash memory using the SPI mode
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2017
 *  @note peripherals used: SPI @ref flash_sdcard_spi
 *  @warning all calls are blocking
 */
#pragma once

/** setup communication with SD card
 *  @return if card has been initialized correctly
 */
bool flash_sdcard_setup(void);
/** get size of SD card flash memory
 *  @return size of SD card flash memory (in bytes)
 */
uint64_t flash_sdcard_size(void);
/** get size of a erase block
 *  @return size of a erase block (in bytes)
 */
uint32_t flash_sdcard_erase_size(void);
/** read data on flash of SD card
 *  @param[in] block address of data to read (in block in 512 bytes unit)
 *  @param[out] data data block to read (with a size of 512 bytes)
 *  @return if read succeeded
 */
bool flash_sdcard_read_data(uint32_t block, uint8_t* data);
/** write data on flash of SD card
 *  @param[in] block address of data to write (in block in 512 bytes unit)
 *  @param[in] data data block to write (with a size of 512 bytes)
 *  @return if write succeeded
 */
bool flash_sdcard_write_data(uint32_t block, uint8_t* data);
