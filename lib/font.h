/** monospace pixel fonts collection (API)
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2018
 */
#pragma once

/** list of available font names */
enum font_name {
	FONT_KING8, /**< custom 8x5 monospace font */
	FONT_KING10, /**< custom 10x6 monospace font */
	FONT_KING14, /**< custom 14x9 monospace font */
	FONT_MAX, /**< number of fonts available */
};

/** font structure containing all properties */
struct font_s {
	uint8_t width; /**< font width in pixels */
	uint8_t height; /**< font height in pixels (max 16) */
	const uint16_t* glyphs; /**< font glyphs: width glyph columns (left to right) times FONT_GLYPH_NUMBERS (MSb is glyph top pixel) */
};

/** number of available glyphs (starting with ' ' and ending with '~') */
#define FONT_GLYPH_NUMBERS 95

/** list of all available fonts */
extern const struct font_s fonts[FONT_MAX];
