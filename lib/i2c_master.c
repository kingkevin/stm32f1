/** library to communicate using I²C as master (code)
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2017-2020
 *  @note peripherals used: I2C
 */

/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdlib.h> // general utilities

/* STM32 (including CM3) libraries */
#include <libopencm3/cm3/systick.h> // SysTick library
#include <libopencm3/cm3/assert.h> // assert utilities
#include <libopencm3/stm32/rcc.h> // real-time control clock library
#include <libopencm3/stm32/gpio.h> // general purpose input output library
#include <libopencm3/stm32/i2c.h> // I²C library

/* own libraries */
#include "global.h" // global utilities
#include "i2c_master.h" // I²C header and definitions

/** get RCC for I²C based on I²C identifier
 *  @param[in] i2c I²C base address
 *  @return RCC address for I²C peripheral
 */
static uint32_t RCC_I2C(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	switch (i2c) {
	case I2C1:
		return RCC_I2C1;
		break;
	case I2C2:
		return RCC_I2C2;
		break;
	default:
		return 0;
	}
}

/** get RCC for GPIO port for SCL pin based on I²C identifier
 *  @param[in] i2c I²C base address
 *  @return RCC GPIO address
 */
static uint32_t RCC_GPIO_PORT_SCL(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	switch (i2c) {
	case I2C1:
	case I2C2:
		return RCC_GPIOB;
		break;
	default:
		return 0;
	}
}

/** get RCC for GPIO port for SDA pin based on I²C identifier
 *  @param[in] i2c I²C base address
 *  @return RCC GPIO address
 */
static uint32_t RCC_GPIO_PORT_SDA(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	switch (i2c) {
	case I2C1:
	case I2C2:
		return RCC_GPIOB;
		break;
	default:
		return 0;
	}
}

/** get GPIO port for SCL pin based on I²C identifier
 *  @param[in] i2c I²C base address
 *  @return GPIO address
 */
static uint32_t GPIO_PORT_SCL(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	switch (i2c) {
	case I2C1:
		if (AFIO_MAPR & AFIO_MAPR_I2C1_REMAP) {
			return GPIO_BANK_I2C1_RE_SCL;
		} else {
			return GPIO_BANK_I2C1_SCL;
		}
		break;
	case I2C2:
		return GPIO_BANK_I2C2_SCL;
		break;
	default:
		return 0;
	}
}

/** get GPIO port for SDA pin based on I²C identifier
 *  @param[in] i2c I²C base address
 *  @return GPIO address
 */
static uint32_t GPIO_PORT_SDA(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	switch (i2c) {
	case I2C1:
		if (AFIO_MAPR & AFIO_MAPR_I2C1_REMAP) {
			return GPIO_BANK_I2C1_RE_SDA;
		} else {
			return GPIO_BANK_I2C1_SDA;
		}
		break;
	case I2C2:
		return GPIO_BANK_I2C2_SDA;
		break;
	default:
		return 0;
	}
}

/** get GPIO pin for SCL pin based on I²C identifier
 *  @param[in] i2c I²C base address
 *  @return GPIO address
 */
static uint32_t GPIO_PIN_SCL(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	switch (i2c) {
	case I2C1:
		if (AFIO_MAPR & AFIO_MAPR_I2C1_REMAP) {
			return GPIO_I2C1_RE_SCL;
		} else {
			return GPIO_I2C1_SCL;
		}
		break;
	case I2C2:
		return GPIO_I2C2_SCL;
		break;
	default:
		return 0;
	}
}

/** get GPIO pin for SDA pin based on I²C identifier
 *  @param[in] i2c I²C base address
 *  @return GPIO address
 */
static uint32_t GPIO_PIN_SDA(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	switch (i2c) {
	case I2C1:
		if (AFIO_MAPR & AFIO_MAPR_I2C1_REMAP) {
			return GPIO_I2C1_RE_SDA;
		} else {
			return GPIO_I2C1_SDA;
		}
		break;
	case I2C2:
		return GPIO_I2C2_SDA;
		break;
	default:
		return 0;
	}
}

void i2c_master_setup(uint32_t i2c, uint16_t frequency)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	// configure I²C peripheral
	rcc_periph_clock_enable(RCC_GPIO_PORT_SCL(i2c)); // enable clock for I²C I/O peripheral
	gpio_set(GPIO_PORT_SCL(i2c), GPIO_PIN_SCL(i2c)); // already put signal high to avoid small pulse
	gpio_set_mode(GPIO_PORT_SCL(i2c), GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN, GPIO_PIN_SCL(i2c)); // setup I²C I/O pins
	rcc_periph_clock_enable(RCC_GPIO_PORT_SDA(i2c)); // enable clock for I²C I/O peripheral
	gpio_set(GPIO_PORT_SDA(i2c), GPIO_PIN_SDA(i2c)); // already put signal high to avoid small pulse
	gpio_set_mode(GPIO_PORT_SDA(i2c), GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN, GPIO_PIN_SDA(i2c)); // setup I²C I/O pins
	rcc_periph_clock_enable(RCC_AFIO); // enable clock for alternate function
	rcc_periph_clock_enable(RCC_I2C(i2c)); // enable clock for I²C peripheral
	i2c_reset(i2c); // reset peripheral domain
	i2c_peripheral_disable(i2c); // I²C needs to be disable to be configured
	I2C_CR1(i2c) |= I2C_CR1_SWRST; // reset peripheral
	I2C_CR1(i2c) &= ~I2C_CR1_SWRST; // clear peripheral reset
	if (0==frequency) { // don't allow null frequency
		frequency = 1;
	} else if (frequency > 400) { // limit frequency to 400 kHz
		frequency = 400;
	}
	i2c_set_clock_frequency(i2c, rcc_apb1_frequency / 1000000); // configure the peripheral clock to the APB1 freq (where it is connected to)
	if (frequency>100) { // use fast mode for frequencies over 100 kHz
		i2c_set_fast_mode(i2c); // set fast mode (Fm)
		i2c_set_ccr(i2c, rcc_apb1_frequency / (frequency * 1000 * 2)); // set Thigh/Tlow to generate frequency (fast duty not used)
		i2c_set_trise(i2c, (300 / (1000 / (rcc_apb1_frequency / 1000000))) + 1); // max rise time for Fm mode (< 400) kHz is 300 ns
	} else {  // use fast mode for frequencies below 100 kHz
		i2c_set_standard_mode(i2c); // set standard mode (Sm)
		i2c_set_ccr(i2c, rcc_apb1_frequency / (frequency * 1000 * 2)); // set Thigh/Tlow to generate frequency of 100 kHz
		i2c_set_trise(i2c, (1000 / (1000 / (rcc_apb1_frequency / 1000000))) + 1); // max rise time for Sm mode (< 100 kHz) is 1000 ns (~1 MHz)
	}
	i2c_peripheral_enable(i2c); // enable I²C after configuration completed
}

void i2c_master_release(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	i2c_reset(i2c); // reset I²C peripheral configuration
	i2c_peripheral_disable(i2c); // disable I²C peripheral
	rcc_periph_clock_disable(RCC_I2C(i2c)); // disable clock for I²C peripheral
	gpio_set_mode(GPIO_PORT_SCL(i2c), GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO_PIN_SCL(i2c)); // put I²C I/O pins back to floating
	gpio_set_mode(GPIO_PORT_SDA(i2c), GPIO_MODE_INPUT, GPIO_CNF_INPUT_FLOAT, GPIO_PIN_SDA(i2c)); // put I²C I/O pins back to floating
}

bool i2c_master_check_signals(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	// enable GPIOs to read SDA and SCL
	rcc_periph_clock_enable(RCC_GPIO_PORT_SDA(i2c)); // enable clock for I²C I/O peripheral
	rcc_periph_clock_enable(RCC_GPIO_PORT_SCL(i2c)); // enable clock for I²C I/O peripheral

	// pull SDA and SDC low to check if there are pull-up resistors
	uint32_t sda_crl = GPIO_CRL(GPIO_PORT_SDA(i2c)); // backup port configuration
	uint32_t sda_crh = GPIO_CRH(GPIO_PORT_SDA(i2c)); // backup port configuration
	uint32_t sda_bsrr = GPIO_BSRR(GPIO_PORT_SDA(i2c)); // backup port configuration
	uint32_t scl_crl = GPIO_CRL(GPIO_PORT_SCL(i2c)); // backup port configuration
	uint32_t scl_crh = GPIO_CRH(GPIO_PORT_SCL(i2c)); // backup port configuration
	uint32_t scl_bsrr = GPIO_BSRR(GPIO_PORT_SCL(i2c)); // backup port configuration
	gpio_set_mode(GPIO_PORT_SDA(i2c), GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_PIN_SDA(i2c)); // configure signal as pull down
	gpio_set_mode(GPIO_PORT_SCL(i2c), GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, GPIO_PIN_SCL(i2c)); // configure signal as pull down
	gpio_clear(GPIO_PORT_SDA(i2c), GPIO_PIN_SDA(i2c)); // pull down
	gpio_clear(GPIO_PORT_SCL(i2c), GPIO_PIN_SCL(i2c)); // pull down
	bool to_return = (0 != gpio_get(GPIO_PORT_SCL(i2c), GPIO_PIN_SCL(i2c)) && 0 != gpio_get(GPIO_PORT_SDA(i2c), GPIO_PIN_SDA(i2c))); // check if the signals are still pulled high by external stronger pull-up resistors
	GPIO_CRL(GPIO_PORT_SDA(i2c)) = sda_crl; // restore port configuration
	GPIO_CRH(GPIO_PORT_SDA(i2c)) = sda_crh; // restore port configuration
	GPIO_BSRR(GPIO_PORT_SDA(i2c)) = sda_bsrr; // restore port configuration
	GPIO_CRL(GPIO_PORT_SCL(i2c)) = scl_crl; // restore port configuration
	GPIO_CRH(GPIO_PORT_SCL(i2c)) = scl_crh; // restore port configuration
	GPIO_BSRR(GPIO_PORT_SCL(i2c)) = scl_bsrr; // restore port configuration

	return to_return;
}

bool i2c_master_reset(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	bool to_return = true;
	// follow procedure described in STM32F10xxC/D/E Errata sheet, Section 2.14.7
	i2c_peripheral_disable(i2c); // disable I²C peripheral
	gpio_set_mode(GPIO_PORT_SCL(i2c), GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_OPENDRAIN, GPIO_PIN_SCL(i2c)); // put I²C I/O pins to general output
	gpio_set(GPIO_PORT_SCL(i2c), GPIO_PIN_SCL(i2c)); // set high
	to_return &= !gpio_get(GPIO_PORT_SCL(i2c), GPIO_PIN_SCL(i2c)); // ensure it is high
	gpio_set_mode(GPIO_PORT_SDA(i2c), GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_OPENDRAIN, GPIO_PIN_SDA(i2c)); // put I²C I/O pins to general output
	gpio_set(GPIO_PORT_SDA(i2c), GPIO_PIN_SDA(i2c)); // set high
	to_return &= !gpio_get(GPIO_PORT_SDA(i2c), GPIO_PIN_SDA(i2c)); // ensure it is high
	gpio_clear(GPIO_PORT_SDA(i2c), GPIO_PIN_SDA(i2c)); // set low (try first transition)
	to_return &= gpio_get(GPIO_PORT_SDA(i2c), GPIO_PIN_SDA(i2c)); // ensure it is low
	gpio_clear(GPIO_PORT_SCL(i2c), GPIO_PIN_SCL(i2c)); // set low (try first transition)
	to_return &= gpio_get(GPIO_PORT_SCL(i2c), GPIO_PIN_SCL(i2c)); // ensure it is low
	gpio_set(GPIO_PORT_SCL(i2c), GPIO_PIN_SCL(i2c)); // set high (try second transition)
	to_return &= !gpio_get(GPIO_PORT_SCL(i2c), GPIO_PIN_SCL(i2c)); // ensure it is high
	gpio_set(GPIO_PORT_SDA(i2c), GPIO_PIN_SDA(i2c)); // set high (try second transition)
	to_return &= !gpio_get(GPIO_PORT_SDA(i2c), GPIO_PIN_SDA(i2c)); // ensure it is high
	gpio_set_mode(GPIO_PORT_SCL(i2c), GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN, GPIO_PIN_SCL(i2c)); // set I²C I/O pins back
	gpio_set_mode(GPIO_PORT_SDA(i2c), GPIO_MODE_OUTPUT_10_MHZ, GPIO_CNF_OUTPUT_ALTFN_OPENDRAIN, GPIO_PIN_SDA(i2c)); // set I²C I/O pins back
	I2C_CR1(i2c) |= I2C_CR1_SWRST; // reset device
	I2C_CR1(i2c) &= ~I2C_CR1_SWRST; // reset device
	i2c_peripheral_enable(i2c); // re-enable device

	return to_return;
}

enum i2c_master_rc i2c_master_start(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	bool retry = true; // retry after reset if first try failed
	enum i2c_master_rc to_return; // return code
	uint16_t sr1; // read register once, since reading/writing other registers or other events clears some flags
try:
	to_return = I2C_MASTER_RC_NONE; // return code
	// send (re-)start condition
	if (I2C_CR1(i2c) & (I2C_CR1_START | I2C_CR1_STOP)) { // ensure start or stop operations are not in progress
		return I2C_MASTER_RC_START_STOP_IN_PROGESS;
	}
	// prepare timer in case the peripheral hangs on sending stop condition (see errata 2.14.4 Wrong behavior of I²C peripheral in master mode after a misplaced Stop)
	systick_counter_disable(); // disable SysTick to reconfigure it
	systick_set_frequency(500, rcc_ahb_frequency); // set timer to 2 ms (that should be long enough to send a start condition)
	systick_clear(); // reset SysTick (set to 0)
	systick_interrupt_disable(); // disable interrupt to prevent ISR to read the flag
	systick_get_countflag(); // reset flag (set when counter is going for 1 to 0)
	i2c_send_start(i2c); // send start condition to start transaction
	bool timeout = false; // remember if the timeout has been reached
	systick_counter_enable(); // start timer
	while ((I2C_CR1(i2c) & I2C_CR1_START) && !((sr1 = I2C_SR1(i2c)) & (I2C_SR1_BERR | I2C_SR1_ARLO)) && !timeout) { // wait until start condition has been accepted and cleared
		timeout |= systick_get_countflag(); // verify if timeout has been reached
	}
	sr1 = I2C_SR1(i2c); // be sure to get the current value
	if (sr1 & (I2C_SR1_BERR | I2C_SR1_ARLO)) {
		to_return = I2C_MASTER_RC_BUS_ERROR;
	}
	while (!((sr1 = I2C_SR1(i2c)) & (I2C_SR1_SB | I2C_SR1_BERR | I2C_SR1_ARLO)) && !timeout && I2C_MASTER_RC_NONE == to_return) { // wait until start condition is transmitted
		timeout |= systick_get_countflag(); // verify if timeout has been reached
	}
	sr1 = I2C_SR1(i2c); // be sure to get the current value
	if (sr1 & (I2C_SR1_BERR|I2C_SR1_ARLO)) {
		to_return = I2C_MASTER_RC_BUS_ERROR;
	} else if (!(sr1 & I2C_SR1_SB)) { // the start bit has not been set although we the peripheral is not busy anymore
		to_return = I2C_MASTER_RC_BUS_ERROR;
	} else if (!(sr1 & I2C_SR2_MSL)) { // verify if in master mode
		to_return = I2C_MASTER_RC_NOT_MASTER;
	} else if (timeout) { // timeout has been reached, i.e. the peripheral hangs
		to_return = I2C_MASTER_RC_NOT_MASTER;
	}

	if (I2C_MASTER_RC_NOT_MASTER == to_return && retry) { // error happened
		retry = false; // don't retry a second time
		I2C_CR1(i2c) |= I2C_CR1_SWRST; // assert peripheral reset
		I2C_CR1(i2c) &= ~I2C_CR1_SWRST; // release peripheral reset
		goto try;
	}
	systick_counter_disable(); // we don't need to timer anymore
	return to_return;
}

/** wait until stop is sent and bus is released
 *  @param[in] i2c I²C base address
 *  @return I²C return code
 */
static enum i2c_master_rc i2c_master_wait_stop(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	enum i2c_master_rc to_return = I2C_MASTER_RC_NONE; // return code
	// prepare timer in case the peripheral hangs on sending stop condition (see errata 2.14.4 Wrong behavior of I²C peripheral in master mode after a misplaced Stop)
	systick_counter_disable(); // disable SysTick to reconfigure it
	systick_set_frequency(500, rcc_ahb_frequency); // set timer to 2 ms (that should be long enough to send a stop condition)
	systick_clear(); // reset SysTick (set to 0)
	systick_interrupt_disable(); // disable interrupt to prevent ISR to read the flag
	systick_get_countflag(); // reset flag (set when counter is going for 1 to 0)
	bool timeout = false; // remember if the timeout has been reached
	systick_counter_enable(); // start timer
	while ((I2C_CR1(i2c) & I2C_CR1_STOP) && !(I2C_SR1(i2c) & (I2C_SR1_BERR | I2C_SR1_ARLO)) && !timeout) { // wait until stop condition is accepted and cleared
		timeout |= systick_get_countflag(); // verify if timeout has been reached
	}
	if (I2C_SR1(i2c) & (I2C_SR1_BERR | I2C_SR1_ARLO)) {
		to_return = I2C_MASTER_RC_BUS_ERROR;
	}
	while ((I2C_SR2(i2c) & I2C_SR2_MSL) && !(I2C_SR1(i2c) & (I2C_SR1_BERR | I2C_SR1_ARLO)) && !timeout) { // wait until bus released (non master mode)
		timeout |= systick_get_countflag(); // verify if timeout has been reached
	}
	if (I2C_SR1(i2c) & (I2C_SR1_BERR | I2C_SR1_ARLO)) {
		to_return = I2C_MASTER_RC_BUS_ERROR;
	}
	while ((I2C_SR2(i2c) & I2C_SR2_BUSY) && !(I2C_SR1(i2c) & (I2C_SR1_BERR)) && !timeout) { // wait until peripheral is not busy anymore
		timeout |= systick_get_countflag(); // verify if timeout has been reached
	}
	if (I2C_SR1(i2c) & (I2C_SR1_BERR | I2C_SR1_ARLO)) {
		to_return = I2C_MASTER_RC_BUS_ERROR;
	}
	while ((0 == gpio_get(GPIO_PORT_SCL(i2c), GPIO_PIN_SCL(i2c)) || 0 == gpio_get(GPIO_PORT_SDA(i2c), GPIO_PIN_SDA(i2c))) && !timeout) { // wait until lines are really high again
		timeout |= systick_get_countflag(); // verify if timeout has been reached
	}

	if (timeout) { // I2C_CR1_STOP could also be used to detect a timeout, but I'm not sure when
		if (I2C_MASTER_RC_NONE == to_return) {
			to_return = I2C_MASTER_RC_TIMEOUT; // indicate timeout only when no more specific error has occurred
		}
		I2C_CR1(i2c) |= I2C_CR1_SWRST; // assert peripheral reset
		I2C_CR1(i2c) &= ~I2C_CR1_SWRST; // release peripheral reset
	}
	systick_counter_disable(); // we don't need to timer anymore
	return to_return;
}

enum i2c_master_rc i2c_master_stop(uint32_t i2c)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	// sanity check
	if (!(I2C_SR2(i2c) & I2C_SR2_BUSY)) { // release is not busy
		return I2C_MASTER_RC_NONE; // bus has probably already been released
	}
	if (I2C_CR1(i2c) & (I2C_CR1_START | I2C_CR1_STOP)) { // ensure start or stop operations are not in progress
		return I2C_MASTER_RC_START_STOP_IN_PROGESS;
	}

	if (!((I2C_SR2(i2c) & I2C_SR2_TRA))) { // if we are in receiver mode
		i2c_disable_ack(i2c); // disable ACK to be able to close the communication
	}

	i2c_send_stop(i2c); // send stop to release bus
	return i2c_master_wait_stop(i2c);
}

enum i2c_master_rc i2c_master_select_slave(uint32_t i2c, uint16_t slave, bool address_10bit, bool write)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	enum i2c_master_rc rc = I2C_MASTER_RC_NONE; // to store I²C return codes
	uint16_t sr1, sr2; // read register once, since reading/writing other registers or other events clears some flags

	if (!((sr1 = I2C_SR1(i2c)) & I2C_SR1_SB)) { // start condition has not been sent
		rc = i2c_master_start(i2c); // send start condition
		if (I2C_MASTER_RC_NONE != rc) {
			return rc;
		}
	}
	if (!((sr2 = I2C_SR2(i2c)) & I2C_SR2_MSL)) { // I²C device is not in master mode
		return I2C_MASTER_RC_NOT_MASTER;
	}

	// select slave
	I2C_SR1(i2c) &= ~(I2C_SR1_AF); // clear acknowledgement failure
	if (!address_10bit) { // 7-bit address
		i2c_send_7bit_address(i2c, slave, write ? I2C_WRITE : I2C_READ); // select slave, with read/write flag
		while (!((sr1 = I2C_SR1(i2c)) & (I2C_SR1_ADDR | I2C_SR1_AF | I2C_SR1_BERR | I2C_SR1_ARLO))); // wait until address is transmitted
		if (sr1 & (I2C_SR1_BERR | I2C_SR1_ARLO)) {
			return I2C_MASTER_RC_BUS_ERROR;
		}
		if (sr1 & I2C_SR1_AF) { // address has not been acknowledged
			return I2C_MASTER_RC_NAK;
		}
	} else { // 10-bit address
		// send first part of address
		I2C_DR(i2c) = 11110000 | (((slave >> 8 ) & 0x3) << 1); // send first header (11110xx0, where xx are 2 MSb of slave address)
		while (!((sr1 = I2C_SR1(i2c)) & (I2C_SR1_ADD10 | I2C_SR1_AF | I2C_SR1_BERR | I2C_SR1_ARLO))); // wait until first part of address is transmitted
		if (sr1 & (I2C_SR1_BERR | I2C_SR1_ARLO)) {
			return I2C_MASTER_RC_BUS_ERROR;
		}
		if (sr1 & I2C_SR1_AF) { // address has not been acknowledged
			return I2C_MASTER_RC_NAK;
		}
		// send second part of address
		I2C_SR1(i2c) &= ~(I2C_SR1_AF); // clear acknowledgement failure
		I2C_DR(i2c) = (slave & 0xff); // send remaining of address
		while (!((sr1 = I2C_SR1(i2c)) & (I2C_SR1_ADDR | I2C_SR1_AF | I2C_SR1_BERR | I2C_SR1_ARLO))); // wait until remaining part of address is transmitted
		if (sr1 & (I2C_SR1_BERR | I2C_SR1_ARLO)) {
			return I2C_MASTER_RC_BUS_ERROR;
		}
		if (sr1 & I2C_SR1_AF) { // address has not been acknowledged
			return I2C_MASTER_RC_NAK;
		}
		// go into receive mode if necessary
		if (!write) {
			rc = i2c_master_start(i2c); // send start condition
			if (I2C_MASTER_RC_NONE != rc) {
				return rc;
			}
			// send first part of address with receive flag
			I2C_SR1(i2c) &= ~(I2C_SR1_AF); // clear acknowledgement failure
			I2C_DR(i2c) = 11110001 | (((slave >> 8) & 0x3) << 1); // send header (11110xx1, where xx are 2 MSb of slave address)
			while (!((sr1 = I2C_SR1(i2c)) & (I2C_SR1_ADDR | I2C_SR1_AF | I2C_SR1_BERR | I2C_SR1_ARLO))); // wait until remaining part of address is transmitted
			if (sr1 & (I2C_SR1_BERR | I2C_SR1_ARLO)) {
				return I2C_MASTER_RC_BUS_ERROR;
			}
			if (sr1 & I2C_SR1_AF) { // address has not been acknowledged
				return I2C_MASTER_RC_NAK;
			}
		}
	}
	// do not check I2C_SR2_TRA to verify if we really are in transmit or receive mode since reading SR2 also clears ADDR and starting the read/write transaction
	return I2C_MASTER_RC_NONE;
}

enum i2c_master_rc i2c_master_read(uint32_t i2c, uint8_t* data, size_t data_size)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	// sanity check
	if (NULL == data || 0 == data_size) { // no data to read
		return I2C_MASTER_RC_NONE;
	}

	// I²C start condition check
	uint16_t sr1 = I2C_SR1(i2c); // read once
	if (!(sr1 & I2C_SR1_ADDR)) { // no slave have been selected
		return I2C_MASTER_RC_NOT_READY;
	}
	if (sr1 & I2C_SR1_AF) { // check if the previous transaction went well
		return I2C_MASTER_RC_NOT_READY;
	}

	// prepare (N)ACK (EV6_3 in RM0008)
	if (1 == data_size) {
		i2c_disable_ack(i2c); // NACK after first byte
	} else {
		i2c_enable_ack(i2c); // NAK after next byte
	}
	uint16_t sr2 = I2C_SR2(i2c); // reading SR2 will also also clear ADDR in SR1 and start the transaction
	if (!(sr2 & I2C_SR2_MSL)) { // I²C device is not master
		return I2C_MASTER_RC_NOT_MASTER;
	}
	if ((sr2 & I2C_SR2_TRA)) { // I²C device not in receiver mode
		return I2C_MASTER_RC_NOT_RECEIVE;
	}

	// read data
	for (size_t i = 0; i < data_size; i++) { // read bytes
		// set (N)ACK (EV6_3, EV6_1)
		if (1 == data_size - i) { // prepare to sent NACK for last byte
			i2c_send_stop(i2c); // already indicate we will send a stop (required to not send an ACK, and this must happen before the byte is transferred, see errata)
			i2c_nack_current(i2c); // (N)ACK current byte
			i2c_disable_ack(i2c); // NACK received to stop slave transmission
		} else if (2 == data_size - i) { // prepare to sent NACK for second last byte
			i2c_nack_next(i2c); // NACK next byte
			i2c_disable_ack(i2c); // NACK received to stop slave transmission
		} else {
			i2c_enable_ack(i2c); // ACK received byte to continue slave transmission
		}
		while (!((sr1 = I2C_SR1(i2c)) & (I2C_SR1_RxNE | I2C_SR1_BERR | I2C_SR1_ARLO))); // wait until byte has been received
		if (sr1 & (I2C_SR1_BERR|I2C_SR1_ARLO)) {
			return I2C_MASTER_RC_BUS_ERROR;
		}
		data[i] = i2c_get_data(i2c); // read received byte
	}

	return i2c_master_wait_stop(i2c);
}

enum i2c_master_rc i2c_master_write(uint32_t i2c, const uint8_t* data, size_t data_size)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	// sanity check
	if (NULL == data || 0 == data_size) { // no data to write
		return I2C_MASTER_RC_NONE;
	}

	// I²C start condition check
	uint16_t sr1 = I2C_SR1(i2c); // read once
	if (!(sr1 & I2C_SR1_ADDR)) { // no slave have been selected
		return I2C_MASTER_RC_NOT_READY;
	}
	if (sr1 & I2C_SR1_AF) { // check if the previous transaction went well
		return I2C_MASTER_RC_NOT_READY;
	}

	// master check
	uint16_t sr2 = I2C_SR2(i2c); // reading SR2 will also also clear ADDR in SR1 and start the transaction
	if (!(sr2 & I2C_SR2_MSL)) { // I²C device is not master
		return I2C_MASTER_RC_NOT_MASTER;
	}
	if (!(sr2 & I2C_SR2_TRA)) { // I²C device not in transmitter mode
		return I2C_MASTER_RC_NOT_TRANSMIT;
	}

	// write data
	for (size_t i = 0; i < data_size; i++) { // write bytes
		I2C_SR1(i2c) &= ~(I2C_SR1_AF); // clear acknowledgement failure
		i2c_send_data(i2c, data[i]); // send byte to be written in memory
		while (!(I2C_SR1(i2c) & (I2C_SR1_TxE | I2C_SR1_AF)) && !(I2C_SR1(i2c) & (I2C_SR1_BERR | I2C_SR1_ARLO))); // wait until byte has been transmitted
		if (I2C_SR1(i2c) & (I2C_SR1_BERR | I2C_SR1_ARLO)) {
			return I2C_MASTER_RC_BUS_ERROR;
		}
		if (I2C_SR1(i2c) & I2C_SR1_AF) { // data has not been acknowledged
			return I2C_MASTER_RC_NAK;
		}
	}

	return I2C_MASTER_RC_NONE;
}

enum i2c_master_rc i2c_master_slave_read(uint32_t i2c, uint16_t slave, bool address_10bit, uint8_t* data, size_t data_size)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	enum i2c_master_rc rc = I2C_MASTER_RC_NONE; // to store I²C return codes
	rc = i2c_master_start(i2c); // send (re-)start condition
	if (I2C_MASTER_RC_NONE != rc) {
		return rc;
	}
	rc = i2c_master_select_slave(i2c, slave, address_10bit, false); // select slave to read
	if (I2C_MASTER_RC_NONE != rc) {
		goto error;
	}
	if (NULL != data && data_size > 0) { // only read data if needed
		rc = i2c_master_read(i2c, data, data_size); // read data (includes stop)
		if (I2C_MASTER_RC_NONE != rc) {
			goto error;
		}
	} else {
		i2c_master_stop(i2c); // sent stop condition
	}

	rc = I2C_MASTER_RC_NONE; // all went well
error:
	if (I2C_MASTER_RC_NONE != rc) {
		i2c_master_stop(i2c); // sent stop condition
	}
	return rc;
}

enum i2c_master_rc i2c_master_slave_write(uint32_t i2c, uint16_t slave, bool address_10bit, const uint8_t* data, size_t data_size)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	enum i2c_master_rc rc = I2C_MASTER_RC_NONE; // to store I²C return codes
	rc = i2c_master_start(i2c); // send (re-)start condition
	if (I2C_MASTER_RC_NONE != rc) {
		return rc;
	}
	rc = i2c_master_select_slave(i2c, slave, address_10bit, true); // select slave to write
	if (I2C_MASTER_RC_NONE != rc) {
		goto error;
	}
	if (NULL != data && data_size > 0) { // write data only is some is available
		rc = i2c_master_write(i2c, data, data_size); // write data
		if (I2C_MASTER_RC_NONE != rc) {
			goto error;
		}
	}

	rc = I2C_MASTER_RC_NONE; // all went well
error:
	i2c_master_stop(i2c); // sent stop condition
	return rc;
}

enum i2c_master_rc i2c_master_address_read(uint32_t i2c, uint16_t slave, bool address_10bit, const uint8_t* address, size_t address_size, uint8_t* data, size_t data_size)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);

	enum i2c_master_rc rc = I2C_MASTER_RC_NONE; // to store I²C return codes
	rc = i2c_master_start(i2c); // send (re-)start condition
	if (I2C_MASTER_RC_NONE != rc) {
		return rc;
	}
	rc = i2c_master_select_slave(i2c, slave, address_10bit, true); // select slave to write
	if (I2C_MASTER_RC_NONE != rc) {
		goto error;
	}

	// write address
	if (NULL != address && address_size > 0) {
		rc = i2c_master_write(i2c, address, address_size); // send memory address
		if (I2C_MASTER_RC_NONE != rc) {
			goto error;
		}
	}
	// read data
	if (NULL != data && data_size > 0) {
		rc = i2c_master_start(i2c); // send re-start condition
		if (I2C_MASTER_RC_NONE != rc) {
			return rc;
		}
		rc = i2c_master_select_slave(i2c, slave, address_10bit, false); // select slave to read
		if (I2C_MASTER_RC_NONE != rc) {
			goto error;
		}
		rc = i2c_master_read(i2c, data, data_size); // read memory (includes stop)
		if (I2C_MASTER_RC_NONE != rc) {
			goto error;
		}
	} else {
		i2c_master_stop(i2c); // sent stop condition
	}

	rc = I2C_MASTER_RC_NONE;
error:
	if (I2C_MASTER_RC_NONE != rc) { // only send stop on error
		i2c_master_stop(i2c); // sent stop condition
	}
	return rc;
}

enum i2c_master_rc i2c_master_address_write(uint32_t i2c, uint16_t slave, bool address_10bit, const uint8_t* address, size_t address_size, const uint8_t* data, size_t data_size)
{
	cm3_assert(I2C1 == i2c || I2C2 == i2c);
	if (SIZE_MAX - address_size < data_size) { // prevent integer overflow
		return I2C_MASTER_RC_OTHER;
	}
	if (address_size + data_size > 10 * 1024) { // we won't enough RAM
		return I2C_MASTER_RC_OTHER;
	}
	if (address_size > 0 && NULL == address) {
		return I2C_MASTER_RC_OTHER;
	}
	if (data_size > 0 && NULL == data) {
		return I2C_MASTER_RC_OTHER;
	}

	uint8_t buffer[address_size + data_size];
	enum i2c_master_rc rc = I2C_MASTER_RC_NONE; // to store I²C return codes
	rc = i2c_master_start(i2c); // send (re-)start condition
	if (I2C_MASTER_RC_NONE != rc) {
		return rc;
	}
	rc = i2c_master_select_slave(i2c, slave, address_10bit, true); // select slave to write
	if (I2C_MASTER_RC_NONE != rc) {
		goto error;
	}

	// we can't send the address then the data size short address will cause a stop (because of how crappy the STM32F10x I²C peripheral is)
	if (address) {
		for (size_t i = 0; i < address_size; i++) {
			buffer[i] = address[i];
		}
	}
	if (data) {
		for (size_t i = 0; i < data_size; i++) {
			buffer[address_size + i] = data[i];
		}
	}
	rc = i2c_master_write(i2c, buffer, address_size + data_size); // send memory address
	if (I2C_MASTER_RC_NONE != rc) {
		goto error;
	}

error:
	rc = i2c_master_stop(i2c); // sent stop condition
	return rc;
}
