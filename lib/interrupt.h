/** BusVoodoo runtime interrupt table
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2018
 */
#pragma once
#include "libopencm3/cm3/nvic.h"
#include "libopencm3/cm3/vector.h"

/** table of interrupts which can set to user functions
 *  @note only interrupt using the default handler can be intercepted
 */
extern vector_table_entry_t interrupt_table[NVIC_IRQ_COUNT];
