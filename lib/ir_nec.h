/** library to decode InfraRed NEC code
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2018-2020
 *  @note peripherals used: timer channel @ref ir_nec_timer
 */
#pragma once

/** set when an IR NEC code has been received */
extern volatile bool ir_nec_code_received_flag;

/** IR NEC code */
struct ir_nec_code_t {
	bool repeat; /**< if this is only a code repeat (received 42.42 ms after the code, 98.75 ms after a repeat) */
	uint16_t address; /**< code address (8-bit for non-extended, 16-bit for extended) */
	uint8_t command; /**< code command */
};

/** last IR NEC code received */
extern struct ir_nec_code_t ir_nec_code_received;

/** setup peripherals to receive IR NEC codes
 *  @param[in] extended if the command address is extended (using 16 bits instead of 8, at the cost of error checking)
 */
void ir_nec_setup(bool extended);
