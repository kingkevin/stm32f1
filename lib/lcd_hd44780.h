/** library to communication with Hitacho HD44780 LCD controller
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2019-2020
 *  @note peripherals used: GPIO @ref lcd_hd44780_gpio, I²C @ref lcd_hd44780_i2c
 */
#pragma once
#include <libopencm3/stm32/i2c.h> // I²C definitions

/** @defgroup lcd_hd44780_i2c I²C peripheral used to control backpack adapter for the HD44780
 *  @{
 */
/** set I²C peripheral if I²C backpack adapter(s) is(/are) used */
//#define LCD_HD44780_I2C I2C1
/** @} */

#ifdef LCD_HD44780_I2C
/** I²C address of I²C backpack adapter (7 bits are LSb) */
extern uint8_t lcd_hd44780_i2c_addr;
#endif

/** setup peripherals to start communicating with HD4478
 *  @param[in] n_2lines 2 (true) or 1 (false) lines
 *  @param[in] f_5x10 5x10 (true) or 5x8 (false) dots font
 *  @return if the display setup is successful, else the display is probably not on the I²C bus (or the I²C bus does not work) 
 */
bool lcd_hd44780_setup(bool n_2lines, bool f_5x10);
/** release peripherals */
void lcd_hd44780_release(void);
/** read data (from CG or DDRAM)
 *  @return read data
 */
uint8_t lcd_hd44780_read_data(void);
/** write data (to CG or DDRAM)
 *  @param[in] data data to write
 *  @warning does not wait for operation to complete: use lcd_hd44780_wait_busy(37)
 */
void lcd_hd44780_write_data(uint8_t data);
/** write command
 *  @param[in] data instruction to write
 *  @warning does not wait for operation to complete: use lcd_hd44780_wait_busy(37)
 */
void lcd_hd44780_write_command(uint8_t data);
/** clears entire display and sets DDRAM address 0 in address counter
 *  @warning does not wait for operation to complete: use lcd_hd44780_wait_busy(1520)
 */
void lcd_hd44780_clear_display(void);
/** sets DDRAM address 0 in address counter, also returns display from being shifted to original position
 *  @warning does not wait for operation to complete: use lcd_hd44780_wait_busy(1520);
 */
void lcd_hd44780_return_home(void);
/** sets cursor move direction and specifies display shift
 *  @param[in] increment true = increment, false = decrement
 *  @param[in] shift true = accompanies display shift
 *  @warning does not wait for operation to complete: use lcd_hd44780_wait_busy(37);
 */
void lcd_hd44780_entry_mode_set(bool increment, bool shift);
/** set display control
 *  @param[in] d entire display on (true) / off (false)
 *  @param[in] c cursor on (true) / off (false)
 *  @param[in] b blinking of cursor position character on (true) / off (false)
 *  @warning does not wait for operation to complete: use lcd_hd44780_wait_busy(37);
 */
void lcd_hd44780_display_control(bool d, bool c, bool b);
/** sets CGRAM address. CGRAM data is sent and received after this setting.
 *  @param[in] acg CGRAM address
 *  @warning does not wait for operation to complete: use lcd_hd44780_wait_busy(37);
 */
void lcd_hd44780_set_cgram_address(uint8_t acg);
/** sets DDRAM address. DDRAM data is sent and received after this setting.
 *  @param[in] add DDRAM address
 *  @warning does not wait for operation to complete: use lcd_hd44780_wait_busy(37);
 */
void lcd_hd44780_set_ddram_address(uint8_t add);
/** utility to write entire line
 *  @param[in] line2 write line 1 (false) or 2 (true)
 *  @param[in] data characters to write on the line
 *  @param[in] length number of characters to write
 */
void lcd_hd44780_write_line(bool line2, const char* data, uint8_t length);
