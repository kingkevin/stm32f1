/** library to communicate with a Maxim MAX7219 IC attached to a 8-digit 7-segment
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2017
 *  @note peripherals used: GPIO @ref led_max7219_gpio, SPI @ref led_max7219_spi
 *  @warning all calls are blocking
 */
#pragma once

/** setup communication with MAX7219 IC
 *  @param[in] displays number of displays in the chain
 */
void led_max7219_setup(uint8_t displays);
/** do nothing (no operation)
 *  @param[in] display display number in chain (0xff for all)
 *  @note send it to the last display in the chain to clear the previous command from the chain
 */
void led_max7219_nop(uint8_t display);
/** switch display on
 *  @param[in] display display number in chain (0xff for all)
 */
void led_max7219_on(uint8_t display);
/** switch display off
 *  @param[in] display display number in chain (0xff for all)
 */
void led_max7219_off(uint8_t display);
/** switch display in test or normal operation mode
 *  @param[in] test switch in test mode (else normal operation)
 *  @param[in] display display number in chain (0xff for all)
 */
void led_max7219_test(bool test, uint8_t display);
/** set display intensity
 *  @param[in] intensity level to set (0-15)
 *  @param[in] digits number of digits to display (1-8)
 *  @param[in] display display number in chain (0xff for all)
 */
void led_max7219_intensity(uint8_t intensity, uint8_t digits, uint8_t display);
/** display text
 *  @param[in] text text to display (8 characters)
 *  @param[in] display display number in chain (0xff for all)
 *  @note use first bit of each character to enable dot
 *  @return false if string has unsupported characters
 */
bool led_max7219_text(char* text, uint8_t display);
/** display number
 *  @param[in] number number to display (8 digits max)
 *  @param[in] dots set bit if dot on corresponding digit should be displayed
 *  @param[in] display display number in chain (0xff for all)
 */
void led_max7219_number(uint32_t number, uint8_t dots, uint8_t display);
