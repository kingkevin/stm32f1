/** library to drive a SK6812RGBW LED chain
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2020
 *  @note peripherals used: SPI @ref led_sk6812rgbw_spi, DMA
 */
#pragma once

/** number of LEDs on the SK6812RGBW strip */
#define LED_SK6812RGBW_LEDS (16 * 2)

/** setup SK6812RGBW LED driver
 *  @note this starts the continuous transmission
 */
void led_sk6812rgbw_setup(void);
/** set color of a single LED
 *  @param[in] led the LED number to set the color
 *  @param[in] red the red color value to set on the LED
 *  @param[in] green the green color value to set on the LED
 *  @param[in] blue the blue color value to set on the LED
 *  @param[in] white the white color value to set on the LED
 */
void led_sk6812rgbw_set_rgb(uint16_t led, uint8_t red, uint8_t green, uint8_t blue, uint8_t white);
