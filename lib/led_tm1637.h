/** library to communicate with a Titan Micro TM1637 IC attached to a 4-digit 7-segment
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2017-2020
 *  @note peripherals used: GPIO @ref led_tm1637_gpio, timer @ref led_tm1637_timer
 *  @warning all calls are blocking
 */
#pragma once

/** display brightness levels
 */
enum led_tm1637_brightness_t {
	LED_TM1637_1DIV16 = 0,
	LED_TM1637_2DIV16 = 1,
	LED_TM1637_4DIV16 = 2,
	LED_TM1637_10DIV16 = 3,
	LED_TM1637_11DIV16 = 4,
	LED_TM1637_12DIV16 = 5,
	LED_TM1637_13DIV16 = 6,
	LED_TM1637_14DIV16 = 7,
};

/** setup communication with TM1637 IC
 *  @param[in] updown if the display is upside down
 */
void led_tm1637_setup(bool updown);
/** switch display on
 *  @return if transmission succeeded
 */
bool led_tm1637_on(void);
/** switch display off
 *  @return if transmission succeeded
 */
bool led_tm1637_off(void);
/** set display brightness
 *  @param[in] brightness brightness level to set
 *  @return if transmission succeeded
 */
bool led_tm1637_brightness(enum led_tm1637_brightness_t brightness);
/** display number
 *  @param[in] number number to display (0-9999)
 *  @param[in] zero pad number with zero on the left
 *  @return if transmission succeeded
 */
bool led_tm1637_number(uint16_t number, bool zero);
/** display time
 *  @param[in] hours hours to display (0-99)
 *  @param[in] minutes minutes to display (0-99)
 *  @note display separator between hours and minutes
 *  @return if transmission succeeded
 */
bool led_tm1637_time(uint8_t hours, uint8_t minutes);
/** display text
 *  @param[in] text text to display (4 characters)
 *  @note use first bit of each character to enable dot
 *  @return if transmission succeeded
 */
bool led_tm1637_text(char* text);
