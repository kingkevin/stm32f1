/** library to drive a WS2812B LED chain
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2020
 *  @note peripherals used: SPI @ref led_ws2812b_spi, DMA
 */
#pragma once

/** number of LEDs on the WS2812B strip */
#define LED_WS2812B_LEDS 48

/** setup WS2812B LED driver
 *  @note this starts the continuous transmission
 */
void led_ws2812b_setup(void);
/** set color of a single LED
 *  @param[in] led the LED number to set the color
 *  @param[in] red the red color value to set on the LED
 *  @param[in] green the green color value to set on the LED
 *  @param[in] blue the blue color value to set on the LED
 */
void led_ws2812b_set_rgb(uint16_t led, uint8_t red, uint8_t green, uint8_t blue);
