/** definitions to build menus (code)
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2018
 */
/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdbool.h> // boolean types
#include <stdlib.h> // standard utilities
#include <string.h> // string utilities

/* own libraries */
#include "menu.h" // definitions for the menu
#include "print.h" // print utilities

bool menu_handle_command(const char* line, const struct menu_command_t* command_list, size_t command_list_length)
{
	// ensure line is not empty
	if (!line || 0 == strlen(line)) {
		return false;
	}
	// ensure command are available
	if (NULL == command_list || 0 == command_list_length) {
		return false;
	}

	// get command
	char* dup = calloc(strlen(line)+1, sizeof(char)); // buffer to copy the line
	strncpy(dup, line, strlen(line)+1); // make a copy of the line since strtok can modify it
	const char* delimiter = " "; // words are separated by spaces
	char* word = strtok(dup, delimiter); // get first word
	if (!word) {
		return false;
	}

	// find corresponding command
	bool command_found = false; // remember if we found the corresponding command
	for (size_t i = 0; i < command_list_length; i++) { // go through available command list
		struct menu_command_t command = command_list[i]; // get current command
		if ((1 == strlen(word) && command.shortcut == word[0]) || 0 == strcmp(word, command.name)) { // check if shortcut or name match
			command_found = true; // remember we found the command
			if (command.command_handler) { // ensure there is an command handler
				if (MENU_ARGUMENT_NONE == command.argument) { // check if argument can be passed
					(*command.command_handler)(NULL); // call without argument
				} else { // argument can be passed
					const char* original_argument = line + strlen(word)+1; // remember the start of the argument in the original line
					word = strtok(NULL, delimiter); // get next word
					if (!word) { // no argument provided
						(*command.command_handler)(NULL); // call without argument
					} else if (MENU_ARGUMENT_SIGNED == command.argument) { // next argument should be a signed integer
						int32_t argument = strtol(word, NULL, 10); // get signed integer
						(*command.command_handler)(&argument); // call with argument
					} else if (MENU_ARGUMENT_UNSIGNED == command.argument) { // next argument should be an unsigned integer
						uint32_t argument = strtoul(word, NULL, 10); // get unsigned integer
						(*command.command_handler)(&argument); // call with argument
					} else if (MENU_ARGUMENT_HEX == command.argument) { // next argument should be a hexadecimal
						uint32_t argument = strtoul(word, NULL, 16); // get hexadecimal
						(*command.command_handler)(&argument); // call with argument
					} else if (MENU_ARGUMENT_BINARY == command.argument) { // next argument should be a binry
						uint32_t argument = strtoul(word, NULL, 2); // get binary
						(*command.command_handler)(&argument); // call with argument
					} else if (MENU_ARGUMENT_FLOAT == command.argument) { // next argument should be a floating point number
						double argument = atof(word); // get floating point number
						(*command.command_handler)(&argument); // call with argument
					} else if (MENU_ARGUMENT_STRING == command.argument) { // next argument should be a string
						if (delimiter[0] == original_argument[strlen(word)]) { // if there is more than one word
							word[strlen(word)] = delimiter[0]; // remove the end of string
						}
						(*command.command_handler)(word); // call with argument (remaining of the string)
					}
				}
			}
			break; // stop searching for the command
		}
	}

	// find default command
	if (!command_found) { // we didn't find the corresponding command
		for (size_t i = 0; i < command_list_length; i++) { // go through available command list
			struct menu_command_t command = command_list[i]; // get current command
			if (0 == command.shortcut && NULL == command.name) { // check if there is a default command
				command_found = true; // remember we found the command
				if (command.command_handler) { // ensure there is an command handler
					(*command.command_handler)(word); // call with current word
				}
				break; // stop searching for the command
			}
		}
	}

	free(dup); // free line copy
	return command_found;
}

void menu_print_commands(const struct menu_command_t* command_list, size_t command_list_length)
{
	for (size_t i = 0; i < command_list_length; i++) {
		struct menu_command_t command = command_list[i];
		if (command.name) {
			if (command.shortcut) {
				printf("%c|", command.shortcut);
			}
			printf("%s", command.name);
			if (MENU_ARGUMENT_NONE != command.argument && command.argument_description) {
				printf(" %s", command.argument_description);
			} else {
				printf("\t");
			}
			if (command.command_description) {
				printf("\t%s", command.command_description);
			}
			printf("\n");
		}
	}
}
