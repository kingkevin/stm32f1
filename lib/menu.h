/** definitions to build menus
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2018
 */
#pragma once

/** types of argument accepted by the command */
enum menu_argument_t {
	MENU_ARGUMENT_NONE, /**< no argument accepted */
	MENU_ARGUMENT_SIGNED, /**< int32 decimal argument accepted */
	MENU_ARGUMENT_UNSIGNED, /**< uint32 decimal argument accepted */
	MENU_ARGUMENT_HEX, /**< uint32 hexadecimal argument accepted */
	MENU_ARGUMENT_BINARY, /**< uint32 binary argument accepted */
	MENU_ARGUMENT_FLOAT, /**< double argument accepted */
	MENU_ARGUMENT_STRING, /**< string argument accepted */
};

/** command menu entry */
struct menu_command_t {
	char shortcut; /**< short command code (0 if not available) */
	char* name; /**< complete name of the command (space-free) */
	char* command_description; /**< human readable description of the command purpose */
	enum menu_argument_t argument; /**< what kind of argument it accepts */
	char* argument_description; /**< human readable description of the argument it can accept */
	void (*command_handler)(void* argument); /**< function to be called to handle this command */
};

/** parse command from line and call corresponding command
 *  @param[in] line use input to parse
 *  @param[in] command_list list of available commands
 *  @param[in] command_list_length number of available commands
 *  @return if an command corresponding to the line has been found and called
 */
bool menu_handle_command(const char* line, const struct menu_command_t* command_list, size_t command_list_length);
/** print the commands from the command list
 *  @param[in] command_list list of available commands
 *  @param[in] command_list_length number of available commands
 */
void menu_print_commands(const struct menu_command_t* command_list, size_t command_list_length);

