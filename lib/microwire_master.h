/** library to communicate using microwore as master
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2017-2020
 *  @note peripherals used: GPIO @ref microwire_master_gpio, timer @ref microwire_master_timer
 *  microwire is a 3-Wire half-duplex synchronous bus. It is very similar to SPI without fixed length messages (bit-wised).
 *  @note the user has to handle the slave select pin (high during operations) so to be able to handle multiple slaves.
 *  @warning this library implements the M93Cx8 EEPROM operation codes. Other microwire-based ICs might use different ones.
 */
#pragma once

/** setup microwire peripheral
 *  @param[in] frequency clock frequency in Hz
 *  @param[in] organization_x16 if x16 memory organization (16-bits) is used, or x8 (8-bits)
 *  @param[in] address_size address size in bits
 *  @note frequency practically limited to 500 kHz due to the software implementation nature
 */
void microwire_master_setup(uint32_t frequency, bool organization_x16, uint8_t address_size);
/** read data from slave memory
 *  @param[in] address memory address of data to read
 *  @param[out] data array to store read data
 *  @param[in] length number of data bytes/words to read
 */
void microwire_master_read(uint32_t address, uint16_t* data, size_t length);
/** enable write and erase operations
 *  @note on slave boot write is disable to prevent corruption
 */
void microwire_master_write_enable(void);
/** disable write and erase operations
 *  @note this should be done after every complete write operation to protect against corruption
 */
void microwire_master_write_disable(void);
/** write data to slave memory
 *  @param[in] address memory address of data to read
 *  @param[in] data byte/word to write
 *  @note after each write and before the next operation user should wait for the slave to be ready
 */
void microwire_master_write(uint32_t address, uint16_t data);
/** wait until slave is ready after a write or erase */
void microwire_master_wait_ready(void);
/** erase memory
 *  @param[in] address memory address of data to read
 *  @note after each erase and before the next operation user should wait for the slave to be ready
 */
void microwire_master_erase(uint32_t address);
/** erase all memory
 *  @note after each erase and before the next operation user should wait for the slave to be ready
 */
void microwire_master_erase_all(void);
/** write data to all slave memory
 *  @param[in] data byte/word to write
 *  @note after each write and before the next operation user should wait for the slave to be ready
 */
void microwire_master_write_all(uint16_t data);
