/** SSD1306 OLED library (code)
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2018-2019
 *  @note peripherals used: I2C @ref oled_ssd1306_i2c
 */
/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdbool.h> // boolean type
#include <stdlib.h> // general utilities

/* STM32 (including CM3) libraries */
#include <libopencm3/stm32/i2c.h> // I2C library

/* own libraries */
#include "global.h" // global utilities
#include "oled_ssd1306.h" // OLED definitions
#include "i2c_master.h" // I²C header and definitions

/** SSD1306 OLED display I²C slave address */
static uint8_t oled_ssd1306_slave_addr = 0x3c;

/** @defgroup oled_ssd1306_i2c I²C peripheral to communicate with the SSD1306 OLED
 *  @{
 */
#define OLED_SSD1306_I2C I2C1 /**< I²C peripheral */
/** @} */

bool oled_ssd1306_setup(uint8_t slave_addr)
{
	if (!i2c_master_check_signals(OLED_SSD1306_I2C)) { // check if there are pull-ups to operator I²C
		return false;
	}
	oled_ssd1306_slave_addr = slave_addr; // save I²C slave address of SSD1306
	i2c_master_setup(OLED_SSD1306_I2C, 400); // setup I²C bus (SSD1306 supports an I²C clock up to 400 kHz)
	const uint8_t oled_init[] = {
		0x00, // control byte: continuous (multiple byes), command
		0xae, // Set Display ON/OFF: OFF
		// hardware configuration
		0xa8, 0x3f, // Set Multiplex Ratio: 64
		0xd3, 0x00, // Set Display Offset: 0
		0xa1, // Set Segment Re-map: column address 0 is mapped to SEG127
		0xc8, // Set COM Output Scan Direction: normal mode (RESET) Scan from COM[N-1] to COM[0]
		0xda, 0x12, // Set COM Pins Hardware Configuration: Alternative COM pin configuration, Disable COM Left/Right remap
		0x40, // Set Display Start Line: start line register from 0
		// fundamental commands
		0x81, 0xff, // Set Contrast Control: 256
		0xa6, // Set Normal/Inverse Display: Normal display (RESET)
		// Timing & Driving Scheme Setting
		0xd5, 0xf0, // Set Display Clock Divide Ratio/Oscillator Frequency: Divide ratio=129, F_OSC=1
		0xd9, 0x22, // Set Pre-charge Period: Phase 1=2 DCLK, Phase 2=2DCLK
		0xdb, 0x20, // Set V_COMH Deselect Level: ~0.77xV_CC
		// Charge Pump
		0x8d, 0x14, // Charge Pump Setting: Enable Charge Pump
		// Addressing Setting
		0x20, 0x00 // Set Memory Addressing Mode: Horizontal Addressing Mode
	}; // command to initialize the display
	return I2C_MASTER_RC_NONE == i2c_master_slave_write(OLED_SSD1306_I2C, oled_ssd1306_slave_addr, false, oled_init, LENGTH(oled_init)); // send command to initialize display
}

void oled_ssd1306_on(void)
{
	const uint8_t oled_display_on[] = {
		0x80, // control byte: no continuation, command
		0xaf, // Set Display ON/OFF: ON
	}; // command to switch on display
	i2c_master_slave_write(OLED_SSD1306_I2C, oled_ssd1306_slave_addr, false, oled_display_on, LENGTH(oled_display_on)); // sent command to switch on display
}

void oled_ssd1306_off(void)
{
	const uint8_t oled_display_off[] = {
		0x80, // control byte: no continuation, command
		0xae, // Set Display ON/OFF: OFF
	}; // command to switch off display
	i2c_master_slave_write(OLED_SSD1306_I2C, oled_ssd1306_slave_addr, false, oled_display_off, LENGTH(oled_display_off)); // sent command to switch font display
}

void oled_ssd1306_test(void)
{
	const uint8_t oled_entire_display_on[] = {
		0x80, // control byte: no continuation, command
		0xa5 // Entire Display ON: Entire display ON Output ignores RAM content
	}; // command to set entire display on
	i2c_master_slave_write(OLED_SSD1306_I2C, oled_ssd1306_slave_addr, false, oled_entire_display_on, LENGTH(oled_entire_display_on)); // send command to switch entire display on
	oled_ssd1306_on(); // set display on
	sleep_ms(200); // let is on for a bit (enough for the user to see it is completely on
	oled_ssd1306_off(); // set display off
	const uint8_t oled_entire_display_ram[] = {
		0x80, // control byte: no continuation, command
		0xa4 // Entire Display ON: Resume to RAM content display
	}; // command to display RAM
	i2c_master_slave_write(OLED_SSD1306_I2C, oled_ssd1306_slave_addr, false, oled_entire_display_ram, LENGTH(oled_entire_display_ram)); // send command to display RAM
}

void oled_ssd1306_display(const uint8_t* display_data, uint16_t display_length)
{
	// verify input
	if (0 == display_length || NULL == display_data) {
		return;
	}

	const uint8_t oled_start_page[] = {
		0x00, // control byte: continuous (multiple byes), command
		0xb0, // Set Page Start Address for Page Addressing Mode: PAGE0
		0x00, // Set Lower Column Start Address for Page Addressing Mode: 0
		0x10 // Set Higher Column Start Address for Page Addressing Mode: 0
	}; // command to set addressing mode
	i2c_master_slave_write(OLED_SSD1306_I2C, oled_ssd1306_slave_addr, false, oled_start_page, LENGTH(oled_start_page)); // send command to set addressing mode
	if (I2C_MASTER_RC_NONE != i2c_master_start(OLED_SSD1306_I2C)) { // send start condition
		return;
	}
	if (I2C_MASTER_RC_NONE != i2c_master_select_slave(OLED_SSD1306_I2C, oled_ssd1306_slave_addr, false, true)) { // select OLED display
		return;
	}
	uint8_t oled_data[display_length + 1]; // we have to copy the data because we need to send a single block since i2c_master_write sends a stop
	oled_data[0] = 0x40; // control byte: continuous (multiple byes), data
	for (uint16_t i = 0; i < display_length; i++) {
		oled_data[i + 1] = display_data[i];
	}
	if (I2C_MASTER_RC_NONE != i2c_master_write(OLED_SSD1306_I2C, oled_data, display_length + 1)) { // send data header
		return;
	}
	i2c_master_stop(OLED_SSD1306_I2C); // send stop condition
}

