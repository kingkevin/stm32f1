/** SSD1306 OLED library (API)
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2018-2019
 *  @note peripherals used: I2C @ref oled_ssd1306_i2c
 */

/** setup OLED display
 *  @param[in] slave_addr I²C slave address of SSD1306 device (least significant 7-bit)
 *  @return if the display setup is successful, else the display is probably not on the I²C bus
 *  @warning only one display on the I²C bus is currently supported
 */
bool oled_ssd1306_setup(uint8_t slave_addr);
/** switch OLED display on */
void oled_ssd1306_on(void);
/** switch OLED display off */
void oled_ssd1306_off(void);
/** test OLED display: switch entire screen on for a brief time */
void oled_ssd1306_test(void);
/** send data to display to OLED display
 *  @param[in] display_data data to display (first byte is left column, MSb is top pixel, warps pages)
 *  @param[in] display_length length of data to display
 */
void oled_ssd1306_display(const uint8_t* display_data, uint16_t display_length);
