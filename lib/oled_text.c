/** library to show display text on SSD1306 OLED display
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2020
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @note peripherals used: I²C @ref oled_ssd1306_i2c
 */
/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdbool.h> // boolean type
#include <string.h> // string utilities

/* own libraries */
#include "global.h" // global utilities
#include "oled_text.h" // own definitions
#include "oled_ssd1306.h" // OLED display utilities
#include "font.h" // font glyphs

/** if the OLED display is present and setup */
static bool oled_text_present = false;

/** display pixel buffer */
static uint8_t oled_text_display[128 * 8] = {0};

/** SSD1306 OLED display I2C slave address */
#define OLED_SSD1306_SLAVE 0x3c

/** look-up table to swap the bit order in a byte
 *  @remark this is useful for the OLED screen since the top pixel is the MSb
 */
static const uint8_t bit_order_switch_lut[256] = { 0x00, 0x80, 0x40, 0xc0, 0x20, 0xa0, 0x60, 0xe0, 0x10, 0x90, 0x50, 0xd0, 0x30, 0xb0, 0x70, 0xf0, 0x08, 0x88, 0x48, 0xc8, 0x28, 0xa8, 0x68, 0xe8, 0x18, 0x98, 0x58, 0xd8, 0x38, 0xb8, 0x78, 0xf8, 0x04, 0x84, 0x44, 0xc4, 0x24, 0xa4, 0x64, 0xe4, 0x14, 0x94, 0x54, 0xd4, 0x34, 0xb4, 0x74, 0xf4, 0x0c, 0x8c, 0x4c, 0xcc, 0x2c, 0xac, 0x6c, 0xec, 0x1c, 0x9c, 0x5c, 0xdc, 0x3c, 0xbc, 0x7c, 0xfc, 0x02, 0x82, 0x42, 0xc2, 0x22, 0xa2, 0x62, 0xe2, 0x12, 0x92, 0x52, 0xd2, 0x32, 0xb2, 0x72, 0xf2, 0x0a, 0x8a, 0x4a, 0xca, 0x2a, 0xaa, 0x6a, 0xea, 0x1a, 0x9a, 0x5a, 0xda, 0x3a, 0xba, 0x7a, 0xfa, 0x06, 0x86, 0x46, 0xc6, 0x26, 0xa6, 0x66, 0xe6, 0x16, 0x96, 0x56, 0xd6, 0x36, 0xb6, 0x76, 0xf6, 0x0e, 0x8e, 0x4e, 0xce, 0x2e, 0xae, 0x6e, 0xee, 0x1e, 0x9e, 0x5e, 0xde, 0x3e, 0xbe, 0x7e, 0xfe, 0x01, 0x81, 0x41, 0xc1, 0x21, 0xa1, 0x61, 0xe1, 0x11, 0x91, 0x51, 0xd1, 0x31, 0xb1, 0x71, 0xf1, 0x09, 0x89, 0x49, 0xc9, 0x29, 0xa9, 0x69, 0xe9, 0x19, 0x99, 0x59, 0xd9, 0x39, 0xb9, 0x79, 0xf9, 0x05, 0x85, 0x45, 0xc5, 0x25, 0xa5, 0x65, 0xe5, 0x15, 0x95, 0x55, 0xd5, 0x35, 0xb5, 0x75, 0xf5, 0x0d, 0x8d, 0x4d, 0xcd, 0x2d, 0xad, 0x6d, 0xed, 0x1d, 0x9d, 0x5d, 0xdd, 0x3d, 0xbd, 0x7d, 0xfd, 0x03, 0x83, 0x43, 0xc3, 0x23, 0xa3, 0x63, 0xe3, 0x13, 0x93, 0x53, 0xd3, 0x33, 0xb3, 0x73, 0xf3, 0x0b, 0x8b, 0x4b, 0xcb, 0x2b, 0xab, 0x6b, 0xeb, 0x1b, 0x9b, 0x5b, 0xdb, 0x3b, 0xbb, 0x7b, 0xfb, 0x07, 0x87, 0x47, 0xc7, 0x27, 0xa7, 0x67, 0xe7, 0x17, 0x97, 0x57, 0xd7, 0x37, 0xb7, 0x77, 0xf7, 0x0f, 0x8f, 0x4f, 0xcf, 0x2f, 0xaf, 0x6f, 0xef, 0x1f, 0x9f, 0x5f, 0xdf, 0x3f, 0xbf, 0x7f, 0xff, };

bool oled_text_setup(void)
{

	// setup SSD1306 OLED display
	oled_text_clear(); // clean display buffer
	oled_text_present = oled_ssd1306_setup(OLED_SSD1306_SLAVE); // setup OLED display
	if (oled_text_present) {
#if DEBUG
		oled_ssd1306_test(); // test OLED display
#endif
		oled_text_update(); // send display buffer
		oled_ssd1306_on(); // switch display back on
	};

	return oled_text_present;
}

void oled_text_clear(void)
{
	// write all buffer to 0
	for (uint16_t i = 0; i < LENGTH(oled_text_display); i++) {
		oled_text_display[i] = 0;
	}
}

void oled_text_pos(uint8_t column, uint8_t row, enum font_name font_name, const char *text)
{
	// sanity checks
	if (column >= 128) {
		return;
	}
	if (row >= 64) {
		return;
	}
	if (font_name >= FONT_MAX) {
		return;
	}
	if (NULL == text) {
		return;
	}

	const struct font_s *font = &fonts[font_name]; // get selected font
	while (*text && column < 128) {
		char c = *text;
		if (c >= ' ' && c < ' ' + FONT_GLYPH_NUMBERS) {
			for (uint8_t i = 0; i < font->width; i++) { // draw glyph from left to right
				uint8_t col = column + i; // calculate destination column position
				if (col >= 128) {
					break; // end of screen reached
				}
				uint16_t glyph_column = font->glyphs[font->width * (c - ' ') + i]; // get glyph column to draw
				// draw bottom part of glyph
				uint16_t pixel_byte_row = 128 * ((row / 8) - 0) + col;
				uint8_t glyph_byte_row = (glyph_column << (7 - (row % 8))) >> 0;
				glyph_byte_row = bit_order_switch_lut[glyph_byte_row];
				oled_text_display[pixel_byte_row] |= glyph_byte_row;
				// draw middle part of glyph
				if (row >= 8 && font->height > 8 - (row % 8)) {
					pixel_byte_row -= 128;
					glyph_byte_row = (glyph_column << (7 - (row % 8))) >> 8;
					glyph_byte_row = bit_order_switch_lut[glyph_byte_row];
					oled_text_display[pixel_byte_row] |= glyph_byte_row;
				}
				// draw top part of glyph
				if (row >= 16 && font->height > 8 + (row % 8)) {
					pixel_byte_row -= 128;
					glyph_byte_row = ((uint32_t)glyph_column << (7 - (row % 8))) >> 16;
					glyph_byte_row = bit_order_switch_lut[glyph_byte_row];
					oled_text_display[pixel_byte_row] |= glyph_byte_row;
				}
			}
		}
		text++; // go to next character
		column += font->width+1;
	}
}

void oled_text_line(const char* text, uint8_t line_nb)
{
	// verify input
	if (NULL == text) {
		return;
	}
	if (line_nb > 3) { // we only use 4 lines
		return;
	}

	// clear line
	for (uint16_t i = 128 * (line_nb * 2); i < 128 * (line_nb * 2 + 2); i++) {
		 oled_text_display[i] = 0;
	}

	oled_text_pos(0, 15 + 16 * line_nb, FONT_KING14, text); // draw text on the left of top line
}

void oled_text_update(void)
{
	if (oled_text_present) { // only do something if the display is present
		oled_ssd1306_display(oled_text_display, LENGTH(oled_text_display)); // send current display buffer
	}
}
