/** library to show display text on SSD1306 OLED display
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @date 2018-2020
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @note peripherals used: I²C @ref oled_ssd1306_i2c
 */
#include "font.h"

/** setup OLED display
 *  @return if OLED screen is present and responsive
 */
bool oled_text_setup(void);
/** clear display buffer
 *  @note update the display to clear it
 */
void oled_text_clear(void);
/** draw text in display buffer
 *  @param[in] column display column where to start drawing the text (0 is left)
 *  @param[in] row display row where to put the lower end of the characters (0 is top)
 *  @param[in] font_name name of the font to use to draw the text
 *  @param[in] text text string to draw
 */
void oled_text_pos(uint8_t column, uint8_t row, enum font_name font_name, const char *text);
/** draw text on display
 *  @param[in] text text to display on top left side of screen
 *  @param[in] line_nb on which line to display the text (up to 3)
 *  @note update the display to display the text
 */
void oled_text_line(const char* text, uint8_t line_nb);
/** update OLED display RAM with current display buffer */
void oled_text_update(void);
