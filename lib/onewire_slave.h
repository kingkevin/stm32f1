/** library for 1-wire protocol as slave
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2017-2020
 *  @note peripherals used: timer @ref onewire_slave_timer, GPIO @ref onewire_slave_gpio
 *  @note overdrive mode is not supported
 */
#pragma once

/** set when a function command code has been received
 *  @note needs to be cleared by user
 */
extern volatile bool onewire_slave_function_code_received;
/** last function command code received */
extern volatile uint8_t onewire_slave_function_code;
/** set when data read/write transfer has been completed
 *  @note needs to be cleared by user
 */
extern volatile bool onewire_slave_transfer_complete;

/** setup 1-wire peripheral
 *  @param[in] family family code for slave ROM code (8 bits)
 *  @param[in] serial serial number for slave ROM code (48 bits)
 */
void onewire_slave_setup(uint8_t family, uint64_t serial);
/** read data from master
 *  @param[out] data buffer to save read bits
 *  @param[in] size number of bytes to read
 *  @return if transfer initialization succeeded
 *  @note onewire_slave_transfer_complete is set when transfer is completed
 */
bool onewire_slave_function_read(uint8_t* data, size_t size);
/** write data to master
 *  @param[in] data data to write
 *  @param[in] size number of bytes to write
 *  @return if transfer initialization succeeded
 *  @note onewire_slave_transfer_complete is set when transfer is completed
 */
bool onewire_slave_function_write(const uint8_t* data, size_t size);
