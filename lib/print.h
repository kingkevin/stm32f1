/** printing utilities to replace the large printf from the standard library (API)
 *  @note use % as format specifier prefix, followed by + to enforce sign or 0x prefix, 0 followed by n for padding or forcing integer part of floating point number, . followed by n for number for fractional precision of floating point numbers, and format specifier
 *  format specifier supported are:
 *  - c for character
 *  - s for string
 *  - u for up to uint32_t unsigned integer
 *  - U for uint64_t unsigned integer
 *  - d for up to int32_t signed integer
 *  - D for int64_t signed integer
 *  - f for float and double floating point numbers
 *  - x for up to uint32_t lower case hexadecimal
 *  - X for uint64_t lower case hexadecimal
 *  - h for up to uint32_t upper case hexadecimal
 *  - H for uint64_t upper case hexadecimal
 *  - b for up to uint32_t bits
 *  - B for uint64_t bits
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2017-2019
 */
#pragma once

extern uint8_t print_error; /**< flags to indicate which error(s) occurred within printf or snprintf */

#define PRINT_ERROR_NONE 0 /**< no error occurred */
#define PRINT_ERROR_MALFORMED 0x1 /**< input format string is malformed */
#define PRINT_ERROR_UNSUPPORTED 0x2 /**< input format string is not supported */
#define PRINT_ERROR_MAX 0x04 /**< maximum returned printed length reached but more has been printed */
#define PRINT_ERROR_TRUNCATED 0x08 /**< output string size is not large enough to include complete printed string */

/** print a single character on user output
 *  @warning this must be implemented by the user (using the desired output interface)
 *  @param[in] c character to be printed
 *  @return number of characters printed
 **/
size_t putc(char c);
/** print string
 *  @param[in] str string to print
 *  @return number of characters printed
 *  @note uses putc to output characters
 *  @note does not require print functions (and is faster/simpler)
 */
size_t puts(const char* str);
/** print format string on user output
 *  @param[in] format format string to be printed
 *  @param[in] ... arguments referenced by format string to be printed
 *  @return number of characters printed
 **/
size_t printf(const char* format, ...);
/** print format string on string or user output
 *  @param[out] str string to print format string on, or user output if str is set to NULL (str will always be terminated with a null character '\0')
 *  @param[in] size size of string (writes at most size characters on str, including the termination null character '\0')
 *  @param[in] format format string to be printed
 *  @param[in] ... arguments referenced by format string to be printed
 *  @return number of characters printed (a return value of size or more means that the output was truncated)
 **/
size_t snprintf(char* str, size_t size, const char* format, ...);
/** print data in xxd style
 *  @param[in] offset base address of the data to print
 *  @param[in] data data to print
 *  @param[in] length size of data to print
 *  @return number of characters printed (a return value of size or more means that the output was truncated)
 */
size_t print_xxd(uint32_t offset, const uint8_t* data, size_t length);
