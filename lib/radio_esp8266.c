/** library to send data using ESP8266 WiFi SoC
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2021
 *  @note peripherals used: USART @ref radio_esp8266_usart
 */

// standard libraries
#include <stdint.h> // standard integer types
#include <stdlib.h> // general utilities
#include <string.h> // string and memory utilities
#include <stdio.h> // string utilities

// STM32 (including CM3) libraries
#include <libopencm3/stm32/rcc.h> // real-time control clock library
#include <libopencm3/stm32/gpio.h> // general purpose input output library
#include <libopencm3/stm32/usart.h> // universal synchronous asynchronous receiver transmitter library
#include <libopencm3/cm3/nvic.h> // interrupt handler
#include <libopencmsis/core_cm3.h> // Cortex M3 utilities

#include "radio_esp8266.h" // radio header and definitions
#include "global.h" // common methods

/** @defgroup radio_esp8266_usart USART peripheral used for communication with radio
 *  @{
 */
#define RADIO_ESP8266_USART 2 /**< USART peripheral */
#define RADIO_ESP8266_TX PA2 /**< pin used for USART TX */
#define RADIO_ESP8266_RX PA3 /**< pin used for USART RX */
#define RADIO_ESP8266_AF GPIO_AF7 /**< alternate function for UART pins */
/** @} */

// input and output buffers and used memory
static uint8_t rx_buffer[24] = {0}; /**< buffer for received data (we only expect AT responses) */
static volatile uint16_t rx_used = 0; /**< number of byte in receive buffer */
static uint8_t tx_buffer[256] = {0}; /**< buffer for data to transmit */
static volatile uint16_t tx_used = 0; /**< number of bytes used in transmit buffer */

// response status
volatile bool radio_esp8266_response = false; /**< when a response has been received (OK or ERROR) */
volatile bool radio_esp8266_success = false; /**< if the response is OK (else ERROR), set when radio_esp8266_response is set to true */

/** transmit data to radio
 *  @param[in] data data to transmit
 *  @param[in] length length of data to transmit
 */
static void radio_esp8266_transmit(const uint8_t* data, uint8_t length) {
	while (tx_used || !usart_get_flag(USART(RADIO_ESP8266_USART), USART_SR_TXE)) { // wait until ongoing transmission completed
		usart_enable_tx_interrupt(USART(RADIO_ESP8266_USART)); // enable transmit interrupt
		__WFI(); // sleep until something happened
	}
	usart_disable_tx_interrupt(USART(RADIO_ESP8266_USART)); // ensure transmit interrupt is disable to prevent index corruption (the ISR should already have done it)
	radio_esp8266_response = false; // reset status because of new activity
	for (tx_used = 0; tx_used < length && tx_used < LENGTH(tx_buffer); tx_used++) { // copy data
		tx_buffer[tx_used] = data[length - 1 - tx_used]; // put character in buffer (in reverse order)
	}
	if (tx_used) {
		usart_enable_tx_interrupt(USART(RADIO_ESP8266_USART)); // enable interrupt to send bytes
	}
}

bool radio_esp8266_setup(void)
{
	// configure pins
	rcc_periph_clock_enable(GPIO_RCC(RADIO_ESP8266_TX)); // enable clock for USART TX pin port peripheral
	gpio_mode_setup(GPIO_PORT(RADIO_ESP8266_TX), GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN(RADIO_ESP8266_TX)); // set TX pin to alternate function
	gpio_set_output_options(GPIO_PORT(RADIO_ESP8266_TX), GPIO_OTYPE_PP, GPIO_OSPEED_25MHZ, GPIO_PIN(RADIO_ESP8266_TX)); // set TX pin output as push-pull
	gpio_set_af(GPIO_PORT(RADIO_ESP8266_TX), RADIO_ESP8266_AF, GPIO_PIN(RADIO_ESP8266_TX)); // set alternate function to USART
	rcc_periph_clock_enable(GPIO_RCC(RADIO_ESP8266_RX)); // enable clock for USART RX pin port peripheral
	gpio_mode_setup(GPIO_PORT(RADIO_ESP8266_RX), GPIO_MODE_AF, GPIO_PUPD_PULLUP, GPIO_PIN(RADIO_ESP8266_RX)); // set GPIO to alternate function, with pull up to avoid noise in case it is not connected
	gpio_set_af(GPIO_PORT(RADIO_ESP8266_RX), RADIO_ESP8266_AF, GPIO_PIN(RADIO_ESP8266_RX)); // set alternate function to USART

	// configure USART
	rcc_periph_clock_enable(RCC_USART(RADIO_ESP8266_USART)); // enable clock for USART peripheral
	rcc_periph_reset_pulse(RST_USART(RADIO_ESP8266_USART)); // reset peripheral
	usart_set_baudrate(USART(RADIO_ESP8266_USART), 115200);
	usart_set_databits(USART(RADIO_ESP8266_USART), 8);
	usart_set_stopbits(USART(RADIO_ESP8266_USART), USART_STOPBITS_1);
	usart_set_mode(USART(RADIO_ESP8266_USART), USART_MODE_TX_RX);
	usart_set_parity(USART(RADIO_ESP8266_USART), USART_PARITY_NONE);
	usart_set_flow_control(USART(RADIO_ESP8266_USART), USART_FLOWCONTROL_NONE);

	nvic_enable_irq(USART_IRQ(RADIO_ESP8266_USART)); // enable the UART interrupt
	usart_enable_rx_interrupt(USART(RADIO_ESP8266_USART)); // enable receive interrupt
	usart_enable(USART(RADIO_ESP8266_USART)); // enable UART

	// reset buffer states
	rx_used = 0;
	tx_used = 0;

	// verify if ESP8266 is reachable
	uint16_t timeout = 0; // reset timeout counter
	radio_esp8266_transmit((uint8_t*)"AT\r\n", 4); // verify if module is present
	while (!radio_esp8266_response) { // wait for response
		if (timeout > 100) { // response takes too long
			return false;
		}
		sleep_ms(10); // wait a tiny bit
		timeout += 10; // remember we waited
	}
	if (!radio_esp8266_success) {
		return false;
	}

	// reset module so it connects to AP
	timeout = 0; // reset timeout counter
	radio_esp8266_transmit((uint8_t*)"AT+RST\r\n", 8); // reset module
	while (!radio_esp8266_response) { // wait for response
		if (timeout > 100) { // response takes too long
			return false;
		}
		sleep_ms(10); // wait a tiny bit
		timeout += 10; // remember we waited
	}
	if (!radio_esp8266_success) {
		return false;
	}
	timeout = 0; // reset timeout counter
	while(rx_used < 13 || 0 != memcmp((char*)&rx_buffer[rx_used - 13], "WIFI GOT IP\r\n", 13)) { // wait to have IP
		if (timeout > 10000) { // connection takes too long
			return false;
		}
		sleep_ms(10); // wait a tiny bit
		timeout += 10; // remember we waited
	}

	// disable echo for better parsing
	timeout = 0; // reset timeout counter
	while (!radio_esp8266_response) { // wait for response
		if (timeout > 100) { // response takes too long
			return false;
		}
		sleep_ms(10); // wait a tiny bit
		timeout += 10; // remember we waited
	}
	if (!radio_esp8266_success) {
		return false;
	}

	return true;
}

bool radio_esp8266_open(const char* host, uint16_t port, bool tcp)
{
	char command[256] = {0}; // string to create command
	int length = snprintf(command, LENGTH(command), "AT+CIPSTART=\"%s\",\"%s\",%u\r\n", tcp ? "TCP" : "UDP", host, port); // create AT command to establish a TCP connection
	if (length > 0) {
		uint16_t timeout = 0; // reset timeout counter
		radio_esp8266_transmit((uint8_t*)command, length);
		while (!radio_esp8266_response) { // wait for response
			if (timeout > 1000) { // response takes too long
				return false;
			}
			sleep_ms(10); // wait a tiny bit
			timeout += 10; // remember we waited
		}
		if (!radio_esp8266_success) {
			return false;
		}
	}
	return true;
}

bool radio_esp8266_send(const uint8_t* data, uint8_t length)
{
	char command[16 + 1] = {0}; // string to create command
	int command_length = snprintf(command, LENGTH(command), "AT+CIPSEND=%u\r\n", length); // create AT command to send data
	if (command_length > 0) {
		// start sending
		uint16_t timeout = 0; // reset timeout counter
		radio_esp8266_transmit((uint8_t*)command, command_length); // transmit AT command
		while (!radio_esp8266_response) { // wait for response
			if (timeout > 1000) { // response takes too long
				return false;
			}
			sleep_ms(10); // wait a tiny bit
			timeout += 10; // remember we waited
		}
		if (!radio_esp8266_success) {
			return false;
		}
		// send actual data
		timeout = 0; // reset timeout counter
		radio_esp8266_transmit(data, length); // transmit data
		while (!radio_esp8266_response) { // wait for response
			if (timeout > 1000) { // response takes too long
				return false;
			}
			sleep_ms(10); // wait a tiny bit
			timeout += 10; // remember we waited
		}
		if (!radio_esp8266_success) {
			return false;
		}
	}
	return true;
}

bool radio_esp8266_close(void)
{
	uint16_t timeout = 0; // reset timeout counter
	radio_esp8266_transmit((uint8_t*)"AT+CIPCLOSE\r\n", 13); // send AT command to close established connection
	while (!radio_esp8266_response) { // wait for response
		if (timeout > 1000) { // response takes too long
			return false;
		}
		sleep_ms(10); // wait a tiny bit
		timeout += 10; // remember we waited
	}
	if (!radio_esp8266_success) {
		return false;
	}
	return true;
}

/** USART interrupt service routine called when data has been transmitted or received */
void USART_ISR(RADIO_ESP8266_USART)(void)
{
	if (usart_get_flag(USART(RADIO_ESP8266_USART), USART_SR_TXE)) { // data has been transmitted
		if (tx_used) { // there is still data in the buffer to transmit
			usart_send(USART(RADIO_ESP8266_USART), tx_buffer[tx_used - 1]); // put data in transmit register
			tx_used--; // update used size
		} else { // no data in the buffer to transmit
			usart_disable_tx_interrupt(USART(RADIO_ESP8266_USART)); // disable transmit interrupt
		}
	}
	if (usart_get_flag(USART(RADIO_ESP8266_USART), USART_SR_RXNE)) { // data has been received
		while (rx_used >= LENGTH(rx_buffer)) { // if buffer is full
			memmove(rx_buffer, &rx_buffer[1], LENGTH(rx_buffer) - 1); // drop old data to make space (ring buffer are more efficient but harder to handle)
			rx_used--; // update used buffer information
		}
		rx_buffer[rx_used++] = usart_recv(USART(RADIO_ESP8266_USART)); // put character in buffer
		// if the used send a packet with these strings during the commands detection the AT command response will break (AT commands are hard to handle perfectly)
		if (rx_used >= 4 && 0 == memcmp((char*)&rx_buffer[rx_used - 4], "OK\r\n", 4)) { // OK received
			radio_esp8266_response = true; // response received
			radio_esp8266_success = true; // command succeeded
			rx_used = 0; // reset buffer
		} else if (rx_used >= 7 && 0 == memcmp((char*)&rx_buffer[rx_used - 7], "ERROR\r\n", 7)) { // ERROR received
			radio_esp8266_response = true; // response received
			radio_esp8266_success = false; // command failed
			rx_used = 0; // reset buffer
		}
	}
}
