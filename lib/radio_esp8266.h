/** library to send data using ESP8266 WiFi SoC (API)
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2021
 *  @note peripherals used: USART @ref radio_esp8266_usart
 */
#pragma once

/** a response has been returned by the radio */
extern volatile bool radio_esp8266_activity;
/** the last command has succeeded */
extern volatile bool radio_esp8266_success;

/** setup peripherals to communicate with radio
 *  @return if it connected to AP
 */
bool radio_esp8266_setup(void);
/** establish TCP connection
 *  @param[in] host host to connect to
 *  @param[in] port port number to connect to
 *  @param[in] tcp if connect to a TCP port (else UDP)
 *  @return if operation succeeded
 */
bool radio_esp8266_open(const char* host, uint16_t port, bool tcp);
/** send data (requires established connection)
 *  @param[in] data data to send
 *  @param[in] length size of data to send
 *  @return if operation succeeded
 */
bool radio_esp8266_send(const uint8_t* data, uint8_t length);
/** close established connection
 *  @return if operation succeeded
 */
bool radio_esp8266_close(void);
