/** library to get time from a DCF77 module
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2020
 *  @note peripherals used: GPIO @ref rtc_dcf77_gpio, timer @ref rtc_dcf77_timer
 */
#pragma once

/** set when time information has been received */
extern volatile bool rtc_dcf77_time_flag;

/** DCF77 time structure */
struct rtc_dcf77_time_t {
	bool valid; /**< if the time is valid */
	uint8_t milliseconds; /**< milliseconds (00-99) */
	uint8_t seconds; /**< seconds (always 0) */
	uint8_t minutes; /**< minutes (00-49) */
	uint8_t hours; /**< hours (00-23) */
	uint8_t day; /**< day of the month (01-31) */
	uint8_t weekday; /**< day of the week (1-7=Monday-Sunday) */
	uint8_t month; /**< month (01-12) */
	uint8_t year; /**< year within century (00-99) */
};

/** decoded DCF77 time received */
extern struct rtc_dcf77_time_t rtc_dcf77_time;

/** setup DCF77 time receiver module */
void rtc_dcf77_setup(void);
/** switch on DCF77 time receiver module
 *  @note it switches back off after too many invalid decoding attempts
 */
void rtc_dcf77_on(void);
/** switch off DCF77 time receiver module */
void rtc_dcf77_off(void);
