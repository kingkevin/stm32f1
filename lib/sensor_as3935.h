/** library to communication with ams AS3935 Franklin lightning sensor IC using SPI
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2019
 *  @note peripherals used: SPI @ref sensor_as3935_spi, GPIO @ref sensor_as3935_gpio
 */
#pragma once

/** a interrupt has been received */
extern volatile bool sensor_as3935_interrupt;

/** SPI operation mode */
enum sensor_as3935_operation_t {
	SENSOR_AS3935_OPERATION_WRITE = 0, /**< write to register */
	SENSOR_AS3935_OPERATION_DIRECT_COMMAND = 0, /**< send direct command (equivalent to write to register) */
	SENSOR_AS3935_OPERATION_READ = 1, /**< read from register */
};

/** register names */
enum sensor_as3935_register_t {
	SENSOR_AS3935_REGISTER_AFE_GB, /**< AFE Gain Boost */
	SENSOR_AS3935_REGISTER_PWD, /**< Power-down */
	SENSOR_AS3935_REGISTER_NF_LEV, /**< Noise Floor Level */
	SENSOR_AS3935_REGISTER_WDTH, /**< Watchdog threshold */
	SENSOR_AS3935_REGISTER_CL_STAT, /**< Clear statistics */
	SENSOR_AS3935_REGISTER_MIN_NUM_LIGH, /**< Minimum number of lightning */
	SENSOR_AS3935_REGISTER_SREJ, /**< Spike rejection */
	SENSOR_AS3935_REGISTER_LCO_FDIV, /**< Frequency division ration for antenna tuning */
	SENSOR_AS3935_REGISTER_MASK_DIST, /**< Mask Disturber */
	SENSOR_AS3935_REGISTER_INT, /**< Interrupt */
	SENSOR_AS3935_REGISTER_S_LIG_L, /**< Energy of the Single Lightning LSBYTE */
	SENSOR_AS3935_REGISTER_S_LIG_M, /**< Energy of the Single Lightning MSBYTE */
	SENSOR_AS3935_REGISTER_S_LIG_MM, /**< Energy of the Single Lightning MMSBYTE */
	SENSOR_AS3935_REGISTER_DISTANCE, /**< Distance estimation */
	SENSOR_AS3935_REGISTER_DISP_LCO, /**< Display LCO on IRQ pin */
	SENSOR_AS3935_REGISTER_DISP_SRCO, /**< Display SRCO on IRQ pin */
	SENSOR_AS3935_REGISTER_DISP_TRCO, /**< Display TRCO on IRQ pin */
	SENSOR_AS3935_REGISTER_TUN_CAP, /**< Internal Tuning Capacitors */
	SENSOR_AS3935_REGISTER_LDLUT1, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT2, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT3, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT4, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT5, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT6, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT7, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT8, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT9, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT10, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT11, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT12, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT13, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT14, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT15, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT16, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT17, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT18, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT19, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT20, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT21, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT22, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT23, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT24, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT25, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT26, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT27, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT28, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT29, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT30, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT31, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT32, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT33, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT34, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT35, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT36, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT37, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT38, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT39, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT40, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT41, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_LDLUT42, /**< Lightning Detection Look-up table */
	SENSOR_AS3935_REGISTER_TRCO_CALIB_DONE, /**< Calibration of TRCO done (1=successful) */
	SENSOR_AS3935_REGISTER_TRCO_CALIB_NOK, /**< Calibration of TRCO unsuccessful (1=not successful) */
	SENSOR_AS3935_REGISTER_SRCO_CALIB_DONE, /**< Calibration of SRCO done (1=successful) */
	SENSOR_AS3935_REGISTER_SRCO_CALIB_NOK, /**< Calibration of SRCO unsuccessful (1=not successful) */
};

/** direct command to sets all registers in default mode */
#define SENSOR_AS3935_DIRECT_COMMAND_PRESET_DEFAULT 0x3C
/** direct command to calibrate automatically the internal RC oscillators */
#define SENSOR_AS3935_DIRECT_COMMAND_CALIB_RCO 0x3D
/** value to be used for direct commands */
#define SENSOR_AS3935_DIRECT_COMMAND_VALUE 0x96

/** interrupt meaning
 *  @note use as bit mask
 *  @note if IRQ is high but no bit is set, a new front calculation is available
 */
enum sensor_as3935_interrupt_t {
	SENSOR_AS3935_INT_NH = 0x01, /**< noise level too high */
	SENSOR_AS3935_INT_D = 0x04, /**< disturber detected */
	SENSOR_AS3935_INT_L = 0x08, /**< lightning detected */
};

/** register value for indoor AFE setting */
#define SENSOR_AS3935_REGISTER_AFE_INDOOR 0x12
/** register value for outdoor AFE setting */
#define SENSOR_AS3935_REGISTER_AFE_OUTDOOR 0x0e

/** setup peripherals to communicate with sensor
 *  @return false if communication with sensor failed
 *  @note the sensor configuration will be set to default and powered down
 */
bool sensor_as3935_setup(void);
/** release peripherals used to communicate with sensor */
void sensor_as3935_release(void);
/** send command
 *  @param[in] operation operation mode
 *  @param[in] address_command register address or direct command
 *  @param[in] data register data
 *  @return register value
 */
uint8_t sensor_as3935_command(enum sensor_as3935_operation_t operation, uint8_t address_command, uint8_t data);
/** read register
 *  @param[in] name register name
 *  @return register value
 */
uint8_t sensor_as3935_read_register(enum sensor_as3935_register_t name);
/** read register
 *  @param[in] name register name
 *  @param[in] value register value
 *  @return if write is allowed and succeeded
 */
bool sensor_as3935_write_register(enum sensor_as3935_register_t name, uint8_t value);
/** power down sensor
 *  @note after a power down the sensor needs to be calibrated again
 */
void sensor_as3935_power_down(void);
/** power up sensor and calibrate RCO
 *  @return 0 if calibration succeeded, < 0 else
 *  @warning the antenna should be tuned before
 */
int8_t sensor_as3935_power_up(void);
/** tune antenna
 *  @return if calibration succeeded (false if no capacitor value leading to a frequency error < 3.5% has been found)
 *  @note powers the sensor (but does not calibrates it)
 *  @warning should be run after setup and before power up
 */
bool sensor_as3935_tune(void);
