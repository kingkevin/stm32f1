/** library to query measurements from Aosong and DHT22/AM2302 temperature and relative humidity sensor
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2017-2020
 *  @note peripherals used: timer channel @ref sensor_dht1122_timer
 */

/* standard libraries */
#include <stdint.h> // standard integer types

/* STM32 (including CM3) libraries */
#include <libopencmsis/core_cm3.h> // Cortex M3 utilities
#include <libopencm3/cm3/nvic.h> // interrupt handler
#include <libopencm3/stm32/rcc.h> // real-time control clock library
#include <libopencm3/stm32/gpio.h> // general purpose input output library
#include <libopencm3/stm32/timer.h> // timer utilities

/* own libraries */
#include "sensor_dht1122.h" // PZEM electricity meter header and definitions
#include "global.h" // common methods

/** @defgroup sensor_dht1122_timer timer peripheral used to measure signal timing for bit decoding
 *  @{
 */
#define SENSOR_DHT1122_PIN PB4 /**< MCU pin connected to DHT1122 I/O pin, pulled up externally */
#define SENSOR_DHT1122_TIMER 3 /**< timer peripheral for DHT1122 pin */
#define SENSOR_DHT1122_CHANNEL 1 /**< channel used as input capture */
#define SENSOR_DHT1122_AF GPIO_AF2 /**< pin alternate function to use as timer */
#define SENSOR_DHT1122_JITTER 0.2 /**< signal timing jitter tolerated in timing */
/** @} */

volatile bool sensor_dht1122_measurement_received = false;

/** remember if the sensor is a DHT11 or DHT22 */
static bool sensor_dht1122_dht22 = false;

/** communication states */
volatile enum sensor_dht1122_state_t {
	SENSOR_DHT1122_OFF, // no request has started
	SENSOR_DHT1122_HOST_START, // host starts request (and waits >18ms)
	SENSOR_DHT1122_HOST_STARTED, // host started request and waits for slave answer
	SENSOR_DHT1122_SLAVE_START, // slave responds to request and puts signal low for 80 us and high for 80 us
	SENSOR_DHT1122_SLAVE_BIT, // slave is sending bit by putting signal low for 50 us and high (26-28 us = 0, 70 us = 1)
	SENSOR_DHT1122_MAX
} sensor_dht1122_state = SENSOR_DHT1122_OFF; /**< current communication state */

/** the bit number being sent (MSb first), up to 40 */
volatile uint8_t sensor_dht1122_bit = 0;

/** the 40 bits (5 bytes) being sent by the device */
volatile uint8_t sensor_dht1122_bits[5] = {0};

/** reset all states */
static void sensor_dht1122_reset(void)
{
	// reset states
	sensor_dht1122_state = SENSOR_DHT1122_OFF;
	sensor_dht1122_bit = 0;
	sensor_dht1122_measurement_received = false;
	gpio_set(GPIO_PORT(SENSOR_DHT1122_PIN), GPIO_PIN(SENSOR_DHT1122_PIN)); // idle is high (using pull-up resistor), pull-up before setting as output else the signal will be low for short
	gpio_mode_setup(GPIO_PORT(SENSOR_DHT1122_PIN), GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_PIN(SENSOR_DHT1122_PIN)); // setup GPIO pin as output (host starts communication before slave replies)
	timer_ic_disable(TIM(SENSOR_DHT1122_TIMER), TIM_IC(SENSOR_DHT1122_CHANNEL));  // disable capture interrupt only when receiving data
	timer_disable_counter(TIM(SENSOR_DHT1122_TIMER)); // disable timer
}

void sensor_dht1122_setup(bool dht22)
{
	sensor_dht1122_dht22 = dht22; // remember sensor type

	// configure pin to use as timer input
	rcc_periph_clock_enable(GPIO_RCC(SENSOR_DHT1122_PIN)); // enable clock for I²C I/O peripheral
	gpio_set(GPIO_PORT(SENSOR_DHT1122_PIN), GPIO_PIN(SENSOR_DHT1122_PIN)); // already put signal high
	gpio_mode_setup(GPIO_PORT(SENSOR_DHT1122_PIN), GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO_PIN(SENSOR_DHT1122_PIN)); // set pin to alternate function
	gpio_set_output_options(GPIO_PORT(SENSOR_DHT1122_PIN), GPIO_OTYPE_OD, GPIO_OSPEED_25MHZ, GPIO_PIN(SENSOR_DHT1122_PIN)); // set pin output as open-drain
	gpio_set_af(GPIO_PORT(SENSOR_DHT1122_PIN), SENSOR_DHT1122_AF, GPIO_PIN(SENSOR_DHT1122_PIN)); // set alternate function to pin

	// setup timer to measure signal timing for bit decoding (use timer channel as input capture)
	rcc_periph_clock_enable(RCC_TIM(SENSOR_DHT1122_TIMER)); // enable clock for timer peripheral
	rcc_periph_reset_pulse(RST_TIM(SENSOR_DHT1122_TIMER)); // reset timer state
	timer_set_mode(TIM(SENSOR_DHT1122_TIMER), TIM_CR1_CKD_CK_INT, TIM_CR1_CMS_EDGE, TIM_CR1_DIR_UP); // set timer mode, use undivided timer clock,edge alignment (simple count), and count up
	timer_enable_break_main_output(TIM(SENSOR_DHT1122_TIMER)); // required to enable some timer, even when no dead time is used
	// one difference between DHT11 and DHT22 is the request pulse duration
	if (!sensor_dht1122_dht22) { // for DHT11
		timer_set_prescaler(TIM(SENSOR_DHT1122_TIMER), 24 - 1); // set the prescaler so this 16 bits timer allows to wait for 18 ms for the start signal ( 1/(84E6/24/(2**16)) = 18.7 ms )
	} else { // for DHT22
		timer_set_prescaler(TIM(SENSOR_DHT1122_TIMER), 2 - 1); // set the prescaler so this 16 bits timer allows to wait for 18 ms for the start signal ( 1/(84E6/2/(2**16)) = 1.56 ms )
	}
	timer_ic_set_input(TIM(SENSOR_DHT1122_TIMER), TIM_IC(SENSOR_DHT1122_CHANNEL), TIM_IC_IN_TI(SENSOR_DHT1122_CHANNEL)); // configure ICx to use TIn
	timer_ic_set_filter(TIM(SENSOR_DHT1122_TIMER), TIM_IC(SENSOR_DHT1122_CHANNEL), TIM_IC_OFF); // use no filter input (precise timing needed)
	timer_ic_set_polarity(TIM(SENSOR_DHT1122_TIMER), TIM_IC(SENSOR_DHT1122_CHANNEL), TIM_IC_FALLING); // capture on falling edge (start of bit)
	timer_ic_set_prescaler(TIM(SENSOR_DHT1122_TIMER), TIM_IC(SENSOR_DHT1122_CHANNEL), TIM_IC_PSC_OFF); // don't use any prescaler since we want to capture every pulse

	timer_clear_flag(TIM(SENSOR_DHT1122_TIMER), TIM_SR_UIF); // clear flag
	timer_update_on_overflow(TIM(SENSOR_DHT1122_TIMER)); // only use counter overflow as UEV source (use overflow as start time or timeout)
	timer_enable_irq(TIM(SENSOR_DHT1122_TIMER), TIM_DIER_UIE); // enable update interrupt for timer

	timer_clear_flag(TIM(SENSOR_DHT1122_TIMER), TIM_SR_CCIF(SENSOR_DHT1122_CHANNEL)); // clear input compare flag
	timer_enable_irq(TIM(SENSOR_DHT1122_TIMER), TIM_DIER_CCIE(SENSOR_DHT1122_CHANNEL)); // enable capture interrupt

	nvic_enable_irq(NVIC_TIM_IRQ(SENSOR_DHT1122_TIMER)); // catch interrupt in service routine

	sensor_dht1122_reset(); // reset state
}

bool sensor_dht1122_measurement_request(void)
{
	if (sensor_dht1122_state != SENSOR_DHT1122_OFF) { // not the right state to start (wait up until timeout to reset state)
		return false;
	}
	if (gpio_get(GPIO_PORT(SENSOR_DHT1122_PIN), GPIO_PIN(SENSOR_DHT1122_PIN)) == 0) { // signal should be high per default
		return false;
	}
	if (TIM_CR1(TIM(SENSOR_DHT1122_TIMER)) & (TIM_CR1_CEN)) { // timer should be off
		return false;
	}
	sensor_dht1122_reset(); // reset states

	// send start signal (pull low for > 18 ms)
	gpio_clear(GPIO_PORT(SENSOR_DHT1122_PIN), GPIO_PIN(SENSOR_DHT1122_PIN)); // set signal to low
	timer_set_counter(TIM(SENSOR_DHT1122_TIMER), 0); // reset timer counter
	timer_enable_counter(TIM(SENSOR_DHT1122_TIMER)); // enable timer to wait for 18 ms until overflow
	sensor_dht1122_state = SENSOR_DHT1122_HOST_START; // remember we started sending signal

	return true;
}

struct sensor_dht1122_measurement_t sensor_dht1122_measurement_decode(void)
{
	struct sensor_dht1122_measurement_t measurement = { 0xff, 0xff }; // measurement to return
	if (sensor_dht1122_bit < 40) { // not enough bits received
		return measurement;
	}
	if ((uint8_t)(sensor_dht1122_bits[0] + sensor_dht1122_bits[1] + sensor_dht1122_bits[2] + sensor_dht1122_bits[3]) != sensor_dht1122_bits[4]) { // error in checksum (not really parity bit, as mentioned in the datasheet)
		return measurement;
	}
	if (0 == sensor_dht1122_bits[0] && 0 == sensor_dht1122_bits[1] && 0 == sensor_dht1122_bits[2] && 0 == sensor_dht1122_bits[3] && 0 == sensor_dht1122_bits[4]) { // this is measurement is very unlikely, there must be an error
		return measurement;
	}
	// the other difference between DHT11 and DHT22 is the encoding of the values
	if (!sensor_dht1122_dht22) { // for DHT11
		// calculate measured values (byte 1 and 3 should be the factional value but they are always 0)
		measurement.humidity = sensor_dht1122_bits[0];
		measurement.temperature = sensor_dht1122_bits[2];
	} else { // for DHT22
		measurement.humidity = (int16_t)((sensor_dht1122_bits[0] << 8) + sensor_dht1122_bits[1]) / 10.0;
		measurement.temperature = (int16_t)((sensor_dht1122_bits[2] << 8) + sensor_dht1122_bits[3]) / 10.0;
	}

	return measurement;
}

/** interrupt service routine called for timer */
void TIM_ISR(SENSOR_DHT1122_TIMER)(void)
{
	if (timer_get_flag(TIM(SENSOR_DHT1122_TIMER), TIM_SR_UIF)) { // overflow update event happened
		timer_clear_flag(TIM(SENSOR_DHT1122_TIMER), TIM_SR_UIF); // clear flag
		if (sensor_dht1122_state == SENSOR_DHT1122_HOST_START) { // start signal sent
			gpio_set(GPIO_PORT(SENSOR_DHT1122_PIN), GPIO_PIN(SENSOR_DHT1122_PIN)); // let pin go so we can use it as input
			gpio_mode_setup(GPIO_PORT(SENSOR_DHT1122_PIN), GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO_PIN(SENSOR_DHT1122_PIN)); // set pin to alternate function so the timer can read the pin
			sensor_dht1122_state = SENSOR_DHT1122_HOST_STARTED; // switch to next state
			timer_ic_enable(TIM(SENSOR_DHT1122_TIMER), TIM_IC(SENSOR_DHT1122_CHANNEL));  // enable capture interrupt only when receiving data
		} else { // timeout occurred
			sensor_dht1122_reset(); // reset states
		}
	} else if (timer_get_flag(TIM(SENSOR_DHT1122_TIMER), TIM_SR_CCIF(SENSOR_DHT1122_CHANNEL))) { // edge detected on input capture
		uint16_t time = TIM_CCR(SENSOR_DHT1122_TIMER, SENSOR_DHT1122_CHANNEL); // save captured bit timing (this clear also the flag)
		timer_set_counter(TIM(SENSOR_DHT1122_TIMER), 0); // reset timer counter
		time = (time * 1E6) / (rcc_ahb_frequency / (TIM_PSC(TIM(SENSOR_DHT1122_TIMER)) + 1)); // calculate time in us
		switch (sensor_dht1122_state) {
			case SENSOR_DHT1122_HOST_STARTED: // the host query data and the slave is responding
				sensor_dht1122_state = SENSOR_DHT1122_SLAVE_START; // set new state
				break;
			case SENSOR_DHT1122_SLAVE_START: // the slave sent the start signal
				if (time >= ((80 + 80) * (1 - SENSOR_DHT1122_JITTER)) && time <= ((80 + 80) * (1 + SENSOR_DHT1122_JITTER))) { // response time should be 80 us low and 80 us high
					sensor_dht1122_state = SENSOR_DHT1122_SLAVE_BIT; // set new state
				} else {
					goto error;
				}
				break;
			case SENSOR_DHT1122_SLAVE_BIT: // the slave sent a bit
				if (sensor_dht1122_bit >= 40) { // no bits should be received after 40 bits
					goto error;
				}
				if (time >= ((50 + 26) * (1 - SENSOR_DHT1122_JITTER)) && time <= ((50 + 28) * (1 + SENSOR_DHT1122_JITTER))) { // bit 0 time should be 50 us low and 26-28 us high
					sensor_dht1122_bits[sensor_dht1122_bit / 8] &= ~(1 << (7 - (sensor_dht1122_bit % 8))); // clear bit
				} else if (time >= ((50 + 70)*(1 - SENSOR_DHT1122_JITTER)) && time <= ((50 + 70) * (1 + SENSOR_DHT1122_JITTER))) { // bit 1 time should be 50 us low and 70 us high
					sensor_dht1122_bits[sensor_dht1122_bit / 8] |= (1 << (7 - (sensor_dht1122_bit % 8))); // set bit
				} else {
					goto error;
				}
				sensor_dht1122_bit++;
				if (sensor_dht1122_bit >= 40) { // all bits received
					sensor_dht1122_reset(); // reset states
					sensor_dht1122_bit = 40; // signal decoder all bits have been received
					sensor_dht1122_measurement_received = true; // signal user all bits have been received
				}
				break;
			default: // unexpected state
error:
				sensor_dht1122_reset(); // reset states
		}
	} else { // no other interrupt should occur
		while (true); // unhandled exception: wait for the watchdog to bite
	}
}
