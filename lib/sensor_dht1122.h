/** library to query measurements from Aosong DHT11 and DHT22/AM2302 temperature and relative humidity sensor
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2017-2021
 *  @note peripherals used: timer channel @ref sensor_dht1122_timer (add external pull-up resistor)
 */
#pragma once

/** a measurement response has been received */
extern volatile bool sensor_dht1122_measurement_received;

/** measurement returned by sensor */
struct sensor_dht1122_measurement_t {
	float humidity; /**< relative humidity in %RH (20-95) */
	float temperature; /**< temperature in °C (0-50) */
};

/** setup peripherals to communicate with sensor
 *  @param[in] dht22 false for DHT11, true for DHT22/AM2302
*/
void sensor_dht1122_setup(bool dht22);
/** request measurement from sensor
 *  @return request started successfully
 */
bool sensor_dht1122_measurement_request(void);
/** decode received measurement
 *  @return decoded measurement (0xff,0xff if invalid)
 */
struct sensor_dht1122_measurement_t sensor_dht1122_measurement_decode(void);
