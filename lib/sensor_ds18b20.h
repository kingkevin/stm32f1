/** library for Maxim DS18B20 digital temperature sensor (using 1-Wire protocol) (API)
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2017-2020
 *  @note peripherals used: 1-Wire (timer @ref onewire_master_timer, GPIO @ref onewire_master_gpio)
 *  @warning this library does not support parasite power mode and alarms
 */
#pragma once

/** setup 1-Wire peripheral to communicate with sensors on bus */
void sensor_ds18b20_setup(void);
/** get number of DS18B20 sensors on bus
 *  @return number of DS18B20 sensors on bus
 */
uint64_t sensor_ds18b20_number(void);
/** verify if only DS18B20 sensors are on the bus
 *  @return if only DS18B20 sensors are on the bus
 */
bool sensor_ds18b20_only(void);
/** send all DS18B20 slaves on the bus
 *  @param[out] code ROM code for sensor (0 if error occurred)
 *  @return if an additional sensors have been detected
 */
bool sensor_ds18b20_list(uint64_t* code);
/** start converting (e.g. measuring) temperature
 *  @warning conversion time to wait before reading temperature depends on the resolution set (9 bits: 93.75ms, 10 bits: 187.5ms, 11 bits: 375ms, 12 bits: 950ms)
 *  @param[in] code ROM code of sensor to start conversion on (0 for all, if only DS18B20 sensors are on the bus)
 *  @return if conversion started
 */
bool sensor_ds18b20_convert(uint64_t code);
/** get converted temperature
 *  @note 85.0 C is the default temperature when no conversion has been performed
 *  @param[in] code ROM code of sensor
 *  @return temperature (NaN if error)
 */
float sensor_ds18b20_temperature(uint64_t code);
/** set conversion precision
 *  @param[in] code ROM code of sensor to start conversion on (0 for single DS18B20 sensor are on the bus)
 *  @param[in] precision precision in bits (9-12)
 *  @return if operation succeeded
 */
bool sensor_ds18b20_precision(uint64_t code, uint8_t precision);
