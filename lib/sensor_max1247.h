/** library to communication with Maxim MAX1247 12-bit 4-channel ADC using SPI
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2020
 *  @note peripherals used: SPI @ref sensor_max1247_spi
 */
#pragma once

/** setup peripherals to communicate with sensor
 *  @note the sensor configuration will be set to default and powered down
 */
void sensor_max1247_setup(void);
/** release peripherals used to communicate with sensor */
void sensor_max1247_release(void);
/** read conversion from channel
 *  @param[in] channel which of the 4 channels to convert
 *  @return 12-bit conversion value (0xffff if error)
 */
uint16_t sensor_max1247_read(uint8_t channel);
