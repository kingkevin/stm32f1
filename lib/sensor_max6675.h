/** library to communication with Maxim MAX6675 K-type thermocouple to digital temperature sensor using SPI
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2020
 *  @note peripherals used: SPI @ref sensor_max6675_spi
 */
#pragma once

/** setup communication to MAX6675 sensor */
void sensor_max6675_setup(void);
/** release peripherals used to communicate with MAX6675 sensor */
void sensor_max6675_release(void);
/** read temperature from MAX6675 sensor
 *  @return temperature (in °C) measured by sensor (infinity if K-thermocouple is missing, nan on error)
 *  @note resolution is in 0.25 °C
 *  @note wait 0.22 s between readings (max. time needed for a conversion)
 */
float sensor_max6675_read(void);
