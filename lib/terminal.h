/** terminal prompt interface (API)
 *  @note allows line editing and supports some ANSI escape codes
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2018
 */

/** terminal prompt prefix */
extern char* terminal_prefix;

/** initialize terminal prompt */
void terminal_setup(void);

/** send character to terminal
 *  @param[in] c character to send
 */
void terminal_send(char c);

/** called when a line is entered
 *  @param[in] line line entered by user (NULL on CTRL+D)
 */
extern void (*terminal_process)(char* line);
