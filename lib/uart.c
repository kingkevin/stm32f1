/** library for UART communication
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2020
 *  @note peripherals used: USART @ref uart
 */

/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdio.h> // standard I/O facilities
#include <stdlib.h> // general utilities

/* STM32 (including CM3) libraries */
#include <libopencm3/stm32/rcc.h> // real-time control clock library
#include <libopencm3/stm32/gpio.h> // general purpose input output library
#include <libopencm3/stm32/usart.h> // universal synchronous asynchronous receiver transmitter library
#include <libopencm3/cm3/nvic.h> // interrupt handler
#include <libopencmsis/core_cm3.h> // Cortex M3 utilities

#include "uart.h" // UART header and definitions
#include "global.h" // common methods

/** @defgroup uart USART peripheral used for UART communication
 *  @{
 */
#define UART_ID 1 /**< USART peripheral */
/** @} */

#define UART_BAUDRATE 921600 /**< serial baud rate, in bits per second (with 8N1 8 bits, no parity bit, 1 stop bit settings) */

/* output ring buffer, indexes, and available memory */
static volatile uint8_t tx_buffer[128] = {0}; /**< ring buffer for data to transmit (size must be a power of 2) */
static volatile uint16_t tx_i = 0; /**< current position of transmitted data */
static volatile uint16_t tx_used = 0; /**< how much data needs to be transmitted */

void uart_setup(void)
{
	/* enable UART I/O peripheral */
	rcc_periph_clock_enable(RCC_USART_PORT(UART_ID)); // enable clock for UART port peripheral
	rcc_periph_clock_enable(RCC_USART(UART_ID)); // enable clock for UART peripheral
	rcc_periph_reset_pulse(RST_USART(UART_ID)); // reset peripheral
	rcc_periph_clock_enable(RCC_AFIO); // enable pin alternate function (UART)
	gpio_set_mode(USART_TX_PORT(UART_ID), GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_ALTFN_PUSHPULL, USART_TX_PIN(UART_ID)); // setup GPIO pin UART transmit
	gpio_set_mode(USART_RX_PORT(UART_ID), GPIO_MODE_INPUT, GPIO_CNF_INPUT_PULL_UPDOWN, USART_RX_PIN(UART_ID)); // setup GPIO pin UART receive
	gpio_set(USART_RX_PORT(UART_ID), USART_RX_PIN(UART_ID)); // pull up to avoid noise when not connected

	/* setup UART parameters */
	usart_set_baudrate(USART(UART_ID), UART_BAUDRATE);
	usart_set_databits(USART(UART_ID), 8);
	usart_set_stopbits(USART(UART_ID), USART_STOPBITS_1);
	usart_set_mode(USART(UART_ID), USART_MODE_TX_RX);
	usart_set_parity(USART(UART_ID), USART_PARITY_NONE);
	usart_set_flow_control(USART(UART_ID), USART_FLOWCONTROL_NONE);

	nvic_enable_irq(USART_IRQ(UART_ID)); // enable the UART interrupt
	usart_enable_rx_interrupt(USART(UART_ID)); // enable receive interrupt
	usart_enable(USART(UART_ID)); // enable UART

	/* reset buffer states */
	tx_i = 0;
	tx_used = 0;
}

void uart_putchar_blocking(char c)
{
	usart_send_blocking(USART(UART_ID), c); // send character (this already wait for the buffer to be empty)
}

void uart_flush(void)
{
	while (tx_used) { // idle until buffer is empty
		__WFI(); // sleep until interrupt
	}
	usart_wait_send_ready(USART(UART_ID)); // wait until transmit register is empty (transmission might not be complete)
	while(!usart_get_flag(USART(UART_ID), USART_SR_TC)); // wait for transmission to be complete
}

void uart_putchar_nonblocking(char c)
{
	while (tx_used >= LENGTH(tx_buffer)) { // idle until buffer has some space
		usart_enable_tx_interrupt(USART(UART_ID)); // enable transmit interrupt
		// don't go to sleep since this might prevent an interrupt
	}
	usart_disable_tx_interrupt(USART(UART_ID)); // disable transmit interrupt to prevent index corruption
	tx_buffer[(tx_i + tx_used) & (LENGTH(tx_buffer) - 1)] = c; // put character in buffer
	tx_used++; // update used buffer
	usart_enable_tx_interrupt(USART(UART_ID)); // enable transmit interrupt
}

/** UART interrupt service routine called when data has been transmitted or received */
void USART_ISR(UART_ID)(void)
{
	if (usart_get_flag(USART(UART_ID), USART_SR_TXE)) { // data has been transmitted
		if (!tx_used) { // no data in the buffer to transmit
			usart_disable_tx_interrupt(USART(UART_ID)); // disable transmit interrupt
		} else {
			usart_send(USART(UART_ID), tx_buffer[tx_i]); // put data in transmit register
			tx_i = (tx_i + 1) & (LENGTH(tx_buffer) - 1); // update location on buffer
			tx_used--; // update used size
		}
	}
	while (usart_get_flag(USART(UART_ID), USART_SR_RXNE)) { // data has been received (repeat while receiving)
		char c = usart_recv(USART(UART_ID)); // save character and free UART buffer
		user_input_store(c); // store data
	}
}
