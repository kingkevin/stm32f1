/** library for UART communication
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2020
 *  @note peripherals used: USART @ref uart
 */
#pragma once

/** setup UART peripheral */
void uart_setup(void);
/** send character over UART (blocking)
 *  @param[in] c character to send
 *  @note blocks until character transmission started */
void uart_putchar_blocking(char c);
/** ensure all data has been transmitted (blocking)
 *  @note block until all data has been transmitted
 */
void uart_flush(void);
/** send character over UART (non-blocking)
 *  @param[in] c character to send
 *  @note blocks if transmit buffer is full, else puts in buffer and returns
 */
void uart_putchar_nonblocking(char c);
