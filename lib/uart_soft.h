/** library to control up to 4 independent receive and transmit software UART ports
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2020
 *  @note peripherals used: GPIO @ref uart_soft_gpio, timer @ref uart_soft_timer
 */

/** if data has been received from UART port and is available to be read */
extern volatile bool uart_soft_received[4];

/** setup software UART ports
 *  @param[in] rx_baudrates baud rates of the 4 UART RX ports (0 if unused)
 *  @param[in] tx_baudrates baud rates of the 4 UART TX ports (0 if unused)
 *  @return is setup succeeded, else the configuration is wrong
 */
bool uart_soft_setup(const uint32_t* rx_baudrates, const uint32_t* tx_baudrates);
/** get received byte from UART port
 *  @param[in] uart UART receive port to read byte from
 *  @return received byte (0 if no byte is available)
 */
uint8_t uart_soft_getbyte(uint8_t uart);
/** ensure all bytes are transmitted for the UART
 *  @param[in] uart UART port to flush
 */
void uart_soft_flush(uint8_t uart);
/** put byte in buffer to be transmitted on UART port
 *  @note blocking if buffer is full
 *  @param[in] uart UART port to transmit the byte from
 *  @param[in] byte byte to put in transmit buffer
 */
void uart_soft_putbyte_nonblocking(uint8_t uart, uint8_t byte);
/** transmit byte on UART port
 *  @note blocks until all buffered byte and this byte are transmitted
 *  @param[in] uart UART port to transmit the byte from
 *  @param[in] byte byte to transmit
 */
void uart_soft_putbyte_blocking(uint8_t uart, uint8_t byte);

