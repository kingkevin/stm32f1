/** library for USB CDC ACM communication (code)
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2019
 */

/* standard libraries */
#include <stdint.h> // standard integer types
#include <stdlib.h> // general utilities

/* STM32 (including CM3) libraries */
#include <libopencmsis/core_cm3.h> // Cortex M3 utilities
#include <libopencm3/cm3/scb.h> // reset utilities
#include <libopencm3/cm3/nvic.h> // interrupt handler
#include <libopencm3/stm32/rcc.h> // real-time control clock library
#include <libopencm3/stm32/gpio.h> // general purpose input output library
#include <libopencm3/usb/usbd.h> // USB library
#include <libopencm3/usb/cdc.h> // USB CDC library
#include <libopencm3/usb/dfu.h> // DFU definitions

/* own libraries */
#include "global.h" // global utilities
#include "usb_cdcacm.h" // USB CDC ACM header and definitions
#include "print.h" // strings utilities (large library, but the main application has space)

/** maximum packet size for USB data transfer */
#define USB_DATA_TRANSFER_SIZE 64 // 64 is the maximum for full speed devices

volatile bool usb_cdcacm_connecting = false;

static uint8_t usbd_control_buffer[128] = {0}; /**< buffer to be used for control requests */
static usbd_device *usb_device = NULL; /**< structure holding all the info related to the USB device */
static volatile bool first_connection = false; /**< used to detect when the first connection occurred */

/* output ring buffer, index, and available memory */
static uint8_t tx_buffer[256] = {0}; /**< ring buffer for data to transmit */
static volatile uint16_t tx_i = 0; /**< current position if transmitted data */
static volatile uint16_t tx_used = 0; /**< how much data needs to be transmitted */
static volatile bool tx_lock = false; /**< if the transmit buffer is currently being written */

/** USB CDC ACM device descriptor
 *  @note as defined in USB CDC specification section 5
 */
static const struct usb_device_descriptor usb_cdcacm_device_descriptor = {
	.bLength = USB_DT_DEVICE_SIZE, /**< the size of this header in bytes, 18 */
	.bDescriptorType = USB_DT_DEVICE, /**< a value of 1 indicates that this is a device descriptor */
	.bcdUSB = 0x0200, /**< this device supports USB 2.0 */
	.bDeviceClass = USB_CLASS_CDC, /**< use the CDC device class */
	.bDeviceSubClass = 0, /**< unused */
	.bDeviceProtocol = 0, /**< unused */
	.bMaxPacketSize0 = 64, /**< packet size for endpoint zero in bytes */
	.idVendor = 0x1209, /**< pid.codes vendor ID */
	.idProduct = 0x4356, /**< CuVoodoo product ID within the Vendor ID space */
	.bcdDevice = 0x0000, /**< Device Release Number: board version number */
	.iManufacturer = 1, /**< the index of the string in the string table that represents the name of the manufacturer of this device */
	.iProduct = 2, /**< the index of the string in the string table that represents the name of the product */
	.iSerialNumber = 3, /**< the index of the string in the string table that represents the serial number of this item in string form */
	.bNumConfigurations = 1, /**< the number of possible configurations this device has */
};

/** USB CDC ACM data endpoints
 *  @note as defined in USB CDC specification section 5
 */
static const struct usb_endpoint_descriptor usb_cdcacm_data_endpoints[] = {{
	.bLength = USB_DT_ENDPOINT_SIZE, /**< the size of the endpoint descriptor in bytes */
	.bDescriptorType = USB_DT_ENDPOINT, /**< a value of 5 indicates that this describes an endpoint */
	.bEndpointAddress = 0x02, /**< OUT (from host) direction (0<<7), endpoint 2 */
	.bmAttributes = USB_ENDPOINT_ATTR_BULK, /**< bulk mode */
	.wMaxPacketSize = USB_DATA_TRANSFER_SIZE, /**< maximum packet size */
	.bInterval = 1, /**< the frequency, in number of frames, that we're going to be sending data */
},{
	.bLength = USB_DT_ENDPOINT_SIZE, /**< the size of the endpoint descriptor in bytes */
	.bDescriptorType = USB_DT_ENDPOINT, /**< a value of 5 indicates that this describes an endpoint */
	.bEndpointAddress = 0x82, /**< IN (to host) direction (1<<7), endpoint 2 */
	.bmAttributes = USB_ENDPOINT_ATTR_BULK, /**< bulk mode */
	.wMaxPacketSize = USB_DATA_TRANSFER_SIZE, /**< maximum packet size */
	.bInterval = 1, /**< the frequency, in number of frames, that we're going to be sending data */
}};

/** USB CDC ACM communication endpoints
 *  @note This notification endpoint isn't implemented. According to CDC spec its optional, but its absence causes a NULL pointer dereference in Linux cdc_acm driver
 */
static const struct usb_endpoint_descriptor usb_cdcacm_communication_endpoints[] = {{
	.bLength = USB_DT_ENDPOINT_SIZE, /**< the size of the endpoint descriptor in bytes */
	.bDescriptorType = USB_DT_ENDPOINT, /**< a value of 5 indicates that this describes an endpoint */
	.bEndpointAddress = 0x81, /**< IN (to host) direction (1<<7), endpoint 1 */
	.bmAttributes = USB_ENDPOINT_ATTR_INTERRUPT, /**< interrupt mode */
	.wMaxPacketSize = 16, /**< maximum packet size */
	.bInterval = 255, /**< the frequency, in number of frames, that we're going to be sending data */
}};

/** USB CDC ACM functional descriptor
 *  @note as defined in USB CDC specification section 5.2.3
 *  @return packed structure
 */
static const struct {
	struct usb_cdc_header_descriptor header; /**< header */
	struct usb_cdc_call_management_descriptor call_mgmt; /**< call management descriptor */
	struct usb_cdc_acm_descriptor acm; /**< descriptor */
	struct usb_cdc_union_descriptor cdc_union;  /**< descriptor */
} __attribute__((packed)) usb_cdcacm_functional_descriptors = {
	.header = {
		.bFunctionLength = sizeof(struct usb_cdc_header_descriptor), /**< descriptor length */
		.bDescriptorType = CS_INTERFACE, /**< descriptor type */
		.bDescriptorSubtype = USB_CDC_TYPE_HEADER, /**< descriptor subtype */
		.bcdCDC = 0x0110, /**< CDC value */
	},
	.call_mgmt = {
		.bFunctionLength = sizeof(struct usb_cdc_call_management_descriptor), /**< descriptor length */
		.bDescriptorType = CS_INTERFACE,  /**< descriptor type */
		.bDescriptorSubtype = USB_CDC_TYPE_CALL_MANAGEMENT, /**< descriptor subtype */
		.bmCapabilities = 0, /**< capabilities */
		.bDataInterface = 1, /**< data interface (==usb_cdcacm_data_interface.bInterfaceNumber) */
	},
	.acm = {
		.bFunctionLength = sizeof(struct usb_cdc_acm_descriptor), /**< descriptor length */
		.bDescriptorType = CS_INTERFACE, /**< descriptor type */
		.bDescriptorSubtype = USB_CDC_TYPE_ACM, /**< descriptor subtype */
		.bmCapabilities = 0, /**< capabilities */
	},
	.cdc_union = {
		.bFunctionLength = sizeof(struct usb_cdc_union_descriptor), /**< descriptor length */
		.bDescriptorType = CS_INTERFACE, /**< descriptor type */
		.bDescriptorSubtype = USB_CDC_TYPE_UNION, /**< descriptor subtype */
		.bControlInterface = 0, /**< control interface (==usb_cdcacm_com_interface.bInterfaceNumber) */
		.bSubordinateInterface0 = 1, /**< subordinate interface (==usb_cdcacm_data_interface.bInterfaceNumber) */
	 },
};

/** USB CDC interface descriptor
 *  @note as defined in USB CDC specification section 5.1.3
 */
static const struct usb_interface_descriptor usb_cdcacm_communication_interface = {
	.bLength = USB_DT_INTERFACE_SIZE,  /**< size of descriptor in byte */
	.bDescriptorType = USB_DT_INTERFACE, /**< interface descriptor type */
	.bInterfaceNumber = 0, /**< interface number in the list */
	.bAlternateSetting = 0, /**< no alternative settings */
	.bNumEndpoints = 1, /**< number of endpoints used (communication OUT)*/
	.bInterfaceClass = USB_CLASS_CDC, /**< USB CDC interface class for CDC */
	.bInterfaceSubClass = USB_CDC_SUBCLASS_ACM,  /**< USB CDC ACM interface subclass */
	.bInterfaceProtocol = USB_CDC_PROTOCOL_NONE, /**< USB CDC ACM none protocol */
	.iInterface = 0, /**< the index of the string in the string table that represents interface description */

	.endpoint = usb_cdcacm_communication_endpoints, /**< pointer to endpoint descriptor */

	.extra = &usb_cdcacm_functional_descriptors, /**< pointer to functional description (for the data interface) */
	.extralen = sizeof(usb_cdcacm_functional_descriptors), /**< number of functional descriptions */
};

/** USB CDC ACM data class interface descriptor
 *  @note as defined in USB CDC specification section 5.1.3
 */
static const struct usb_interface_descriptor usb_cdcacm_data_interface = {
	.bLength = USB_DT_INTERFACE_SIZE,  /**< size of descriptor in byte */
	.bDescriptorType = USB_DT_INTERFACE, /**< interface descriptor type */
	.bInterfaceNumber = 1, /**< interface number in the list */
	.bAlternateSetting = 0, /**< no alternative settings */
	.bNumEndpoints = 2, /**< number of endpoints used (data IN and OUT) */
	.bInterfaceClass = USB_CLASS_DATA, /**< USB CDC interface class for data */
	.bInterfaceSubClass = 0,  /**< USB CDC ACM interface subclass */
	.bInterfaceProtocol = 0, /**< USB CDC ACM none protocol */
	.iInterface = 0, /**< the index of the string in the string table that represents interface description */

	.endpoint = usb_cdcacm_data_endpoints, /**< pointer to endpoint descriptor */
};

/** USB DFU functional descriptor
 *  @note as defined in USB Device Firmware Upgrade specification section 4.2.4
 */
static const struct usb_dfu_descriptor usb_dfu_functional = {
	.bLength = sizeof(struct usb_dfu_descriptor), /**< provide own size */
	.bDescriptorType = DFU_FUNCTIONAL, /**< functional descriptor type */
	.bmAttributes = USB_DFU_CAN_DOWNLOAD | USB_DFU_WILL_DETACH, /**< this DFU can download and will detach after download */
	.wDetachTimeout = 200, /**< maximum time (in milliseconds) needed to detach (else resume normal operation) */
	.wTransferSize = sizeof(usbd_control_buffer), /**< set max transfer size */
	.bcdDFUVersion = 0x0110, /**< DFU specification version 1.1 used */
};

/** USB DFU interface descriptor
 *  @note as defined in USB Device Firmware Upgrade specification section 4.2.3
 */
static const struct usb_interface_descriptor usb_dfu_interface = {
	.bLength = USB_DT_INTERFACE_SIZE, /**< size of descriptor in byte */
	.bDescriptorType = USB_DT_INTERFACE, /**< interface descriptor type */
	.bInterfaceNumber = 2, /**< interface number in the list */
	.bAlternateSetting = 0, /**< no alternative settings */
	.bNumEndpoints = 0, /**< only the control pipe at endpoint 0 is used */
	.bInterfaceClass = 0xFE, /**< DFU interface class (not defined in libopencm3 dfu lib) */
	.bInterfaceSubClass = 1, /**< DFU interface subclass (not defined in libopencm3 dfu lib) */
	.bInterfaceProtocol = 1,  /**< runtime protocol (not defined in libopencm3 dfu lib) */
	.iInterface = 4, /**< the index of the string in the string table that represents interface description */

	.extra = &usb_dfu_functional, /**< point to functional descriptor */
	.extralen = sizeof(usb_dfu_functional), /**< size of functional descriptor */
};

/** USB CDC ACM interface descriptor */
static const struct usb_interface usb_cdcacm_interfaces[] = {{
	.num_altsetting = 1,
	.altsetting = &usb_cdcacm_communication_interface,
}, {
	.num_altsetting = 1,
	.altsetting = &usb_cdcacm_data_interface,
}, {
	.num_altsetting = 1,
	.altsetting = &usb_dfu_interface,
}};

/** USB CDC ACM configuration descriptor */
static const struct usb_config_descriptor usb_cdcacm_configuration_descriptor = {
	.bLength = USB_DT_CONFIGURATION_SIZE, /**< the length of this header in bytes */
	.bDescriptorType = USB_DT_CONFIGURATION, /**< a value of 2 indicates that this is a configuration descriptor */
	.wTotalLength = 0, /**< this should hold the total size of the configuration descriptor including all sub interfaces. it is automatically filled in by the USB stack in libopencm3 */
	.bNumInterfaces = LENGTH(usb_cdcacm_interfaces), /**< the number of interfaces in this configuration */
	.bConfigurationValue = 1, /**< the index of this configuration */
	.iConfiguration = 0, /**< a string index describing this configuration (zero means not provided) */
	.bmAttributes = 0x80, /**< bus powered (1<<7) */
	.bMaxPower = 0xfa, /**< the maximum amount of current that this device will draw in 2mA units */
	// end of header
	.interface = usb_cdcacm_interfaces, /**< pointer to an array of interfaces */
};

/** device ID used as serial number */
static char usb_serial[] = "00112233445566778899aabb";

/** USB string table
 *  @note starting with index 1
 */
static const char* usb_strings[] = {
	"CuVoodoo", /**< manufacturer string */
	"CuVoodoo STM32F1xx firmware", /**< product string */
	(const char*)usb_serial, /**< device ID used as serial number */
	"DFU bootloader (runtime mode)", /**< DFU interface string */
};

/** disconnect USB to force re-enumerate */
static void usb_disconnect(void)
{
	if (usb_device) {
		usbd_disconnect(usb_device, true);
	}
#if defined(MAPLE_MINI)
	// disconnect USB D+ using dedicated DISC line/circuit on PB9
	rcc_periph_clock_enable(RCC_GPIOB);
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO9);
	gpio_set(GPIOB, GPIO9);
#elif defined(BLASTER)
	// enable USB D+ pull-up
	rcc_periph_clock_enable(RCC_GPIOB);
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO6);
	gpio_clear(GPIOB, GPIO6);
#endif
	// pull USB D+ low for a short while
	rcc_periph_clock_enable(RCC_GPIOA);
	gpio_set_mode(GPIOA, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO12);
	gpio_clear(GPIOA, GPIO12);
	// USB disconnected must be at least 10 ms long, at most 100 ms
	for (uint32_t i = 0; i < 0x2000; i++) {
		__asm__("nop");
	}
}

/** DFU detach (disconnect USB and perform core reset)
 *  @param[in] usbd_dev USB device (unused)
 *  @param[in] req USB request (unused)
 */
static void usb_dfu_detach(usbd_device *usbd_dev, struct usb_setup_data *req)
{
	(void)usbd_dev; // variable not used
	(void)req; // variable not used
	usb_disconnect(); // USB detach (disconnect to force re-enumeration)
	// set DFU magic
	__dfu_magic[0] = 'D';
	__dfu_magic[1] = 'F';
	__dfu_magic[2] = 'U';
	__dfu_magic[3] = '!';
	scb_reset_system(); // reset system (core and peripherals)
	while (true); // wait for the reset to happen
}

/** incoming USB CDC ACM control request
 *  @param[in] usbd_dev USB device descriptor
 *  @param[in] req control request information
 *  @param[in] buf control request data
 *  @param[in] len control request data length
 *  @param[in] complete function to run after request completed
 *  @return USBD_REQ_HANDLED if handled correctly, USBD_REQ_NOTSUPP else
 *  @note resets device when configured with 5 bits
 */
static enum usbd_request_return_codes usb_cdcacm_control_request(usbd_device *usbd_dev, struct usb_setup_data *req, uint8_t **buf, uint16_t *len, void (**complete)(usbd_device *usbd_dev, struct usb_setup_data *req))
{
	if (usb_dfu_interface.bInterfaceNumber==req->wIndex) { // check if request is for DFU
		switch (req->bRequest) {
			case DFU_DETACH: // USB detach requested
				*complete = usb_dfu_detach; // detach after reply
				break;
			case DFU_GETSTATUS: // get status
				(*buf)[0] = DFU_STATUS_OK;; // set OK status
				(*buf)[1] = 0; // set null poll timeout
				(*buf)[2] = 0; // set null poll timeout
				(*buf)[3] = 0; // set null poll timeout
				(*buf)[4] = STATE_APP_IDLE; // application is running
				(*buf)[5] = 0; // string not used
				*len = 6; // set length of buffer to return
				break;
			default: // other requests are not supported
				return USBD_REQ_NOTSUPP;
		}
	} else if (usb_cdcacm_communication_interface.bInterfaceNumber==req->wIndex) { // check if request is for CDC
		switch (req->bRequest) {
			case USB_CDC_REQ_SET_CONTROL_LINE_STATE:
				usb_cdcacm_connecting = (0!=req->wValue); // check if terminal is open (windows set the control line state before a terminal opens the port, but with value 0)
				//bool dtr = (req->wValue & (1 << 0)) ? true : false;
				//bool rts = (req->wValue & (1 << 1)) ? true : false;
				/* the Linux cdc_acm driver requires this to be implemented
				 * even though it's optional in the CDC spec, and we don't
				 * advertise it in the ACM functional descriptor.
				 */
				uint8_t reply[10] = {0};
				struct usb_cdc_notification *notif = (void *)reply;
				/* we echo signals back to host as notification. */
				notif->bmRequestType = 0xA1;
				notif->bNotification = USB_CDC_NOTIFY_SERIAL_STATE;
				notif->wValue = 0;
				notif->wIndex = 0;
				notif->wLength = 2;
				reply[8] = req->wValue & 3;
				reply[9] = 0;
				usbd_ep_write_packet(usbd_dev, usb_cdcacm_communication_endpoints[0].bEndpointAddress, reply, LENGTH(reply)); // send the reply from communication endpoint (we can't use the complete callback because we are not using the current control endpoint)
				break;
			case USB_CDC_REQ_SET_LINE_CODING:
				// ignore if length is wrong
				if (*len < sizeof(struct usb_cdc_line_coding)) {
					return USBD_REQ_NOTSUPP;
				}
				// line coding is ignored
				// to get the line coding
				// struct usb_cdc_line_coding *coding = (struct usb_cdc_line_coding *)*buf;
				break;
			default:
				return USBD_REQ_NOTSUPP;
		}
	}
	return USBD_REQ_HANDLED;
}

/** USB CDC ACM data received callback
 *  @note called when data has been received
 *  @param[in] usbd_dev USB device descriptor
 *  @param[in] ep endpoint where data came in
 */
static void usb_cdcacm_data_rx_cb(usbd_device *usbd_dev, uint8_t ep)
{
	(void)ep; // not used

	char usb_data[USB_DATA_TRANSFER_SIZE] = {0}; // buffer to read data
	uint16_t usb_length = 0; // length of incoming data

	// receive data
	usb_length = usbd_ep_read_packet(usbd_dev, usb_cdcacm_data_endpoints[0].bEndpointAddress, usb_data, sizeof(usb_data));
	if (usb_length) { // copy received data
		for (uint16_t i=0; i<usb_length && i<LENGTH(usb_data); i++) { // only until buffer is full
			user_input_store(usb_data[i]); // store user data
		}
	}
}

/** USB CDC ACM data transmitted callback
 *  @note called once transmission is completed
 *  @param[in] usbd_dev USB device descriptor
 *  @param[in] ep endpoint where data came in
 */
static void usb_cdcacm_data_tx_cb(usbd_device *usbd_dev, uint8_t ep)
{
	(void)ep; // not used
	static bool usb_tx_ongoing = false; // if USB transmission is already ongoing

	if (!usbd_dev && usb_tx_ongoing) { // putchar is trying to send data but a transmission is already ongoing
		return;
	}
	if (0==tx_used || !first_connection) { // verify if we can send and there is something to send
		usb_tx_ongoing = false; // transmission ended
		return;
	}
	if (!tx_lock) { // ensure no data is in being put in the buffer
		tx_lock = true; // get the lock (no dead lock should occur since putchar should not be used in interrupts)
		usb_tx_ongoing = true; // remember we started transmission
		uint16_t usb_length = (tx_used > USB_DATA_TRANSFER_SIZE ? USB_DATA_TRANSFER_SIZE : tx_used); // length of data to be transmitted (respect max packet size)
		usb_length = (usb_length > (LENGTH(tx_buffer)-tx_i) ? LENGTH(tx_buffer)-tx_i : usb_length); // since here we use the source array not as ring buffer, only go up to the end
		usb_length = usbd_ep_write_packet(usb_device, 0x82, (void*)(&tx_buffer[tx_i]), usb_length); // transmit data (put into USB FIFO)
		tx_i = (tx_i+usb_length)%LENGTH(tx_buffer); // update location on buffer
		tx_used -= usb_length; // update used size
		tx_lock = false; // release lock
	} else {
		usbd_ep_write_packet(usb_device, usb_cdcacm_data_endpoints[1].bEndpointAddress, NULL, 0); // trigger empty tx for a later callback
	}
	usbd_poll(usb_device); // ensure the data gets sent
}

/** USB CDC ACM communication callback
 *  @note if transmission happens before the control setting is complete with a response form the communication endpoint, Linux echoes back the data
 *  @param[in] usbd_dev USB device descriptor
 *  @param[in] ep endpoint where data came in
 */
static void usb_cdcacm_communication_cb(usbd_device *usbd_dev, uint8_t ep)
{
	(void)ep; // not used
	(void)usbd_dev; // not used

	first_connection |= usb_cdcacm_connecting; // check if port has been opened
	if (tx_used>0 && first_connection) { // if buffer is not empty
		usb_cdcacm_data_tx_cb(NULL, 0); // send available data
	}
}

/** set USB CDC ACM configuration
 *  @param[in] usbd_dev USB device descriptor
 *  @param[in] wValue not used
 */
static void usb_cdcacm_set_config(usbd_device *usbd_dev, uint16_t wValue)
{
	(void)wValue; // not used

	usbd_ep_setup(usbd_dev, usb_cdcacm_communication_endpoints[0].bEndpointAddress, usb_cdcacm_communication_endpoints[0].bmAttributes, usb_cdcacm_communication_endpoints[0].wMaxPacketSize, usb_cdcacm_communication_cb); // set communication endpoint
	usbd_ep_setup(usbd_dev, usb_cdcacm_data_endpoints[0].bEndpointAddress, usb_cdcacm_data_endpoints[0].bmAttributes, usb_cdcacm_data_endpoints[0].wMaxPacketSize, usb_cdcacm_data_rx_cb); // set outgoing (from host) data endpoint
	usbd_ep_setup(usbd_dev, usb_cdcacm_data_endpoints[1].bEndpointAddress, usb_cdcacm_data_endpoints[1].bmAttributes, usb_cdcacm_data_endpoints[1].wMaxPacketSize, usb_cdcacm_data_tx_cb); // set incoming (to host) data endpoint

	usbd_register_control_callback(usbd_dev, USB_REQ_TYPE_CLASS | USB_REQ_TYPE_INTERFACE, USB_REQ_TYPE_TYPE | USB_REQ_TYPE_RECIPIENT, usb_cdcacm_control_request);
}

void usb_cdcacm_setup(void)
{
	snprintf(usb_serial, LENGTH(usb_serial), "%08x%08x%04x%04x", DESIG_UNIQUE_ID2, DESIG_UNIQUE_ID1, DESIG_UNIQUE_ID0 & 0xffff, DESIG_UNIQUE_ID0 >> 16); // set actual device ID as serial

	// initialize USB
	rcc_periph_reset_pulse(RST_USB); // reset USB peripheral
	usb_disconnect(); // disconnect to force re-enumeration
#if defined(MAPLE_MINI)
	// connect USB D+ using dedicated DISC line/circuit on PB9
	rcc_periph_clock_enable(RCC_GPIOB);
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO9);
	gpio_clear(GPIOB, GPIO9);
#elif defined(BLASTER)
	// enable USB D+ pull-up using GPIO
	rcc_periph_clock_enable(RCC_GPIOB);
	gpio_set_mode(GPIOB, GPIO_MODE_OUTPUT_2_MHZ, GPIO_CNF_OUTPUT_PUSHPULL, GPIO6);
	gpio_set(GPIOB, GPIO6);
#endif
	rcc_periph_clock_enable(RCC_GPIOA); // enable clock for GPIO used for USB
	rcc_periph_clock_enable(RCC_USB); // enable clock for USB domain
	usb_device = usbd_init(&st_usbfs_v1_usb_driver, &usb_cdcacm_device_descriptor, &usb_cdcacm_configuration_descriptor, usb_strings, LENGTH(usb_strings), usbd_control_buffer, sizeof(usbd_control_buffer));
	usbd_register_set_config_callback(usb_device, usb_cdcacm_set_config);
	nvic_enable_irq(NVIC_USB_LP_CAN_RX0_IRQ); // enable interrupts (to not have to poll all the time)

	// reset buffer states
	tx_i = 0;
	tx_used = 0;
	tx_lock = false; // release lock
	usb_cdcacm_connecting = false; // clear flag
	first_connection = false; // reset first connection detection
}

void usb_cdcacm_putchar(char c)
{
	if (!usb_device) {
		return;
	}
	while (tx_lock); // wait for lock to be released
	tx_lock = true; // put lock on transmit buffer
	if (tx_used<LENGTH(tx_buffer)) { // buffer not full
		tx_buffer[(tx_i+tx_used)%LENGTH(tx_buffer)] = c; // put character in buffer
		tx_used++; // update used buffer
	} else { // buffer full (might be that no terminal is connected to this serial)
		tx_i = (tx_i+1)%LENGTH(tx_buffer); // shift start
		tx_buffer[(tx_i+tx_used)%LENGTH(tx_buffer)] = c; // overwrite old data
	}
	tx_lock = false; // release lock on transmit buffer
	usb_cdcacm_data_tx_cb(NULL, 0); // send data over USB when possible
}

void usb_cdcacm_flush(void)
{
	while (tx_used);
}

/** USB interrupt service routine called when data is received */
void usb_lp_can_rx0_isr(void)
{
	usbd_poll(usb_device);
}
