/** library for USB CDC ACM communication (API)
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2019
 */
#pragma once

/** flag set to true when user is connected to USB CDC ACM port (e.g. when a terminal is opened) */
extern volatile bool usb_cdcacm_connecting;

/** setup USB CDC ACM peripheral */
void usb_cdcacm_setup(void);
/** send character over USB (non-blocking)
 *  @param[in] c character to send
 *  @note blocks if transmit buffer is full, else puts in buffer and returns
 */
void usb_cdcacm_putchar(char c);
/** wait until all is transmitted (blocking)
 */
void usb_cdcacm_flush(void);
