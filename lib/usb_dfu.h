/** library for USB DFU to write on internal flash (API)
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2017-2019
 */
#pragma once

/** setup USB DFU peripheral */
void usb_dfu_setup(void);
/** start USB DFU handling */
void usb_dfu_start(void);

