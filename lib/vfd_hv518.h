/** library to drive vacuum fluorescent display using supertex HV518 shift register VFD drivers
 *  @details the current configuration is for a VFD extracted from a Samsung SER-6500 cash register
 *  @file
 *  @author King Kévin <kingkevin@cuvoodoo.info>
 *  @copyright SPDX-License-Identifier: GPL-3.0-or-later
 *  @date 2016-2020
 *  @note peripherals used: SPI @ref vfd_hv518_spi , GPIO @ref vfd_hv518_gpio , timer @ref vfd_hv518_timer
 */
#pragma once

/** number HV518 VFD drivers */
#define VFD_DRIVERS 3
/** number of digits blocks on SER-6500 VFD */
#define VFD_DIGITS 10
/** number of dot matrix blocks on SER-6500 VFD */
#define VFD_MATRIX 12

/** set character to digit block
 *  @param[in] nb digit block to set
 *  @param[in] c ASCII character to set
 *  @note use the MSB of @p nb to enable the dot
 */
void vfd_digit(uint8_t nb, char c);
/** set character to matrix block
 *  @param[in] nb matrix block to set
 *  @param[in] c ASCII character to set
 *  @note on ASCII characters are used for pictures
 */
void vfd_matrix(uint8_t nb, char c);
/** clear VFD display */
void vfd_clear(void);
/** test VFD display (light up all segments) */
void vfd_test(void);
/** switch VFD on */
void vfd_on(void);
/** switch VFD display off */
void vfd_off(void);
/** setup VFD */
void vfd_setup(void);
